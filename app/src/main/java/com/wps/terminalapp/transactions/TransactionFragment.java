package com.wps.terminalapp.transactions;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appolica.flubber.Flubber;
import com.wps.terminalapp.BuildConfig;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.adapters.PopAdapter;
import com.wps.terminalapp.adapters.TransactionAdapter;
import com.wps.terminalapp.custom.ButtonSansBold;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.listeners.AlertListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.listeners.ViewTransactionListener;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.settings.AppPreferences;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.wps.terminalapp.utils.Util.currentSettleYN;
import static com.wps.terminalapp.utils.Util.settleAmount;

/**
 * Created by vignaraj.r on 08-Aug-18.
 */

public class TransactionFragment extends Fragment {

    @BindView(R.id.btn_all)
    ButtonSansBold btnAll;
    @BindView(R.id.btn_history)
    ButtonSansBold btnHistory;
    @BindView(R.id.txt_settleamt)
    TextView txtSettleamt;
    @BindView(R.id.txt_aed)
    TextSansBold txtAed;
    @BindView(R.id.layout_header)
    FrameLayout layoutHeader;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_list;
    @BindView(R.id.list_transactions)
    RecyclerView listTransactions;
    @BindView(R.id.layout_container)
    RelativeLayout layoutContainer;
    @BindView(R.id.btn_settle)
    TextSansBold btnSettle;
    @BindView(R.id.layout_bottom)
    RelativeLayout layoutBottom;
    @BindView(R.id.view_space)
    View view_space;
    @BindView(R.id.layout_loadview)
    RelativeLayout layout_loadview;
    @BindView(R.id.txt_loading)
    TextSansRegular txt_loading;

    @BindView(R.id.txt_cashsale)
    TextSansRegular txt_cashsale;
    @BindView(R.id.txt_cardsale)
    TextSansRegular txt_cardsale;
    @BindView(R.id.txt_totaltip)
    TextSansRegular txt_totaltip;
    @BindView(R.id.layout_payments)
    LinearLayout layout_payments;

    //
    public static String TAG = TransactionFragment.class.getSimpleName();

    Context context;

    private View stickyViewSpacer;
    TransactionListener listener;


    TransactionAdapter transactionAdapter;
    String page_url = "";

    LinearLayoutManager layoutManager;
    boolean isLoading = false;
    public String currentType = "ALL";


    public interface TransactionListener {
        void goHistory();

        void viewTransaction(TransactionModel model);

        void settleTransaction();
    }

    public interface PopListener {
        void selectedMenu(String menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transactions, container, false);
        context = getActivity();
        LayoutInflater headerInf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listHeader = headerInf.inflate(R.layout.list_header, null);
        stickyViewSpacer = listHeader.findViewById(R.id.stickyViewPlaceholder);

        ButterKnife.bind(this, view);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listTransactions.setLayoutManager(layoutManager);
        swipe_list.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        showEmptyScreen();
        swipe_list.setOnRefreshListener(refreshListener);
        listTransactions.addOnScrollListener(scrollListener);
//        controller = new TransactionController(context, view, listener, controllerInterface);
        manageTransactions();
        if (Util.isPopBack) {
            Flubber.with()
                    .animation(Flubber.AnimationPreset.SLIDE_LEFT)
                    .repeatCount(0)
                    .duration(2000)
                    .createFor(view)
                    .start();
            Util.isPopBack = false;
        } else {
            Flubber.with()
                    .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                    .repeatCount(0)
                    .duration(2000)
                    .createFor(view)
                    .start();
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainActivity) context;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(context, e.getMessage());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        showEmptyScreen();
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    public ViewTransactionListener viewTransactionListener = new ViewTransactionListener() {
        @Override
        public void selectedTransaction(TransactionModel model) {
            // to view the specific tranaction.
            listener.viewTransaction(model);
        }
    };

    public RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int lastVissibleItemPOs = layoutManager.findLastVisibleItemPosition();
//            Log.i("lastVisPos", String.valueOf(lastVissibleItemPOs));
//            Log.i("childCount", String.valueOf(Util.transactionList.size() - 1));
            if (!isLoading && lastVissibleItemPOs == Util.transactionList.size() - 1) {
                if (!page_url.equals("") && !page_url.equals("null")) {
                    isLoading = true;
                    loadMoreTransaction(lastVissibleItemPOs);
                }
            }
        }
    };

    public void refreshAll() {
        Util.freeMemory();
        Util.dTag(1, TAG, "refreshAll");
        loadTransactions("CURRENTBATCH", "ALL", "ALL", "ALL");

    }

    public SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            Util.freeMemory();
            loadTransactions("CURRENTBATCH", currentType, "ALL", "ALL");
        }
    };

//    public ControllerInterface controllerInterface = new ControllerInterface() {
//        @Override
//        public void selectedType(String type) {
//            currentType = type;
//            loadTransactions("CURRENTBATCH", type, "ALL", "ALL");
//        }
//    };

    /**
     * Load the transaction
     */
    public void loadTransactions(String... params) {
        Util.freeMemory();
        Util.dTag(0, TAG, "loadTransactions");
        swipe_list.setRefreshing(true);
        String completeUrl = "";
        if (params[2].equals("SELECTDATE")) {
            completeUrl = GeneralUtil.getDeviceSerial() + "&strBATCH=" + params[0] + "&strTXNTYPE=" + params[1] + "&strDATE=" + params[2] + "&strCARDTYPE=" + params[3] + "&dtFrom=" + params[4] + "&dtTo=" + params[5] + "&recCount=0";
        } else {
            completeUrl = GeneralUtil.getDeviceSerial() + "&strBATCH=" + params[0] + "&strTXNTYPE=" + params[1] + "&strDATE=" + params[2] + "&strCARDTYPE=" + params[3] + "&dtFrom=" + "&dtTo=" + "&recCount=0";
        }

        int transactionSize = Util.transactionList.size();

        if (transactionSize > 0) {
            Util.transactionList.clear();
        }

        WebServices.getInstance().getTransactions(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("trans_resp##", responseStr);
                            JSONObject obj = new JSONObject(responseStr);
                            if (obj.getBoolean("IsSuccess")) {
                                JSONObject responseData = obj.getJSONObject("ResponseData");
                                page_url = responseData.optString("fetchUrl");
                                double sAmt = responseData.optDouble("decSettleAmt");
                                double actualVal = sAmt / 100;

                                double mCard = responseData.optDouble("decCardAmt");
                                double fCardTotal = mCard / 100;

                                double mCash = responseData.optDouble("decCashAmt");
                                double fCashTotal = mCash / 100;

                                double mTip = responseData.optDouble("decTipAmt");
                                double fTipTotal = mTip / 100;

                                String settleYN = responseData.optString("settle_yn");

                                Util.totalCardAmount = Util.formatAmount(fCardTotal);
                                Util.totalCashAmount = Util.formatAmount(fCashTotal);
                                Util.totalTipAmount = Util.formatAmount(fTipTotal);

                                settleAmount = Util.formatAmount(actualVal);
                                JSONArray tArray = responseData.getJSONArray("Transactions");
                                if (tArray != null && tArray.length() > 0) {
                                    for (int i = 0; i < tArray.length(); i++) {
                                        JSONObject tObject = tArray.getJSONObject(i);
                                        double amount = tObject.optDouble("mAmount");
                                        double actVal = 0;
                                        if (amount > 0) {
                                            actVal = amount / 100;
                                        }
                                        String cardNumber = tObject.optString("mCardNumber");
                                        String cardType = tObject.optString("mCardType");
                                        String cardImgType = tObject.optString("mCardTypeImg");
                                        String transactionId = tObject.optString("mTransactionId");
                                        String transactionStatus = tObject.optString("mTransactionStatus");
                                        String type = tObject.optString("mType");
                                        String cardHolderName = tObject.optString("mCardHolderName");
                                        String mDisplayNo = tObject.optString("mDisplayNo");
                                        long date = tObject.optLong("mDate");
                                        String transactionType = tObject.optString("mPayType");
                                        Util.transactionList.add(new TransactionModel(transactionId, date, type, transactionType, cardType,
                                                cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo));
                                    }
                                    if (Util.transactionList.size() > 0) {
                                        layout_loadview.setVisibility(View.GONE);
//                                        mFadingCircleDrawable.stop();
                                        swipe_list.setVisibility(View.VISIBLE);
                                        Flubber.with()
                                                .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                                                .repeatCount(0)
                                                .duration(1000)
                                                .createFor(listTransactions)
                                                .start();
                                        transactionAdapter = new TransactionAdapter(getActivity(), Util.transactionList, viewTransactionListener);
                                        listTransactions.setAdapter(transactionAdapter);
                                        transactionAdapter.notifyDataSetChanged();
                                        Log.i("settleAmt", settleAmount);
                                        currentSettleYN = settleYN;
                                        if (!settleAmount.equals("")) {
                                            settleAmount = settleAmount.replace(",", "");
                                            if (settleYN.equals("Y")) {
                                                showSettleButtonByType();
                                            } else {
                                                layoutBottom.setVisibility(View.GONE);
                                            }
                                            int beforeDecimal = Integer.parseInt(settleAmount.substring(0, settleAmount.indexOf(".")));
                                            int afterDecimal = Integer.parseInt(settleAmount.substring(settleAmount.indexOf(".") + 1, settleAmount.length()));
                                            Util.animateText(txtSettleamt, beforeDecimal, afterDecimal);
                                            layout_payments.setVisibility(View.VISIBLE);
                                            txt_cardsale.setText(Util.totalCardAmount + "\nCARD");
                                            txt_cashsale.setText(Util.totalCashAmount + "\nCASH");
                                            txt_totaltip.setText(Util.totalTipAmount + "\nTIP");
                                        } else {
                                            layout_payments.setVisibility(View.GONE);
                                        }
                                    } else {
                                        swipe_list.setVisibility(View.GONE);
                                        layout_loadview.setVisibility(View.VISIBLE);
                                        txt_loading.setText("No Transaction!");
                                    }
                                } else {
                                    swipe_list.setVisibility(View.GONE);
                                    layout_loadview.setVisibility(View.VISIBLE);
                                    txt_loading.setText("No Transaction!");
                                }
                            } else {
                                String errMsg = obj.optString("ErrMsg");
                                swipe_list.setVisibility(View.GONE);
                                layout_loadview.setVisibility(View.VISIBLE);
                                txt_loading.setText("Failed to load Transaction!" + "\n" + errMsg);
                            }
                        } catch (JSONException e) {
                            swipe_list.setVisibility(View.GONE);
                            layout_loadview.setVisibility(View.VISIBLE);
                            txt_loading.setText("Failed to load Transaction!" + "\n" + e.getMessage());
                        }

                        swipe_list.setRefreshing(false);
                    }
                });

            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_loading.setText("Faild to load Transaction!" + "\n" + message);
                        swipe_list.setRefreshing(false);
                    }
                });
            }
        }, completeUrl);
    }

    /**
     * To scroll to load more transaction
     **/
    public void loadMoreTransaction(final int lastPosition) {
        Util.dTag(0, TAG, "loadMoreTransaction");
        Util.freeMemory();

        WebServices.getInstance().loadMoreTransactions(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString(TAG, "loadmore##"+responseStr);
                            JSONObject obj = new JSONObject(responseStr);
                            if (obj.getBoolean("IsSuccess")) {
                                JSONObject responseData = obj.getJSONObject("ResponseData");
                                page_url = responseData.optString("fetchUrl");
                                double sAmt = responseData.optDouble("decSettleAmt");
                                double actualVal = sAmt / 100;

                                double mCard = responseData.optDouble("decCardAmt");
                                double fCardTotal = mCard / 100;

                                double mCash = responseData.optDouble("decCashAmt");
                                double fCashTotal = mCash / 100;

                                double mTip = responseData.optDouble("decTipAmt");
                                double fTipTotal = mTip / 100;

                                String settleYN = responseData.optString("settle_yn");

                                Util.totalCardAmount = Util.formatAmount(fCardTotal);
                                Util.totalCashAmount = Util.formatAmount(fCashTotal);
                                Util.totalTipAmount = Util.formatAmount(fTipTotal);


                                settleAmount = Util.formatAmount(actualVal);
                                JSONArray tArray = responseData.getJSONArray("Transactions");
                                if (tArray != null && tArray.length() > 0) {
                                    for (int i = 0; i < tArray.length(); i++) {
                                        JSONObject tObject = tArray.getJSONObject(i);
                                        double amount = tObject.optDouble("mAmount");
                                        double actVal = 0;
                                        if (amount > 0) {
                                            actVal = amount / 100;
                                        }
                                        String cardNumber = tObject.optString("mCardNumber");
                                        String cardType = tObject.optString("mCardType");
                                        String cardImgType = tObject.optString("mCardTypeImg");
                                        String cardHolderName = tObject.optString("mCardHolderName");
                                        String transactionId = tObject.optString("mTransactionId");
                                        String transactionStatus = tObject.optString("mTransactionStatus");
                                        String type = tObject.optString("mType");
                                        String mDisplayNo = tObject.optString("mDisplayNo");
                                        long date = tObject.optLong("mDate");
                                        String transactionType = tObject.optString("mPayType");
                                        Util.transactionList.add(new TransactionModel(transactionId, date, type, transactionType, cardType,
                                                cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo));
                                    }
                                    Flubber.with()
                                            .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                                            .repeatCount(0)
                                            .duration(1000)
                                            .createFor(listTransactions)
                                            .start();
                                    transactionAdapter = new TransactionAdapter(getActivity(), Util.transactionList, viewTransactionListener);
                                    listTransactions.setAdapter(transactionAdapter);
                                    transactionAdapter.notifyDataSetChanged();
                                    listTransactions.scrollToPosition(lastPosition);
                                    Log.i("settleAmt", settleAmount);
                                    if (!settleAmount.equals("")) {
                                        settleAmount = settleAmount.replace(",", "");
                                        currentSettleYN = settleYN;
                                        if (settleYN.equals("Y")) {
                                            showSettleButtonByType();
                                        } else {
                                            layoutBottom.setVisibility(View.GONE);
                                        }
                                        int beforeDecimal = Integer.parseInt(settleAmount.substring(0, settleAmount.indexOf(".")));
                                        int afterDecimal = Integer.parseInt(settleAmount.substring(settleAmount.indexOf(".") + 1, settleAmount.length()));
                                        Util.animateText(txtSettleamt, beforeDecimal, afterDecimal);
                                        layout_payments.setVisibility(View.VISIBLE);
                                        txt_cardsale.setText(Util.totalCardAmount + "\nCARD");
                                        txt_cashsale.setText(Util.totalCashAmount + "\nCASH");
                                        txt_totaltip.setText(Util.totalTipAmount + "\nTIP");
                                    } else {
                                        layout_payments.setVisibility(View.GONE);
                                    }
                                } else {
                                    txt_loading.setText("No Transaction!");
                                }
                            } else {
                                String errMsg = obj.optString("ErrMsg");
                                txt_loading.setText("Failed to load Transaction!\n" + errMsg);
                            }
                        } catch (JSONException e) {
                            txt_loading.setText("Failed to load Transaction!\n" + e.getMessage());
                        }
                        swipe_list.setRefreshing(false);
                        isLoading = false;


                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_loading.setText("Faild to load Transaction!\n" + message);
                        swipe_list.setRefreshing(false);
                        isLoading = false;
                    }
                });
            }
        }, page_url);
    }

    public void manageTransactions() {
        Util.freeMemory();
        Util.dTag(0, TAG, "manageTransactions");
        Util.transactionList.clear();
        if (Util.transactionList.size() > 0) {
            layout_loadview.setVisibility(View.GONE);
            swipe_list.setVisibility(View.VISIBLE);
            Flubber.with()
                    .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                    .repeatCount(0)
                    .duration(1000)
                    .createFor(listTransactions)
                    .start();
            transactionAdapter = new TransactionAdapter(getActivity(), Util.transactionList, viewTransactionListener);
            listTransactions.setAdapter(transactionAdapter);
            transactionAdapter.notifyDataSetChanged();
            if (!settleAmount.equals("")) {
                settleAmount = settleAmount.replace(",", "");
                if (currentSettleYN.equals("Y")) {
                    showSettleButtonByType();
                } else {
                    layoutBottom.setVisibility(View.GONE);
                }

                if (!settleAmount.equals("0.00")) {
                    layout_payments.setVisibility(View.VISIBLE);
                    txt_cardsale.setText(Util.totalCardAmount + "\nCARD");
                    txt_cashsale.setText(Util.totalCashAmount + "\nCASH");
                    txt_totaltip.setText(Util.totalTipAmount + "\nTIP");
                } else {
                    layout_payments.setVisibility(View.GONE);
                }
                int beforeDecimal = Integer.parseInt(settleAmount.substring(0, settleAmount.indexOf(".")));
                int afterDecimal = Integer.parseInt(settleAmount.substring(settleAmount.indexOf(".") + 1, settleAmount.length()));
                Util.animateText(txtSettleamt, beforeDecimal, afterDecimal);

            }
        } else {
            loadTransactions("CURRENTBATCH", "ALL", "ALL", "ALL");
        }


    }

    public void showSettleButtonByType() {
        if (AppPreferences.getInstance().getSettleType(getActivity()).equals("AUTO")) {
            layoutBottom.setVisibility(View.GONE);
        } else {
            layoutBottom.setVisibility(View.VISIBLE);
        }
    }

    public void showEmptyScreen() {
        Util.dTag(0, TAG, "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });
    }

    @OnClick(R.id.btn_history)
    public void goHistory() {
        Util.dTag(1, TAG, "goHistory");
        listener.goHistory();
    }

    @OnClick(R.id.btn_all)
    public void showPopWindow() {
        Util.freeMemory();
        Util.dTag(1, TAG, "showPopWindow");
        final PopupWindow popWindow = new PopupWindow(context);
        popWindow.setFocusable(true);
        popWindow.setWidth(280);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        ArrayList<String> menuList = new ArrayList<>();
        menuList.add("SALE");
        menuList.add("VOID");
        menuList.add("ALL");
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = inflater.inflate(R.layout.layout_popup, null);
        popWindow.setContentView(v);
        //orderMenu.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e5e5e5")));
        popWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ListView list_menuItems = (ListView) v.findViewById(R.id.order_poplist);
        list_menuItems.setAdapter(new PopAdapter(context, menuList, new TransactionFragment.PopListener() {
            @Override
            public void selectedMenu(String name) {
                btnAll.setText(name);
                popWindow.dismiss();
                loadTransactions("CURRENTBATCH", name, "ALL", "ALL");
            }
        }));
        popWindow.showAsDropDown(btnAll, 0, 0);
    }

    @OnClick({R.id.btn_settle, R.id.layout_bottom})
    public void settleTransaction() {
        Util.dTag(1, TAG, "settleTransaction");
        Util.showConfirmation(context, "Do you want to Settle All Transaction?", new AlertListener() {
            @Override
            public void done() {
                if (BuildConfig.BUILD_TYPE.equals("debug") && Util.isProviderExist(context)) {
                    Util.showErrorToast(context, "Please remove the live transaction processor to proceed the settlements");
                } else {
                    listener.settleTransaction();
                }
            }

            @Override
            public void cancel() {
                Util.showInfoToast(context, "Settle Cancelled by user");
            }
        });
    }

}