package com.wps.terminalapp.transactions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.appolica.flubber.Flubber;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.squareup.picasso.Picasso;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.api.APIBuilder;
import com.wps.terminalapp.api.APIComponent;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.dialogs.PinDialog;
import com.wps.terminalapp.listeners.ErrorListener;
import com.wps.terminalapp.listeners.PinListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.models.ErrorLogResponse;
import com.wps.terminalapp.models.FetchDeviceDetailsResponse;
import com.wps.terminalapp.models.FetchResponse;
import com.wps.terminalapp.models.LoGErr;
import com.wps.terminalapp.models.OrderRefundResponse;
import com.wps.terminalapp.models.OrderRevokeResponse;
import com.wps.terminalapp.models.PrintModel;
import com.wps.terminalapp.models.StoreTransactionRequest;
import com.wps.terminalapp.models.StoreTransactionResponse;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.models.print.PoyntPrintData;
import com.wps.terminalapp.models.print.PoyntPrintDataNestedImg;
import com.wps.terminalapp.models.print.PrintData;
import com.wps.terminalapp.models.print.PrintResponse;
import com.wps.terminalapp.models.receet.ReceetData;
import com.wps.terminalapp.models.receet.ReceetResponse;
import com.wps.terminalapp.models.receet.ResponseData;
import com.wps.terminalapp.settings.AppPreferences;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.PrintUtil;
import com.wps.terminalapp.utils.PrintUtil2;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.utils.WPRSAUtil;
import com.wps.terminalapp.webrequest.WebServices;

import net.minidev.json.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.ParametersAreNonnullByDefault;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.poynt.api.model.Transaction;
import co.poynt.api.model.TransactionAction;
import co.poynt.api.model.TransactionStatus;
import co.poynt.os.Constants;
import co.poynt.os.model.AccessoryProvider;
import co.poynt.os.model.AccessoryProviderFilter;
import co.poynt.os.model.AccessoryType;
import co.poynt.os.model.Intents;
import co.poynt.os.model.Payment;
import co.poynt.os.model.PaymentStatus;
import co.poynt.os.model.PoyntError;
import co.poynt.os.model.PrintedReceipt;
import co.poynt.os.model.PrintedReceiptV2;
import co.poynt.os.model.PrinterStatus;
import co.poynt.os.model.ReceiptOption;
import co.poynt.os.model.ReceiptType;
import co.poynt.os.printing.ReceiptPrintingPref;
import co.poynt.os.services.v1.IPoyntAccessoryManagerListener;
import co.poynt.os.services.v1.IPoyntPrinterService;
import co.poynt.os.services.v1.IPoyntPrinterServiceListener;
import co.poynt.os.services.v1.IPoyntReceiptPrintingService;
import co.poynt.os.services.v1.IPoyntReceiptPrintingServiceListener;
import co.poynt.os.services.v1.IPoyntReceiptSendListener;
import co.poynt.os.services.v1.IPoyntTransactionServiceListener;
import co.poynt.os.services.v2.IPoyntReceiptChoiceListener;
import co.poynt.os.util.AccessoryProviderServiceHelper;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by vignaraj.r on 27-Aug-18.
 */

@SuppressLint("ValidFragment")
public class ViewTransaction extends Fragment {

    TransactionModel model;

    @BindView(R.id.img_tstatus)
    ImageView imgTstatus;
    @BindView(R.id.txt_transactionStatus)
    TextSansRegular txtTransactionStatus;
    @BindView(R.id.txt_transactionamount)
    TextSansRegular txtTransactionamount;
    @BindView(R.id.txt_transactiontime)
    TextSansRegular txtTransactiontime;
    @BindView(R.id.layout_statusview)
    RelativeLayout layoutStatusview;
    @BindView(R.id.layout_one)
    LinearLayout layoutOne;
    @BindView(R.id.txt_saleamount)
    TextSansBold txtSaleamount;
    @BindView(R.id.txt_netamount)
    TextSansBold txtNetamount;
    @BindView(R.id.txt_tipamount)
    TextSansBold txtTipamount;
    @BindView(R.id.layout_two)
    LinearLayout layoutTwo;
    @BindView(R.id.layout_three)
    LinearLayout layoutThree;
    @BindView(R.id.img_cardtype)
    ImageView imgCardType;
    @BindView(R.id.txt_cardholdername)
    TextSansRegular txtCardholdername;
    @BindView(R.id.txt_cardnumber)
    TextSansRegular txtCardnumber;
    @BindView(R.id.txt_invoiceno)
    TextSansRegular txtInvoiceno;
    @BindView(R.id.txt_invoicedate)
    TextSansRegular txtInvoicedate;
    @BindView(R.id.txt_invoice_note)
    TextSansRegular txtInvoiceNote;
    @BindView(R.id.layout_invoice)
    LinearLayout layoutInvoice;
    @BindView(R.id.layout_carddtl)
    LinearLayout layoutCarddtl;
    @BindView(R.id.btn_custreceipt)
    ButtonSansRegular btnCustreceipt;
    @BindView(R.id.btn_mercreceipt)
    ButtonSansRegular btnMercreceipt;
    @BindView(R.id.btnprint_layout)
    LinearLayout btnprintLayout;
    /*    @Bind(R.id.btn_void)
        ButtonSansRegular btnVoid;*/
    //
    public static String TAG = ViewTransaction.class.getSimpleName();
    private static final int DISPLAY_PAYMENT_REQUEST = 12222;
    //
    private AccessoryProviderServiceHelper accessoryProviderServiceHelper;
    private HashMap<AccessoryProvider, IBinder> mPrinterServices = new HashMap<>();
    private List<AccessoryProvider> providers;
    private Set<String> printers;
    String errorMsg;
    LoGErr loGErr = new LoGErr();

    public interface ViewTransactionListener {
        void reloadTransaction(TransactionModel model);
    }

    ViewTransactionListener listener;

    // this is the accessory manager listener which gets invoked when accessory manager completes
    // scanning for the requested accessories
    private IPoyntAccessoryManagerListener poyntAccessoryManagerListener
            = new IPoyntAccessoryManagerListener.Stub() {

        @Override
        public void onError(PoyntError poyntError) throws RemoteException {
            Log.e(TAG, "Failed to connect to accessory manager: " + poyntError);
        }

        @Override
        public void onSuccess(final List<AccessoryProvider> printers) throws RemoteException {
            // now that we are connected - request service connections to each accessory provider
            if (printers != null && printers.size() > 0) {
                // save it for future reference
                providers = printers;
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    // for each printer accessory - request "service" connections if it's still connected
                    for (AccessoryProvider printer : printers) {
                        Log.d(TAG, "Printer: " + printer.toString());
                        if (printer.isConnected()) {
                            // request service connection binder
                            // IMP: note that this method returns service connection if it already exists
                            // hence the need for both connection callback and the returned value
                            IBinder binder = accessoryProviderServiceHelper.getAccessoryService(
                                    printer, AccessoryType.PRINTER,
                                    providerConnectionCallback);
                            //already cached connection.
                            if (binder != null) {
                                mPrinterServices.put(printer, binder);
                            }
                        }
                    }
                }
            } else {
                Log.e(TAG, "No Printers found");
            }
        }
    };

    // this is the callback for the service connection to each accessory provider service in this case
    // the android service supporting  printer accessory
    private AccessoryProviderServiceHelper.ProviderConnectionCallback providerConnectionCallback
            = new AccessoryProviderServiceHelper.ProviderConnectionCallback() {

        @Override
        public void onConnected(AccessoryProvider provider, IBinder binder) {
            // in some cases multiple accessories of the same type (eg. two printers of same
            // make/model or two star printers) might be supported by the same android service
            // so here we check if we need to share the same service connection for more than
            // one accessory provider
            List<AccessoryProvider> otherProviders = findMatchingProviders(provider);
            // all of them share the same service binder
            for (AccessoryProvider matchedProvider : otherProviders) {
                mPrinterServices.put(matchedProvider, binder);
            }
        }

        @Override
        public void onDisconnected(AccessoryProvider provider, IBinder binder) {
            // set the lookup done to false so we can try looking up again if needed
            if (mPrinterServices != null && mPrinterServices.size() > 0) {
                mPrinterServices.remove(binder);
                // try to renew the connection.
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    IBinder binder2 = accessoryProviderServiceHelper.getAccessoryService(
                            provider, AccessoryType.PRINTER,
                            providerConnectionCallback);
                    if (binder2 != null) {//already cached connection.
                        mPrinterServices.put(provider, binder2);
                    }
                }
            }
        }
    };

    // we do this if there are multiple accessories connected of the same type/provider
    private List<AccessoryProvider> findMatchingProviders(AccessoryProvider provider) {
        ArrayList<AccessoryProvider> matchedProviders = new ArrayList<>();
        if (providers != null) {
            for (AccessoryProvider printer : providers) {
                if (provider.getAccessoryType() == printer.getAccessoryType()
                        && provider.getPackageName().equals(printer.getPackageName())
                        && provider.getClassName().equals(printer.getClassName())) {
                    matchedProviders.add(printer);
                }
            }
        }
        return matchedProviders;
    }
    //

    @SuppressLint("ValidFragment")
    public ViewTransaction(TransactionModel model) {
        this.model = model;
    }

    View fragmentView;
    Context context;
    String cardType = "";
    Transaction currentTransaction = null;

    private APIComponent componentApliPay;
    private APIComponent componentTerminal;
    private APIComponent retrofitPrint;
    private String transactionId;

    private WebSocket webSocket = null;
    private String webSocketURL = "";
    private String fetchURL = "";
    private String tokenKey = "";
    private String odrTxnJSON = "";
    private String jsonString= "";
    private int qRDispTimeSec = 0;
    private OkHttpClient client;
    private Handler handler = new Handler();
    private Handler connectionHandler = new Handler();
    private APIBuilder apiBuilder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiBuilder = new APIBuilder(requireContext());

        Retrofit retrofitApliPay = apiBuilder.retrofitApliPay();
        componentApliPay = retrofitApliPay.create(APIComponent.class);

        Retrofit retrofitTerminal = apiBuilder.retrofitTerminal();
        componentTerminal = retrofitTerminal.create(APIComponent.class);

        Retrofit mRetrofitPrint = apiBuilder.retrofitPrint();
        retrofitPrint = mRetrofitPrint.create(APIComponent.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_viewtransaction, container, false);
        context = getActivity();
        ButterKnife.bind(this, fragmentView);
        initializeAccessories();
        fragmentView.setVisibility(View.GONE);
        Log.d("model#", new Gson().toJson(model));

        Util.dTag(0, TAG, "assignUI");
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                .repeatCount(0)
                .duration(1000)
                .createFor(fragmentView)
                .start();
        fragmentView.setVisibility(View.VISIBLE);

        if (model != null) {
            transactionId = model.getmTransactionId();
            cardType = model.getmCardType();
            Log.d("cardType#", cardType);
            MainActivity activity = (MainActivity) getActivity();
            activity.setTransactionTag(cardType);
            assignUIFromLocal(model);

            String screenType = AppPreferences.getInstance().getScreenType(requireActivity());
            if("DBL".equals(screenType)){
                collectCustomerReceipt("error");
            }

            if (!cardType.equalsIgnoreCase("Alipay")) {
                getTransaction(transactionId);
            } else {
//                Util.closeProgressDialog();
                assignUI(null);
            }

            fetchOrderDetails(transactionId);

        }
        showEmptyScreen();


/*        WebServices.getInstance().getTxnById(context, model.getmTransactionId(), new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {

            }

            @Override
            public void onFailure(String message) {

            }
        });*/

        return fragmentView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainActivity) context;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(context, e.getMessage());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Received onActivityResult (" + requestCode + ")");
        // After voiding the transaction, send the order details to cloud.
        if (requestCode == DISPLAY_PAYMENT_REQUEST) {
//            Log.i("##qq", "diap_frag");
            if (resultCode == Activity.RESULT_OK) {
//                Log.i("##qq", "result_ok");
                if (data != null) {
                    Payment payment = data.getParcelableExtra(Intents.INTENT_EXTRAS_PAYMENT);
//                    Util.logLargeString("data", new Gson().toJson(payment));
                    String transId = "";
                    if (payment.getStatus().equals(PaymentStatus.VOIDED)) {
//                        Util.logLargeString("payment_his", new Gson().toJson(payment));
                        transId = payment.getTransactions().get(0).getId().toString();
                        //tranaction action refund means voided!
                        if (payment.getTransactions().get(0).getAction().equals(TransactionAction.REFUND)) {
                            updateVoidedScreen(transId);
                        }
                    } else {
                        Util.showErrorDialog(getActivity(), "Error Voiding the transaction!", new ErrorListener() {
                            @Override
                            public void doAction() {
                                // do nothing
                            }
                        });
                    }
                }
            } else {
                Log.d(TAG, "void_cancelled");
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void assignUIFromLocal(TransactionModel model){
        if (model.getmTransactionType().equals("CASH")) {
            layoutCarddtl.setVisibility(View.GONE);
        } else if (model.getmTransactionType().equals("CARD")) {
            layoutCarddtl.setVisibility(View.VISIBLE);
        }

        double amount = model.getmAmount();
        txtSaleamount.setText(Util.formatAmount(amount));
        txtNetamount.setText(Util.formatAmount(amount));
        txtTipamount.setText("0.00");

        String cardHolderName = model.getmCardHolderName();
        String transactionStatus = model.getmTransactionStatus();
        String transactionAmount = Util.formatAmount(model.getmAmount()) + " AED";
//        String date = DateUtils.getRelativeTimeSpanString(model.getmDate()).toString();
        String date = Util.setDate(model.getmDate());
        String cardNumber = model.getmCardNumber();
        txtTransactionStatus.setText(transactionStatus);
        if (transactionStatus.equals("AUTHORIZED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.auth_blue));
//            btnVoid.setVisibility(View.VISIBLE);
            imgTstatus.setImageResource(R.drawable.done_150);
        } else if (transactionStatus.equals("CAPTURED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.green_success));
            imgTstatus.setImageResource(R.drawable.done_150);
        } else if (transactionStatus.equals("DECLINED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.dec_red));
            imgTstatus.setImageResource(R.drawable.cancel_150);
        } else if (transactionStatus.equals("VOIDED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.void_grey));
            imgTstatus.setImageResource(R.drawable.cancel_150);
        }
//        String transId = "#" + model.getmTransactionId().substring(0, 8);

        txtTransactionamount.setText(transactionAmount);
        txtTransactiontime.setText(date);
        txtCardnumber.setText(cardNumber);
        txtCardholdername.setText(cardHolderName);
        if(model.getmCardImage() != null && !model.getmCardImage().isEmpty()) {
            Picasso.get().load(model.getmCardImage()).into(imgCardType);
        }
    }

    public void assignUI(Transaction transaction) {
        Util.closeProgressDialog();

        if (model.getmTransactionType().equals("CASH")) {
            layoutCarddtl.setVisibility(View.GONE);
        } else if (model.getmTransactionType().equals("CARD")) {
            layoutCarddtl.setVisibility(View.VISIBLE);
        }


        if (transaction != null) {
            currentTransaction = transaction;
            String cardHolderName = "";
            double tipAmount = 0;
            double totalSale = 0;
            double netTotal = 0;
            if (transaction.getAmounts().getTipAmount() != null) {
                tipAmount = transaction.getAmounts().getTipAmount().doubleValue() / 100;
                Log.i("tipAmount", String.valueOf(tipAmount));
                txtTipamount.setText(Util.formatAmount(tipAmount));
            }

            if (transaction.getAmounts().getOrderAmount() != null) {
                netTotal = transaction.getAmounts().getOrderAmount().doubleValue() / 100;
                Log.i("netTotal", String.valueOf(netTotal));
                double totalAmount = netTotal + tipAmount;
                txtNetamount.setText(Util.formatAmount(netTotal));
                txtSaleamount.setText(Util.formatAmount(totalAmount));
            } else {
                if (transaction.getAmounts().getTransactionAmount() != null) {
                    totalSale = transaction.getAmounts().getTransactionAmount().doubleValue() / 100;
                    Log.i("saleAmount", String.valueOf(totalSale));
                    txtNetamount.setText(Util.formatAmount(totalSale));
                    txtSaleamount.setText(Util.formatAmount(totalSale));
                }
            }

            if (model.getmTransactionType().equals("CARD")) {
                cardHolderName = transaction.getFundingSource().getCard().getCardHolderFullName();
                if (cardHolderName != null) {
                    txtCardholdername.setText(cardHolderName);
                }
            }
            String[] notes = null;
           /* if (transaction.getNotes() != null && !transaction.getNotes().equals("")) {
                notes = transaction.getNotes().split("~");
                String invId = notes[0];
                String invDate = notes[1];
                String invNotes = notes[2];
                if (!transaction.getNotes().equals("~~") && !transaction.getNotes().equals("")) {
                    layoutInvoice.setVisibility(View.VISIBLE);
                    if (invId != null && !invId.equals("")) {
                        txtInvoiceno.setText(invId);
                    }
                    if (invDate != null && !invDate.equals("")) {
                        txtInvoicedate.setText(invDate);
                    }
                    if (invNotes != null && !invNotes.equals("")) {
                        txtInvoiceNote.setText(invNotes);
                    }
                } else {
                    layoutInvoice.setVisibility(View.GONE);
                }
            }*/
        } else {
            txtSaleamount.setText(Util.formatAmount(model.getmAmount()));
            txtNetamount.setText(Util.formatAmount(model.getmAmount()));
            txtTipamount.setText("0.00");
        }
        String cardHolderName = model.getmCardHolderName();
        String transactionStatus = model.getmTransactionStatus();
        String transactionAmount = Util.formatAmount(model.getmAmount()) + " AED";
//        String date = DateUtils.getRelativeTimeSpanString(model.getmDate()).toString();
        String date = Util.setDate(model.getmDate());
        String cardNumber = model.getmCardNumber();
        txtTransactionStatus.setText(transactionStatus);
        if (transactionStatus.equals("AUTHORIZED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.auth_blue));
//            btnVoid.setVisibility(View.VISIBLE);
            imgTstatus.setImageResource(R.drawable.done_150);
        } else if (transactionStatus.equals("CAPTURED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.green_success));
            imgTstatus.setImageResource(R.drawable.done_150);
        } else if (transactionStatus.equals("DECLINED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.dec_red));
            imgTstatus.setImageResource(R.drawable.cancel_150);
        } else if (transactionStatus.equals("VOIDED")) {
            layoutStatusview.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.void_grey));
            imgTstatus.setImageResource(R.drawable.cancel_150);
        }
//        String transId = "#" + model.getmTransactionId().substring(0, 8);

        txtTransactionamount.setText(transactionAmount);
        txtTransactiontime.setText(date);
        txtCardnumber.setText(cardNumber);
        txtCardholdername.setText(cardHolderName);
        if(model.getmCardImage() != null && !model.getmCardImage().isEmpty()) {
            Picasso.get().load(model.getmCardImage()).into(imgCardType);
        }
    }

    /**
     * Transaction listener
     **/
    private IPoyntTransactionServiceListener mTransactionServiceListener = new IPoyntTransactionServiceListener.Stub() {
        public void onResponse(Transaction _transaction, String s, PoyntError poyntError) throws RemoteException {
            final Transaction trans = _transaction;
//            Util.logLargeString(TAG, "trans# " + new Gson().toJson(trans));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (trans != null) {
//                        Util.closeProgressDialog();
                        assignUI(trans);
                    } else {
//                        Util.closeProgressDialog();
                        if (!cardType.equalsIgnoreCase("Alipay")) {
                            Util.showErrorToast(getActivity(), "Error fetching transaction!");
                        }
                        assignUI(null);
                    }
                }
            });
        }

        public void onLaunchActivity(Intent intent, String s) throws RemoteException {
            //do nothing
        }

        public void onLoginRequired() throws RemoteException {
            Log.d("OnOrderFragment", "onLoginRequired called");
        }

    };

    public void getTransaction(String transactionId) {
        Util.dTag(0, TAG, "getTransaction");
        Log.d(TAG, "transactionId# " + transactionId);
        if (transactionId != null) {
            if (MainActivity.isTransactionServiceConnected) {
                //Util.showProgressDialog(getActivity(), "Fetching Transaction");
                try {
                    MainActivity.transactionService.getTransaction(transactionId, UUID.randomUUID().toString(),
                            mTransactionServiceListener);
                } catch (RemoteException e) {
                    Util.closeProgressDialog();
                }
            } else {
                assignUI(null);
            }
        }
    }

/*    @OnClick(R.id.btn_void)
    public void showPinDialog() {

    }*/

    public void showTransactionFragment(String transactionId) {
        try {
            Util.dTag(0, TAG, "showTransactionFragment");
            Intent displayPaymentIntent = new Intent(Intents.ACTION_DISPLAY_PAYMENT);
            displayPaymentIntent.putExtra(Intents.INTENT_EXTRAS_TRANSACTION_ID, transactionId);
            startActivityForResult(displayPaymentIntent, DISPLAY_PAYMENT_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            Util.showErrorToast(getActivity(), "Something Went Wrong");
            Crashlytics.logException(e);
            MainActivity.sendErrorToServer(getContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }
    }


    @OnClick(R.id.btn_mercreceipt)
    public void printMerchantReceipt() {
        try {
            Util.dTag(1, TAG, "printMerchantReceipt");
            printReceipt("M");
        } catch (Exception e) {
            e.printStackTrace();
            Util.showErrorToast(getActivity(), "Something Went Wrong");
            Crashlytics.logException(e);
            MainActivity.sendErrorToServer(getContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }
    }

    @OnClick(R.id.btn_custreceipt)
    public void printCustomerReceipt() {
        try {
            Util.dTag(1, TAG, "printCustomerReceipt");
            String screenType = AppPreferences.getInstance().getScreenType(requireActivity());
            String receetPrintEnabled = AppPreferences.getInstance().getIsReceetEnabled(requireActivity());
            if("SGL".equals(screenType) && "T".equals(receetPrintEnabled)){
                collectCustomerReceipt("");
            }else {
                printReceipt("C");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Util.showErrorToast(getActivity(), "Something Went Wrong");
            Crashlytics.logException(e);
            MainActivity.sendErrorToServer(getContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }
    }

    public void printReceipt(final String mode) {
//        Util.dTag(0, TAG, "printReceipt..");
//        Log.d(TAG, mode);
//        Log.d("CARD TYPE", cardType);
//
//        WebServices.getInstance().getPrint(getActivity(), model.getmTransactionId(), mode, new ResponseListener() {
//            @Override
//            public void onResponse(final String responseStr) {
////                Util.logLargeString("response_" + mode, responseStr);
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            JSONObject object = new JSONObject(responseStr);
//                            JSONArray arr = object.getJSONArray("ResponseData");
//                            receiptData(arr.toString());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Util.logLargeString("json_exception" + mode, e.getMessage());
//                        }
//                    }
//                });
//            }
//
//            @Override
//            public void onFailure(String message) {
////                Util.logLargeString("failure_" + mode, message);
//            }
//        });


        if (cardType.equalsIgnoreCase("Alipay")) {
            Log.d("cardType", "Alipay");

            Map<String, String> params = new LinkedHashMap<>();
            params.put("sDevId", GeneralUtil.getDeviceSerial());
            params.put("strTxnid", model.getmTransactionId());
            params.put("strMode", "C");
            retrofitPrint.printDetails(params).enqueue(new Callback<PrintResponse>() {
                @Override
                @ParametersAreNonnullByDefault
                public void onResponse(Call<PrintResponse> call, Response<PrintResponse> response) {
                    try {
                        printOrderResponse(response.body().getResponseData());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Util.showErrorToast(getActivity(), "Something Went Wrong");
                        Crashlytics.logException(e);
                        MainActivity.sendErrorToServer(getContext(), e);
                        loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                        loGErr.setErrMsg(e.getMessage());
                        retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                            @Override
                            public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                                if (response.body() != null) {
                                    if (response.body().getSuccess()) {
                                        if (response.body().getResponseData().equals("OK")) {
                                            Log.e("Error send", "OK");
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                                Log.e("Error send", "Failed");
                            }
                        });
                    }
                }

                @Override
                @ParametersAreNonnullByDefault
                public void onFailure(Call<PrintResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } else {

            Map<String, String> params = new LinkedHashMap<>();
            params.put("sDevId", GeneralUtil.getDeviceSerial());
            params.put("strTxnid", model.getmTransactionId());
            params.put("strMode", mode);


//            Map<String, String> params = new LinkedHashMap<>();
//            params.put("sDevId", "P61SWD215HS037049");
//            params.put("strTxnid", "308ca79d-5abd-4163-8183-0a2b997c5ac7");
//            params.put("strMode", "C");
            retrofitPrint.printDetails(params).enqueue(new Callback<PrintResponse>() {
                @Override
                @ParametersAreNonnullByDefault
                public void onResponse(Call<PrintResponse> call, Response<PrintResponse> response) {
                    try {
                        printOrderResponseOtherMode(response.body().getResponseData());
                    } catch (Exception e) {
                        e.printStackTrace();
                        Util.showErrorToast(getActivity(), "Something Went Wrong");
                        Crashlytics.logException(e);
                        MainActivity.sendErrorToServer(getContext(), e);
                        loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                        loGErr.setErrMsg(e.getMessage());
                        retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                            @Override
                            public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                                if (response.body() != null) {
                                    if (response.body().getSuccess()) {
                                        if (response.body().getResponseData().equals("OK")) {
                                            Log.e("Error send", "OK");
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                                Log.e("Error send", "Failed");
                            }
                        });
                    }
                }

                @Override
                @ParametersAreNonnullByDefault
                public void onFailure(Call<PrintResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });


//            WebServices.getInstance().getPrint(getActivity(), model.getmTransactionId(), mode, new ResponseListener() {
//                @Override
//                public void onResponse(final String responseStr) {
////                Util.logLargeString("response_" + mode, responseStr);
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                JSONObject object = new JSONObject(responseStr);
//                                JSONArray arr = object.getJSONArray("ResponseData");
//                                receiptData(arr.toString());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                                Util.logLargeString("json_exception" + mode, e.getMessage());
//                            }
//                        }
//                    });
//                }
//
//                @Override
//                public void onFailure(String message) {
////                Util.logLargeString("failure_" + mode, message);
//                }
//            });
        }

    }

    private void printOrderResponse(List<PrintData> orderResponseReceipt) {
        if (orderResponseReceipt != null && orderResponseReceipt.size() > 0) {

            for (PrintData otherData1 : orderResponseReceipt) {
                String ipAddress = otherData1.getPRINTER_IP();

                if (ipAddress != null && ipAddress.length() > 0) {
                    Disposable d = Observable.create((ObservableOnSubscribe<Boolean>) emitter -> emitter.onNext(new PrintUtil2(requireActivity()).networkPrint(otherData1)))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(b -> {
                                if (!b)
                                    Util.showErrorToast(getActivity(), "Printer not available");
                            });
                } else {

                    Disposable d = Observable.create((ObservableOnSubscribe<PoyntPrintData>) emitter -> emitter.onNext(new PrintUtil2(requireActivity()).printOrderResponse(otherData1)))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(receipt -> {
                                if (receipt != null) {
                                    poyntPrint(UUID.randomUUID().toString(), receipt); //TODO change
                                }
                            });

                }

            }

        }
    }

    private void printOrderResponseOtherMode(List<PrintData> orderResponseReceipt) {
        if (orderResponseReceipt != null && orderResponseReceipt.size() > 0) {

            for (PrintData otherData1 : orderResponseReceipt) {
                String ipAddress = otherData1.getPRINTER_IP();
//                String ipAddress = "192.168.0.5";

                if (ipAddress != null && ipAddress.length() > 0) {
                    Disposable d = Observable.create((ObservableOnSubscribe<Boolean>) emitter -> emitter.onNext(new PrintUtil2(requireActivity()).networkPrintOtherMode(otherData1)))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(b -> {
                                if (!b)
                                    Util.showErrorToast(getActivity(), "Printer not available");
                            });
                } else {

                    Disposable d = Observable.create((ObservableOnSubscribe<PoyntPrintDataNestedImg>) emitter -> emitter.onNext(new PrintUtil2(requireActivity()).printOrderResponseOtherMode(otherData1)))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(receipt -> {
                                if (receipt != null) {
                                    poyntPrintForNestedImg(UUID.randomUUID().toString(), receipt); //TODO change
                                }
                            });

                }

            }

        }
    }


    private void poyntPrint(String id, PoyntPrintData receipt) {
        printers = findReceiptPrinter(getActivity());
        Util.dTag(0, TAG, "printReceipt");
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            boolean print = false;
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {


                    if (!print) {
                        print = true;
                        doPrintV2(binder, receipt);
                    }

//                    if (binder != null) {
//                        doPrintV2(binder, receipt);
//                        break;
//                    } else {
//                        Log.e(TAG, "No service connection found");
//                        initializeAccessories();
//                        doPrintV2(binder, receipt);
//                    }


                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }

    private void poyntPrintForNestedImg(String id, PoyntPrintDataNestedImg receipt) {
        printers = findReceiptPrinter(getActivity());
        Util.dTag(0, TAG, "printReceipt");
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            boolean print = false;
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {


                    if (!print) {
                        print = true;
                        doPrintForHeaderImage(binder, receipt);
                    }

//                    if (binder != null) {
//                        doPrintV2(binder, receipt);
//                        break;
//                    } else {
//                        Log.e(TAG, "No service connection found");
//                        initializeAccessories();
//                        doPrintV2(binder, receipt);
//                    }


                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }

    public void receiptData(String response) {
        Util.dTag(0, TAG, "receiptData");
        Log.d(TAG, "receiptData");
        try {
            JSONArray printArray = new JSONArray(response);
            for (int i = 0; i < printArray.length(); i++) {
                JSONObject pObj = printArray.getJSONObject(i);
                final String printerIP = pObj.getString("PRINTER_IP");
                final String printerPort = pObj.getString("PRINTER_PORT");
                final String printerName = pObj.getString("PRINTER_NAME");
                final String orderId = pObj.getString("PRINT_ORDER_ID");
                final String jobId = pObj.getString("PRINT_JOB_ID");
                final String printType = pObj.getString("PRINTER_TYPE");
                final JSONArray printHeader = pObj.getJSONArray("PRINT_HEADER");
                final JSONArray printBody = pObj.getJSONArray("PRINT_BODY");
                final JSONArray printFooter = pObj.getJSONArray("PRINT_FOOTER");

                List<PrintModel> header = new ArrayList<>();
                List<PrintModel> body = new ArrayList<>();
                List<PrintModel> footer = new ArrayList<>();
                Util.addIntoList(header, printHeader);
                Util.addIntoList(body, printBody);
                Util.addIntoList(footer, printFooter);
                if (printerIP.equals("")) {
                    Log.d(TAG, "No Ip Print");
                    printers = findReceiptPrinter(getActivity());
                    PrintedReceipt receipt = PrintUtil.getInstance(getActivity()).generateReceipt(printHeader, printBody, printFooter, cardType);
                    printReceipt(receipt, printerName);
//                    PrintedReceiptV2 receipt = PrintUtil.getInstance(getActivity()).generateReceiptV2(printHeader, printBody, printBody);
//                    printReceiptV2(receipt, printerName);
                } else {
                    //noinspection unchecked
                    PrintUtil.getInstance(context).PrintNew(printerIP, Integer.valueOf(printerPort), header, body, footer);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Initialize the print accessories
     **/
    public void initializeAccessories() {
        Util.dTag(0, TAG, "initializeAccessories");
        try {
            // initialize capabilityProviderServiceHelper
            accessoryProviderServiceHelper = new AccessoryProviderServiceHelper(getActivity());
            // connect to accessory manager service
            accessoryProviderServiceHelper.bindAccessoryManager(
                    new AccessoryProviderServiceHelper.AccessoryManagerConnectionCallback() {
                        @Override
                        public void onConnected(AccessoryProviderServiceHelper helper) {
                            // when connected check if we have any printers registered
                            if (helper.getAccessoryServiceManager() != null) {
                                AccessoryProviderFilter filter = new AccessoryProviderFilter(AccessoryType.PRINTER);
                                Log.d(TAG, "trying to get PRINTER accessory...");
                                try {
                                    // look up the printers using the filter
                                    helper.getAccessoryServiceManager().getAccessoryProviders(
                                            filter, poyntAccessoryManagerListener);
                                } catch (RemoteException e) {
                                    Log.e(TAG, "Unable to connect to Accessory Service", e);
                                }
                            } else {
                                Log.e(TAG, "Not connected with accessory service manager");
                            }
                        }

                        @Override
                        public void onDisconnected(AccessoryProviderServiceHelper accessoryProviderServiceHelper) {
                            Log.e(TAG, "Disconnected with accessory service manager");
                        }
                    });
        } catch (SecurityException e) {
            Log.e(TAG, "Failed to connect to capability or accessory manager", e);
        }
    }

    public void printReceipt(PrintedReceipt receipt, String printerName) {
        Util.dTag(0, TAG, "printReceipt");
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrint(binder, receipt);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrint(binder, receipt);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }


    public void printReceiptV2(PrintedReceiptV2 receipt, String printerName) {
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrintV2(binder, receipt, printerName);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrintV2(binder, receipt, printerName);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        requireActivity().bindService(Intents.getComponentIntent(Intents.COMPONENT_POYNT_RECEIPT_PRINTING_SERVICE),
                receiptServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
        showWelcomeScreen();
        requireActivity().unbindService(receiptServiceConnection);
    }

    private IPoyntReceiptPrintingService receiptPrintingService;
    private IPoyntReceiptPrintingServiceListener receiptPrintingServiceListener = new IPoyntReceiptPrintingServiceListener.Stub() {
        @Override
        public void printQueued() throws RemoteException {
            Log.d(TAG, "Receipt queued");
        }

        @Override
        public void printFailed(PrinterStatus status) throws RemoteException {
            Log.d(TAG, "Receipt printing failed");
        }
    };

    private IPoyntReceiptSendListener receiptSendListener = new IPoyntReceiptSendListener.Stub() {

        @Override
        public void success() throws RemoteException {
            Log.d(TAG, "Receipt sent");
        }

        @Override
        public void failure() throws RemoteException {
            Log.d(TAG, "Receipt sending failed");
        }
    };

    private ServiceConnection receiptServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            receiptPrintingService = IPoyntReceiptPrintingService.Stub.asInterface(iBinder);
            Log.d(TAG, "Receiptprintingservice connection established");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            receiptPrintingService = null;
            Log.d(TAG, "Receiptprintingservice connection disconnected");
        }
    };

    public void doPrintV2(IBinder binder, PoyntPrintData receipt) {
        Util.dTag(0, TAG, "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

                if (receipt.getLogo() != null) {
                    receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getLogo(), new IPoyntReceiptPrintingServiceListener.Stub() {
                        @Override
                        public void printQueued() throws RemoteException {
                            if (receipt.getLogo() != null) {

                                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getHead(), new IPoyntPrinterServiceListener.Stub() {
                                    @Override
                                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());

                                        if (receipt.getQr() != null) {
                                            receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getQr(), new IPoyntReceiptPrintingServiceListener.Stub() {
                                                @Override
                                                public void printQueued() throws RemoteException {
                                                    if (receipt.getBarcode() != null) {
                                                        receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getBarcode(), new Stub() {
                                                            @Override
                                                            public void printQueued() throws RemoteException {

                                                                if (receipt.getFooter() != null) {
                                                                    printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getFooter(), new IPoyntPrinterServiceListener.Stub() {
                                                                        @Override
                                                                        public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                                                            Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                                                                        }

                                                                    });
                                                                }

                                                                receipt.setFooter(null);
                                                            }

                                                            @Override
                                                            public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                                                            }
                                                        });
                                                    }
                                                    receipt.setBarcode(null);
                                                }

                                                @Override
                                                public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                                                }
                                            });
                                        }
                                        receipt.setQr(null);

                                    }

                                });

                                receipt.setLogo(null);
                            }
                        }

                        @Override
                        public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                        }
                    });
                } else {
                    printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getHead(), new IPoyntPrinterServiceListener.Stub() {
                        @Override
                        public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                            Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());

                            if (receipt.getQr() != null) {
                                receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getQr(), new IPoyntReceiptPrintingServiceListener.Stub() {
                                    @Override
                                    public void printQueued() throws RemoteException {
                                        if (receipt.getBarcode() != null) {
                                            receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getBarcode(), new Stub() {
                                                @Override
                                                public void printQueued() throws RemoteException {

                                                    if (receipt.getFooter() != null) {
                                                        printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getFooter(), new IPoyntPrinterServiceListener.Stub() {
                                                            @Override
                                                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                                                Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                                                            }

                                                        });
                                                    }

                                                    receipt.setFooter(null);
                                                }

                                                @Override
                                                public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                                                }
                                            });
                                        }
                                        receipt.setBarcode(null);
                                    }

                                    @Override
                                    public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                                    }
                                });
                            }
                            receipt.setQr(null);

                        }

                    });
                }


            } catch (Exception e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    public void doPrint(IBinder binder, PrintedReceipt receipt) {
        Util.dTag(0, TAG, "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                });
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    public void doPrintV2(IBinder binder, PrintedReceiptV2 receipt, String printerName) {
        Log.i("method", "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

/*                printerService.printReceipt(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                },null);*/

                printerService.printReceiptForPrinter(printerName, UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }
                }, null);

              /*  printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                });*/
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    public void doPrintForHeaderImage(IBinder binder, PoyntPrintDataNestedImg receipt) {
        Util.dTag(0, TAG, "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

                if (receipt.getImage() != null) {
                    receiptPrintingService.printBitmap(UUID.randomUUID().toString(), receipt.getImage(), new IPoyntReceiptPrintingServiceListener.Stub() {
                        @Override
                        public void printQueued() throws RemoteException {

                            if (receipt.getImage() != null) {

                                if (receipt.getBody() != null) {
                                    printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getBody(), new IPoyntPrinterServiceListener.Stub() {
                                        @Override
                                        public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                            Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());

//                                            if (receipt.getFooter() != null) {
//                                                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getFooter(), new IPoyntPrinterServiceListener.Stub() {
//                                                    @Override
//                                                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
//                                                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
//                                                    }
//
//                                                });
//                                            }
//                                            receipt.setFooter(null);

                                        }

                                    });
                                }
                                receipt.setBody(null);
                            }
                            receipt.setImage(null);
                        }

                        @Override
                        public void printFailed(PrinterStatus printerStatus) throws RemoteException {

                        }
                    });
                } else {
                    printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getBody(), new IPoyntPrinterServiceListener.Stub() {
                        @Override
                        public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                            Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());

//                            if (receipt.getFooter() != null) {
//                                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt.getFooter(), new IPoyntPrinterServiceListener.Stub() {
//                                    @Override
//                                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
//                                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
//                                    }
//
//                                });
//                            }
//                            receipt.setFooter(null);

                        }

                    });
                }


            } catch (Exception e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    /**
     * Find the order receipt printers
     */
    private Set<String> findReceiptPrinter(Context context) {
        Util.dTag(0, TAG, "findReceiptPrinter");
        Set<String> set = ReceiptPrintingPref.readReceiptPrefsFromDb(context, Constants.ReceiptPreference.PREF_PAYMENT_RECEIPT);
        if (set != null && set.size() > 0) {
            for (String s : set) {
                Log.d("_printer_#", s);
            }
        } else {
            Log.d(TAG, "No receipt printers found");
        }
        return set;
    }

    public void collectCustomerReceipt(final String input) {
        Util.dTag(0, TAG, "collectCustomerReceipt");
        try {
            Bundle options = new Bundle();
            options.putString(Intents.EXTRA_TITLE, "Receipt Options");
            options.putString(Intents.EXTRA_FOOTER_TEXT, "Thank you");
            List<ReceiptOption> receiptOptions = new ArrayList<>();

            ReceiptOption option1 = new ReceiptOption();
            option1.setType(ReceiptType.PAPER);
            option1.setId("PAPER");
            option1.setLabel("By Paper");
            option1.setData("By Paper");
            receiptOptions.add(option1);

            if (input != null && input.equals("")) {
                ReceiptOption option4 = new ReceiptOption();
                option4.setType(ReceiptType.EMAIL);
                option4.setId("RECEET");
                option4.setLabel("RECEET");
//                option4.setData("RECEET");
                receiptOptions.add(option4);
            }
            ReceiptOption option2 = new ReceiptOption();
            option2.setType(ReceiptType.NONE);
            option2.setId("NO-PAPER");
            option2.setLabel("No Paper");
            option2.setData("No Paper");
            receiptOptions.add(option2);


            MainActivity.poyntSecondScreenServiceV2.captureReceiptChoice(null,
                    receiptOptions, options, new IPoyntReceiptChoiceListener.Stub() {
                        @Override
                        public void onReceiptChoice(ReceiptType receiptType, String s, String data) throws RemoteException {
                            if (receiptType == ReceiptType.PAPER) {
                                showConfirmation("Thank you.");

                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        printReceipt("C");
                                    }
                                }, 2000);
                            } else if (receiptType == ReceiptType.EMAIL) {
                                if (jsonString != null && !jsonString.equals("")) {
                                    requireActivity().runOnUiThread(() -> createConnection(jsonString));
                                } else {
                                    showConfirmation(s);
                                }
                            }
                        }
                    });
        } catch (RemoteException e) {
            Util.showErrorToast(getActivity(), e.getMessage());
            Log.e(TAG, e.getMessage());
        }
    }

    private void showConfirmation(String message) {
        Util.dTag(0, TAG, "showConfirmation");
        try {
            Bitmap customBackgroundImage = BitmapFactory.decodeResource(getResources(),
                    R.drawable.thank_you_bg);
            Bundle options = new Bundle();
            options.putParcelable(Intents.EXTRA_BACKGROUND_IMAGE, customBackgroundImage);
            MainActivity.poyntSecondScreenServiceV2.displayMessage(message, options);
            showWelcomeScreen();
        } catch (RemoteException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void showWelcomeScreen() {
        Util.dTag(0, TAG, "showWelcomeScreen");
        if (MainActivity.isSecondScreenServiceConnected) {
            try {
                MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
            } catch (RemoteException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void showEmptyScreen() {
        Util.dTag(0, TAG, "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });
    }

    public void showPoyntFragment(StoreTransactionRequest paymentDetails) {
        Util.dTag(0, TAG, "showPoyntFragment");
        if (currentTransaction != null) {
//            Util.logLargeString(TAG, "trans#" + new Gson().toJson(currentTransaction));
            if (currentTransaction.getStatus().equals(TransactionStatus.VOIDED)) {
                Util.showWarningToast(getActivity(), "Transaction Already voided!");
                return;
            }
        }

        List<UserData> usersList = AppPreferences.getInstance().getUsersList(getActivity());

        if (usersList != null && usersList.size() > 0) {
            PinDialog pinDialog = new PinDialog(context, 1, AppPreferences.getInstance().getUsersList(context), new PinListener() {
                @Override
                public void isValidUser(String pin, boolean valid) {
                    if (valid) {
                        //show the payment fragment
                        if (cardType.equalsIgnoreCase("Alipay")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
                            builder.setMessage("Are you sure to void this transaction")
                                    .setCancelable(true)
                                    .setPositiveButton("Yes", (dialog, id) -> {
                                        aliPayVoid(paymentDetails);
                                    })
                                    .setNegativeButton("No", (dialog, id) -> {
                                    });
                            builder.setCancelable(false);
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            showTransactionFragment(model.getmTransactionId());
                        }
                    }
                }
            });
            pinDialog.show();
        } else {
            Util.showErrorToast(getActivity(), "No Users available, Please Sync Users from Settings!");
        }
    }

    public void showPoyntFragment() {
        Util.dTag(0, TAG, "showPoyntFragment");
        if (currentTransaction != null) {
//            Util.logLargeString(TAG, "trans#" + new Gson().toJson(currentTransaction));
            if (currentTransaction.getStatus().equals(TransactionStatus.VOIDED)) {
                Util.showWarningToast(getActivity(), "Transaction Already voided!");
                return;
            }
        }

        List<UserData> usersList = AppPreferences.getInstance().getUsersList(getActivity());

        if (usersList != null && usersList.size() > 0) {
            PinDialog pinDialog = new PinDialog(context, 1, AppPreferences.getInstance().getUsersList(context), new PinListener() {
                @Override
                public void isValidUser(String pin, boolean valid) {
                    if (valid) {
                        //show the payment fragment
                        showTransactionFragment(model.getmTransactionId());
                    }
                }
            });
            pinDialog.show();
        } else {
            Util.showErrorToast(getActivity(), "No Users available, Please Sync Users from Settings!");
        }
    }

    public void updateVoidedScreen(final String transactionId) {
        Util.dTag(0, TAG, "updateVoidedScreen");
        Util.showProgressDialog(context, "Updating Transaction");
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getTransactionById(transactionId);
            }
        }, 5000);
    }

    public void getTransactionById(String id) {
        Util.dTag(0, TAG, "getTransactionById");
        WebServices.getInstance().getTxnById(getActivity(), id, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                try {
                    JSONObject obj = new JSONObject(responseStr);
                    if (obj.getBoolean("IsSuccess")) {
                        JSONObject responseData = obj.getJSONObject("ResponseData");
                        long sAmt = responseData.optLong("decSettleAmt");
                        double actualVal = sAmt / 100;
                        JSONArray tArray = responseData.getJSONArray("Transactions");
                        TransactionModel tModel = null;
                        if (tArray != null && tArray.length() > 0) {
                            for (int i = 0; i < tArray.length(); i++) {
                                JSONObject tObject = tArray.getJSONObject(i);
                                long amount = tObject.optLong("mAmount");
                                double actVal = 0;
                                if (amount > 0) {
                                    actVal = amount / 100;
                                }
                                String cardNumber = tObject.optString("mCardNumber");
                                String cardType = tObject.optString("mCardType");
                                String cardImgType = tObject.optString("mCardTypeImg");
                                String transactionId = tObject.optString("mTransactionId");
                                String transactionStatus = tObject.optString("mTransactionStatus");
                                String type = tObject.optString("mType");
                                String cardHolderName = tObject.optString("mCardHolderName");
                                long date = tObject.optLong("mDate");
                                String transactionType = tObject.optString("mPayType");
                                String mDisplayNo = tObject.optString("mDisplayNo");
                                tModel = new TransactionModel(transactionId, date, type, transactionType, cardType,
                                        cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo);
                            }
                            if (tModel != null) {
//                                Util.logLargeString("successed_trans", new Gson().toJson(tModel));
                                Util.showSuccessToast(getActivity(), "Transaction Void Successfully");
                                if (Util.transactionList.size() > 0) {
                                    Util.transactionList.clear();
                                    Util.settleAmount = "0.00";
                                    Util.totalCashAmount = "0.00";
                                    Util.totalCardAmount = "0.00";
                                    Util.totalTipAmount = "0.00";
                                }
                                if (Util.historyList.size() > 0) {
                                    Util.historyList.clear();
                                    Util.lastSelectedFilter = "ALL";
                                    Util.currentFilter = "ALL";
                                }
                                listener.reloadTransaction(tModel);

                                //Receet data change
                                JSONObject receetData = obj.getJSONObject("ReceetData");
                                if (receetData != null && receetData.length() != 0) {
                                    errorMsg = receetData.getString("ErrMsg");
                                    JSONObject qrResponseData = receetData.getJSONObject("ResponseData");
                                    webSocketURL = qrResponseData.getString("WebSocketURL");
                                    fetchURL = qrResponseData.getString("FetchURL");
                                    tokenKey = qrResponseData.getString("TokenKey");
                                    qRDispTimeSec = Integer.parseInt(qrResponseData.getString("QRDispTimeSec"));
                                    odrTxnJSON = new Gson().toJson(qrResponseData.get("OdrTxnJSON"));
                                    jsonString = qrResponseData.getString("OtherData1");
                                    collectCustomerReceiptCheck(errorMsg);
                                } else {
                                    Util.showErrorToast(getActivity(), "No Receet");
                                }


                            } else {
                                Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again from history");
                            }
                        } else {
                            Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again!");
                        }
                    } else {
                        Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again!");
                    }
                } catch (JSONException e) {
                    Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again!");
                }
            }

            @Override
            public void onFailure(String message) {
                Util.dTag(0, TAG, message);
            }
        });
    }

    private void aliPayVoid(StoreTransactionRequest paymentDetails) {
        Log.d("transactionId", transactionId);
        Util.showProgressDialog(requireContext(), "Voiding Transaction");
        Single.create((SingleOnSubscribe<String>) emitter -> {
            String result = "Result";
            FetchResponse fetchResponse = fetchDeviceDetails(0);
            if (fetchResponse == null) {
                Util.showErrorToast(getActivity(), "can not fetch device details");
            }

            if (paymentDetails != null && fetchResponse != null) {
                StoreTransactionRequest storeResponse = storeTransactions("F", paymentDetails);
                if (storeResponse != null) {
                    OrderRefundResponse requestResult = null;
                    try {
                        requestResult = orderRefund(paymentDetails, fetchResponse);
                        if (requestResult.getData().getErrMsg() != null && requestResult.getData().getErrMsg().length() > 0) {
                            Util.showErrorToast(getActivity(), requestResult.getData().getErrMsg());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (requestResult == null) {
                        Util.showInfoToast(requireContext(), "Revoke failed!");
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);

                        storeResponse.setOALKSHERREFUNDNO(requestResult.getData().getKsherRefundNo());
                        storeResponse.setOALCHANNELREFUNDNO(requestResult.getData().getChannelRefundNo());
                        storeResponse.setOALCASHREFUNDFEE(String.valueOf(requestResult.getData().getCashRefundFee()));
                        storeResponse.setOALREFUNDTIME(sdf.format(Calendar.getInstance().getTime()));

                        storeResponse.setOALREFUNDFEE(requestResult.getData().getRefundFee());
                        storeResponse.setOALMCHREFUNDNO(requestResult.getData().getMchRefundNo());
                        storeResponse.setOALRESULT(requestResult.getData().getResult());

                        storeResponse.setOALERRCODE(requestResult.getData().getErrCode());
                        storeResponse.setOALERRMSG(requestResult.getData().getErrMsg());

                        if (requestResult.getData().getResult().equalsIgnoreCase("SUCCESS")) {
                            storeResponse.setOALSTSYN("Y");
                            result = "SUCCESS";
                        } else
                            storeResponse.setOALSTSYN("X");

                        boolean updateStatus = updateTransactions(storeResponse);
                        if (updateStatus) {
                            Util.showInfoToast(requireContext(), "Completed");
                        } else {
                            Util.showErrorToast(requireContext(), "ServerError");
                        }
                    }
                } else
                    Util.showErrorToast(requireContext(), "Revoke failed");
            }

            emitter.onSuccess(result);
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(String s) {
                        Util.closeProgressDialog();
                        if (s.equalsIgnoreCase("SUCCESS")) {
                            layoutStatusview.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.void_grey));
                            imgTstatus.setImageResource(R.drawable.cancel_150);
                            txtTransactionStatus.setText("VOIDED");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Util.closeProgressDialog();
                    }
                });
    }

    private StoreTransactionRequest storeTransactions(String oalType, StoreTransactionRequest paymentDetails) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);
        StoreTransactionRequest storeRequest = new StoreTransactionRequest();
        storeRequest.setOALAUTHCODE(paymentDetails.getOALAUTHCODE());
        storeRequest.setOALPDEVICEID(GeneralUtil.getDeviceSerial());
        storeRequest.setOALAPPID(paymentDetails.getOALAPPID());
        storeRequest.setOALATTACH("");
        storeRequest.setOALCHANNEL(paymentDetails.getOALCHANNEL());
        storeRequest.setOALCHANNELORDERNO(null);
        storeRequest.setOALFEETYPE(paymentDetails.getOALFEETYPE());
        storeRequest.setOALKSHERORDERNO(null);
        storeRequest.setOALMCHORDERNO(paymentDetails.getOALMCHORDERNO());
        storeRequest.setOALREFUNDFEE(0);
        storeRequest.setOALMCHREFUNDNO(null);
        storeRequest.setOALOPERATORID("0");
        storeRequest.setOALTOTALFEE(paymentDetails.getOALTOTALFEE());
        storeRequest.setOALCRDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALUPDDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALSTDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALTIMESTART(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALCRUID("1");
        storeRequest.setOALUPDUID("1");
        storeRequest.setOALBATCHNO(paymentDetails.getOALBATCHNO());
        storeRequest.setOALTYPE(oalType);
        storeRequest.setOALSTSYN("N"); // set y if success, x if failed


        Call<StoreTransactionResponse> call = componentTerminal.storeTransaction(storeRequest);
        try {
            Response<StoreTransactionResponse> response = call.execute();
            StoreTransactionResponse res = response.body();
            if (res != null && res.getIsSuccess()) {
                return res.getResponseData().get(0);
            } else
                Util.showErrorToast(getActivity(), "Store Transaction failed");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private OrderRefundResponse orderRefund(StoreTransactionRequest store, FetchResponse fetch) throws IOException {
        Util.showProgressDialog(requireContext(), "Processing Transaction");

        long amount = store.getOALTOTALFEE();
        String channel = store.getOALCHANNEL().toLowerCase();
        String appid = store.getOALAPPID();
        String nonceStr = Util.randomAlphanumeric(4);
        String fee_type = store.getOALFEETYPE();
        String mchRefundNo = Util.getNextRefundNo();

        java.util.Map<String, String> paras = new java.util.HashMap<>();
        paras.put("appid", appid);
        paras.put("channel", channel);
        paras.put("mch_order_no", store.getOALMCHORDERNO());
        paras.put("ksher_order_no", store.getOALKSHERORDERNO());
        if (store.getOALCHANNELORDERNO() != null)
            paras.put("channel_order_no", store.getOALCHANNELORDERNO());
        paras.put("fee_type", fee_type);
        paras.put("total_fee", "" + amount);
        paras.put("refund_fee", "" + amount);
        paras.put("mch_refund_no", mchRefundNo);
        paras.put("device_id", GeneralUtil.getDeviceSerial());
        paras.put("operator_id", "");
        paras.put("attach", "");
        paras.put("nonce_str", nonceStr);
        paras.put("version", "3.0.0");


        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());

        if (appid.isEmpty() || nonceStr.isEmpty() || amount == 0 ||
                fee_type.isEmpty() || mchRefundNo.isEmpty() || sign == null || sign.isEmpty()) {
            return null;
        }

        Call<OrderRefundResponse> call = componentApliPay.orderRefund(appid, channel, store.getOALMCHORDERNO(), store.getOALKSHERORDERNO(),
                store.getOALCHANNELORDERNO(), fee_type, "" + amount, "" + amount, mchRefundNo, GeneralUtil.getDeviceSerial(), "",
                "", nonceStr, "3.0.0", sign);

        Response<OrderRefundResponse> response = call.execute();

        OrderRefundResponse res = response.body();
        if (res != null && res.getData() != null) {
            return res;
        }

        return null;
    }

    private OrderRevokeResponse orderRevoke(StoreTransactionRequest store, FetchResponse fetch) throws IOException {
        String channel = store.getOALCHANNEL().toLowerCase();
        String appid = store.getOALAPPID();
        String nonceStr = Util.randomAlphanumeric(4);

        Log.d("mch_order_no", store.getOALMCHORDERNO());
        Log.d("ksher_order_no", store.getOALKSHERORDERNO());
        Log.d("channel_order_no", store.getOALCHANNELORDERNO());

        java.util.Map<String, String> paras = new java.util.HashMap<>();
        paras.put("appid", appid);
        paras.put("channel", channel);
        paras.put("mch_order_no", store.getOALMCHORDERNO());
        paras.put("ksher_order_no", store.getOALKSHERORDERNO());
        paras.put("channel_order_no", store.getOALCHANNELORDERNO());
        paras.put("nonce_str", nonceStr);
        paras.put("version", "3.0.0");

        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());

        Call<OrderRevokeResponse> call = componentApliPay.orderRevoke(appid, channel, store.getOALMCHORDERNO(),
                store.getOALKSHERORDERNO(), store.getOALCHANNELORDERNO(), nonceStr, "3.0.0", sign);

        Response<OrderRevokeResponse> response = call.execute();

        OrderRevokeResponse res = response.body();
        return res;

    }

    private boolean updateTransactions(StoreTransactionRequest storeRequest) {
        Call<StoreTransactionResponse> call = componentTerminal.updateTransaction(storeRequest);
        try {
            Response<StoreTransactionResponse> response = call.execute();
            StoreTransactionResponse res = response.body();
            if (res != null && res.getIsSuccess()) {
                Util.showInfoToast(getActivity(), "Transaction Updated");
                return true;
            } else
                Util.showErrorToast(getActivity(), "Update Transaction failed");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private StoreTransactionRequest alipayPaymentDetails(String murchentNo, String ksherOrderNo) throws IOException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("MurchentNo", murchentNo);
        params.put("KsherOrderNo", ksherOrderNo);

        Call<StoreTransactionResponse> call = componentTerminal.getAlipayPaymentDetails(params);
        try {
            Response<StoreTransactionResponse> response = call.execute();
            StoreTransactionResponse res = response.body();
            if (res != null && res.getIsSuccess()) {
                return res.getResponseData().get(0);
            } else
                Util.showErrorToast(getActivity(), res.getErrMsg());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void alipayPaymentDetailsAPI() {
        Util.showProgressDialog(requireContext(), "Fetching details");

        Map<String, String> params = new LinkedHashMap<>();
        params.put("MurchentNo", transactionId);
        params.put("KsherOrderNo", "");

        Call<StoreTransactionResponse> call = componentTerminal.getAlipayPaymentDetails(params);

        call.enqueue(new Callback<StoreTransactionResponse>() {
            @Override
            @ParametersAreNonnullByDefault
            public void onResponse(Call<StoreTransactionResponse> call, Response<StoreTransactionResponse> response) {
                try {
                    Util.closeProgressDialog();
                    StoreTransactionResponse res = response.body();
                    if (res != null && res.getIsSuccess()) {
                        showPoyntFragment(res.getResponseData().get(0));
                    } else
                        Util.showErrorToast(getActivity(), res.getErrMsg());
                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showErrorToast(getActivity(), "Something Went Wrong");
                    Crashlytics.logException(e);
                    MainActivity.sendErrorToServer(requireContext(), e);
                    loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                    loGErr.setErrMsg(e.getMessage());
                    retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                        @Override
                        public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getSuccess()) {
                                    if (response.body().getResponseData().equals("OK")) {
                                        Log.e("Error send", "OK");
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                            Log.e("Error send", "Failed");
                        }
                    });
                }
            }

            @Override
            @ParametersAreNonnullByDefault
            public void onFailure(Call<StoreTransactionResponse> call, Throwable t) {
                Util.closeProgressDialog();
                Util.showErrorToast(getActivity(), "Can not get payment details");
            }
        });
    }

    private FetchResponse fetchDeviceDetails(long amount) {

        Map<String, String> params = new LinkedHashMap<>();
        params.put("DeviceID", GeneralUtil.getDeviceSerial());
        params.put("Amount", String.valueOf(amount));

        Call<FetchDeviceDetailsResponse> call = componentTerminal.fetchDeviceDetails(params);
        try {
            Response<FetchDeviceDetailsResponse> response = call.execute();
            FetchDeviceDetailsResponse res = response.body();
            if (res != null && res.getIsSuccess()) {
                if (res.getResponseData() != null && res.getResponseData().size() > 0) {
                    return res.getResponseData().get(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.sendErrorToServer(requireContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }

        return null;
    }

    //  Receet chnage
    private void fetchOrderDetails(String transactionID) {
        //Util.showProgressDialog(requireContext(), "Fetching order details");
        handler.removeCallbacksAndMessages(null);
        Single<String> single = Single.create((SingleOnSubscribe<String>) emitter -> {
            ReceetResponse responses = null;
            for (int i = 0; i < 3; i++){
                responses = _fetchOrderDetails(transactionID);
                if(responses != null){
                    break;
                }
                Thread.sleep(1800);
            }
            if (responses != null) {
                ReceetData receetData = responses.getReceetData();
                if (receetData != null) {
                    errorMsg = responses.getErrMsg();
                    if(receetData.getResponseData() != null){
                        webSocketURL = receetData.getResponseData().getWebSocketURL();
                        fetchURL = receetData.getResponseData().getFetchURL();
                        tokenKey = receetData.getResponseData().getTokenKey();
                        odrTxnJSON = new Gson().toJson(receetData.getResponseData().getOdrTxnJSON());
                        jsonString = receetData.getResponseData().getOtherData1();
                        qRDispTimeSec = Integer.parseInt(receetData.getResponseData().getQRDispTimeSec());
                    }
                }
            }
            emitter.onSuccess(errorMsg);
        }) .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        single.subscribe(new SingleObserver<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(String responses) {
                Util.closeProgressDialog();
                collectCustomerReceiptCheck(responses);
            }

            @Override
            public void onError(Throwable e) {
                Util.closeProgressDialog();
                collectCustomerReceiptCheck(null);
                Util.showErrorToast(getActivity(), "Unable to fetch order detail.");
            }
        });
    }

    private ReceetResponse _fetchOrderDetails(String transactionID) {
        HashMap param = new HashMap<String, String>();
        param.put("sDevId", GeneralUtil.getDeviceSerial());
        param.put("strTxnId", transactionID);

        try {
//            Retrofit mRetrofitPrint = new APIBuilder(requireContext()).retrofitReceet();
//            retrofit2.Call<ReceetResponse> call = mRetrofitPrint.create(APIComponent.class).getReceetOrderDetail(param);

            retrofit2.Call<ReceetResponse> call = retrofitPrint.getTrxnById(param);
            retrofit2.Response<ReceetResponse> apiRes = call.execute();
            return apiRes.body();
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return null;
    }

    private void showQrCode(JSONObject jsonObject) {
        requireActivity().runOnUiThread(() -> {
            try {
                String url = fetchURL + jsonObject.getString("genericReceiptId");
                Log.d("QR", url);

                String screenType = AppPreferences.getInstance().getScreenType(requireActivity());
                if("SGL".equals(screenType)){
                    Bitmap qr = GeneralUtil.generate(url, BarcodeFormat.QR_CODE, 350, 350);
                    showQRDialog(qr);
                }else {
                    Bitmap qr = GeneralUtil.generate(url, BarcodeFormat.QR_CODE, 350, 200);
                    MainActivity.poyntSecondScreenService.displayMessage("", qr);
                }

                handler.postDelayed(() -> {
                    showConfirmation("Thank you.");
                    closeQrDialog();
                    webSocket.close(1000, "");
                }, qRDispTimeSec * 1000);


            } catch (Exception e) {
                e.printStackTrace();
                Crashlytics.logException(e);
                MainActivity.sendErrorToServer(requireContext(), e);
                loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                loGErr.setErrMsg(e.getMessage());
                retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                    @Override
                    public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getSuccess()) {
                                if (response.body().getResponseData().equals("OK")) {
                                    Log.e("Error send", "OK");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                        Log.e("Error send", "Failed");
                    }
                });
            }
        });
    }

    private void createConnection(String input) {

        try {
            Util.showProgressDialog(requireContext(), "Fetching receet");

            Request request = new Request.Builder().url(webSocketURL).build();
            if(client == null){
                client = apiBuilder.okHttpClientHttps();
            }
            webSocket = client.newWebSocket(request, new WebSocketListener() {

                @Override
                public void onClosing(WebSocket webSocket, int code, String reason) {
                    super.onClosing(webSocket, code, reason);
                    requireActivity().runOnUiThread(() -> {
                        Util.showErrorToast(getActivity(), "web socket connection closing");
                        Util.closeProgressDialog();
                    });
                }

                @Override
                public void onFailure(WebSocket webSocket, Throwable t, okhttp3.Response response) {
                    super.onFailure(webSocket, t, response);
                    requireActivity().runOnUiThread(() -> {
                        Util.showErrorToast(getActivity(), "web socket connection failure");
                        Util.closeProgressDialog();
                    });
                }

                @Override
                public void onMessage(WebSocket webSocket, String text) {
                    super.onMessage(webSocket, text);
                    requireActivity().runOnUiThread(() -> handleMessage(text, input));
                }

            });

            MainActivity.poyntSecondScreenServiceV2.displayMessage("Please wait..", new Bundle());

        } catch (Exception e) {
            Util.closeProgressDialog();
            Util.showErrorToast(getActivity(), e.getMessage());
            e.printStackTrace();
            Crashlytics.logException(e);
            MainActivity.sendErrorToServer(requireContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }
    }

    private void handleMessage(String message, String input) {
        try {
            JSONObject jsonObject = new JSONObject(message);
            switch (jsonObject.getInt("action")) {
                case 4027: {
                    Log.d("generic code", jsonObject.toString());
                    Util.closeProgressDialog();
                    Util.showSuccessToast(getActivity(), "Qr Generated.");
                    showQrCode(jsonObject);
                    break;
                }
                case 4023: {
                    Log.d("4023", input);
                    sendReceetData(input);
                    break;
                }
                case 4022: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "Authentication Failed.");
                    break;
                }
                case 4021: {
                    authenticate();
                    break;
                }
                case 4019: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "Receet Connection closed.");
                    break;
                }
                case 4007: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "Required parameter missing.");
                    break;
                }
                case 4008: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "invalid billing address.");
                    break;
                }
                case 4009: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "invalid order.");
                    break;
                }
                case 4010: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "invalid order Item.");
                    break;
                }
                case 4011: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "invalid order Items.");
                    break;
                }
                case 4025: {
                    Util.closeProgressDialog();
                    Util.showSuccessToast(getActivity(), jsonObject.getString("message"));
                    handler.removeCallbacksAndMessages(null);
                    webSocket.close(1000, "");
                    showConfirmation("Thank you.");
                    closeQrDialog();
                    break;
                }
                default: {
                    Util.closeProgressDialog();
                    showConfirmation("Thank you.");
                    Util.showErrorToast(getActivity(), "Receet Connection failed.");
                    break;
                }
            }
        } catch (Exception e) {
            Util.closeProgressDialog();
            Util.showErrorToast(getActivity(), e.getMessage());
            e.printStackTrace();
            Crashlytics.logException(e);
            MainActivity.sendErrorToServer(requireContext(), e);
            loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
            loGErr.setErrMsg(e.getMessage());
            retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                @Override
                public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getResponseData().equals("OK")) {
                                Log.e("Error send", "OK");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                    Log.e("Error send", "Failed");
                }
            });
        }
    }

    private void sendReceetData(String input) {
        webSocket.send(input);
    }

    private void authenticate() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("action", 4020);
        jsonObject.put("access_token", tokenKey);
        webSocket.send(jsonObject.toString());
    }

    private void collectCustomerReceiptCheck(String str){
        String screenType = AppPreferences.getInstance().getScreenType(requireActivity());
        if("DBL".equals(screenType)){
            collectCustomerReceipt(str);
        }
    }

    AlertDialog qrDialog;
    private void showQRDialog(Bitmap qr) {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            builder.setPositiveButton("CLOSE", (dialog, which) -> {
            });
            qrDialog = builder.create();
            LayoutInflater inflater = getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.qr_dialog, null);
            qrDialog.setView(dialogLayout);
            qrDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


            qrDialog.show();
            ImageView image = (qrDialog.findViewById(R.id.qr_image_view));
            image.setImageBitmap(qr);

        }catch (Exception ignore){ }
    }

    private void closeQrDialog(){
        try{
            if(qrDialog != null){
                qrDialog.dismiss();
            }
        }catch (Exception ignore){ }
    }
}
