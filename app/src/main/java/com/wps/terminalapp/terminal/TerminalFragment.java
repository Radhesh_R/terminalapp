package com.wps.terminalapp.terminal;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.appolica.flubber.Flubber;
import com.google.gson.Gson;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.api.APIBuilder;
import com.wps.terminalapp.api.APIComponent;
import com.wps.terminalapp.custom.AutoFormatEditText;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.DecimalInputFilter;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.listeners.ErrorListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.models.FetchDeviceDetailsResponse;
import com.wps.terminalapp.models.FetchResponse;
import com.wps.terminalapp.models.OrderRefundResponse;
import com.wps.terminalapp.models.OrderRequestResponse;
import com.wps.terminalapp.models.OrderRequestResponseData;
import com.wps.terminalapp.models.OrderRevokeResponse;
import com.wps.terminalapp.models.StoreResponse;
import com.wps.terminalapp.models.StoreTransactionRequest;
import com.wps.terminalapp.models.StoreTransactionResponse;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.settings.AppPreferences;
import com.wps.terminalapp.utils.ErrorCode;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.utils.WPRSAUtil;
import com.wps.terminalapp.webrequest.WebRequest;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.poynt.api.model.Transaction;
import co.poynt.api.model.TransactionStatus;
import co.poynt.os.model.Intents;
import co.poynt.os.model.Payment;
import co.poynt.os.model.PaymentStatus;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleObserver;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by vignaraj.r on 08-Aug-18.
 */

public class TerminalFragment extends Fragment {

    @BindView(R.id.txt_invoice_id)
    TextSansRegular txtInvoiceId;
    @BindView(R.id.txt_notes)
    TextSansRegular txtNotes;
    @BindView(R.id.btn_details)
    ButtonSansRegular btnDetails;
    @BindView(R.id.btn_erasevalue)
    ButtonSansRegular btnErasevalue;
    @BindView(R.id.edit_amount)
    AutoFormatEditText editAmount;
    @BindView(R.id.btn_one)
    ButtonSansRegular btnOne;
    @BindView(R.id.btn_two)
    ButtonSansRegular btnTwo;
    @BindView(R.id.btn_three)
    ButtonSansRegular btnThree;
    @BindView(R.id.btn_four)
    ButtonSansRegular btnFour;
    @BindView(R.id.btn_five)
    ButtonSansRegular btnFive;
    @BindView(R.id.btn_six)
    ButtonSansRegular btnSix;
    @BindView(R.id.btn_seven)
    ButtonSansRegular btnSeven;
    @BindView(R.id.btn_eight)
    ButtonSansRegular btnEight;
    @BindView(R.id.btn_nine)
    ButtonSansRegular btnNine;
    @BindView(R.id.btn_clear)
    ButtonSansRegular btnClear;
    @BindView(R.id.btn_zero)
    ButtonSansRegular btnZero;
    @BindView(R.id.btn_dzero)
    ButtonSansRegular btnDzero;
    @BindView(R.id.btn_cash)
    RelativeLayout btnCash;
    @BindView(R.id.btn_card)
    RelativeLayout btnCard;
    @BindView(R.id.btn_alipay)
    RelativeLayout btnAlipay;
    @BindView(R.id.btn_done)
    ButtonSansRegular btnDone;
    @BindView(R.id.btn_back)
    ButtonSansRegular btnBack;
    @BindView(R.id.edit_invoiceno)
    TextInputEditText edit_invoiceno;
    @BindView(R.id.edit_invoicenote)
    TextInputEditText edit_invoicenote;
    @BindView(R.id.btn_invoicedate)
    ButtonSansRegular btnInvoiceDate;
    @BindView(R.id.details_layout)
    LinearLayout layout_details;
    @BindView(R.id.numbers_layout)
    LinearLayout layout_numbers;
    @BindView(R.id.btn_layout)
    LinearLayout layout_btn;


    //
    InputMethodManager mgr;
    public String strNum = "0.00";
    double num = 0;
    private static final int COLLECT_PAYMENT_REQUEST = 13132;
    private static final int SCANNER_REQUEST_CODE = 1002;
    public String orderInvoiceNum = "";
    public String orderInvoiceNote = "";
    public String orderInvoiceDate = "";
    int year, month, dayOfMonth;
    boolean isCallBack = false;
    String callbackUrl = "";
    String orderId = "";
    public static String TAG = AppPreferences.class.getSimpleName();

    TerminalListener terminalListener;

    private APIComponent componentApliPay;
    private APIComponent componentTerminal;
    private FetchResponse fetch;
    private Disposable disposable;
    private AlertDialog alert;
    private volatile boolean loopCancel = false;
    private OrderRequestResponseData store;
    private String transactionId;

    public interface TerminalListener {
        void showSuccessTransaction(TransactionModel model);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Retrofit retrofitApliPay = new APIBuilder(requireContext()).retrofitApliPay();
        componentApliPay = retrofitApliPay.create(APIComponent.class);

        Retrofit retrofitTerminal = new APIBuilder(requireContext()).retrofitTerminal();
        componentTerminal = retrofitTerminal.create(APIComponent.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terminal, container, false);
        ButterKnife.bind(this, view);
        editAmount.setFilters(new InputFilter[]{new DecimalInputFilter(5, 2)});
        editAmount.setCursorVisible(false);
        editAmount.setFocusable(false);
        editAmount.setFocusableInTouchMode(false);
        mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(editAmount.getWindowToken(), 0);
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        btnErasevalue.setVisibility(View.GONE);
        callbackUrl = "";
        orderId = "";
        isCallBack = false;
        showEmptyScreen();

        String setCash = AppPreferences.getInstance().getCashTrans(getActivity());
        String setCard = AppPreferences.getInstance().getCardTrans(getActivity());

        if (setCash.toLowerCase().equals("n") && setCard.toLowerCase().equals("n")) {
            syncUserData();
        } else {
            setButton(setCash, setCard);
        }


        //
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                .repeatCount(0)
                .duration(2000)
                .createFor(view)
                .start();
        //
        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.equals("0.00") && num > 0) {
                    btnErasevalue.setVisibility(View.VISIBLE);
                } else {
                    btnErasevalue.setVisibility(View.GONE);
                }
            }
        });

        if (getArguments() != null) {
            if (getArguments().getBoolean("notification", false)) {
                long amount = getArguments().getLong("amount");
                String type = getArguments().getString("type");
                String notes = getArguments().getString("notes");
                String orderid = getArguments().getString("order_id");
                String callback_url = getArguments().getString("callback_url");
                firePaymentFromNotificaction(amount, type, notes, orderid, callback_url);
            }
        }
        return view;
    }

    void setButton(String setCash, String setCard) {

        if (setCash.toLowerCase().equals("y")) {
            btnCash.setVisibility(View.VISIBLE);
        } else if (setCash.toLowerCase().equals("n")) {
            btnCash.setVisibility(View.GONE);
        }
        if (setCard.toLowerCase().equals("y")) {
            btnCard.setVisibility(View.VISIBLE);
        } else if (setCard.toLowerCase().equals("n")) {
            btnCard.setVisibility(View.GONE);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            terminalListener = (MainActivity) context;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(context, e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COLLECT_PAYMENT_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("result", "ok");
                if (data != null) {
                    Payment payment = data.getParcelableExtra(Intents.INTENT_EXTRAS_PAYMENT);
                    if (payment != null) {
                        Log.d(TAG, "Received onPaymentAction from PaymentFragment w/ Status("
                                + payment.getStatus() + ")");
//                        Util.logLargeString(TAG, new Gson().toJson(payment));
                        if (payment.getStatus().equals(PaymentStatus.COMPLETED)) {
                            clearValues();
                            logData("Payment Completed");
                            storeSuccessfulTransaction(payment);
                        } else if (payment.getStatus().equals(PaymentStatus.AUTHORIZED)) {
                            clearValues();
                            logData("Payment Authorized");
                            storeSuccessfulTransaction(payment);
                        } else if (payment.getStatus().equals(PaymentStatus.CANCELED)) {
                            logData("Payment Canceled");
                        } else if (payment.getStatus().equals(PaymentStatus.FAILED)) {
                            logData("Payment Failed");
                        } else if (payment.getStatus().equals(PaymentStatus.REFUNDED)) {
                            logData("Payment Refunded");
                        } else if (payment.getStatus().equals(PaymentStatus.VOIDED)) {
                            logData("Payment Voided");
                        } else if (payment.getStatus().equals(PaymentStatus.DECLINED)) {
                            logData("Payment Declined");
                            storeDeclinedTransaction(payment);
                        } else {
                            logData("Other Reasons");
                        }
                        fireCallBackService(payment.getStatus().toString());
                        showEmptyScreen();
                        resetButtons();
                    } else {
                        showEmptyScreen();
                        resetButtons();
                    }
                } else {
                    showEmptyScreen();
                    resetButtons();
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                logData("Payment Canceled");
                fireCallBackService("CANCELLED");
                showEmptyScreen();
                resetButtons();
            }
        }else if (requestCode == SCANNER_REQUEST_CODE && data != null) {
            if (resultCode == Activity.RESULT_OK) {
                String code = data.getStringExtra("CODE");
                Log.d("barCode", code);
                orderRequest(code);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Util.showInfoToast(requireContext(), "Scanner canceled!");
                resetButtons();
            }
        }else if(requestCode == SCANNER_REQUEST_CODE){
            resetButtons();
        }
    }

    private void orderRequest(String authCode){
        Util.showProgressDialog(requireActivity(), "Processing Transaction");
        Single.create((SingleOnSubscribe<String>) emitter -> {
            String rxResult = "";
            StoreTransactionRequest storeResponse = storeTransactions(authCode, "R");
            if(storeResponse != null){
                OrderRequestResponse requestResult = null;
                try {
                    requestResult = quickPay(authCode);
                    store = requestResult.getData();
                    if(requestResult.getData().getErrMsg() != null && requestResult.getData().getErrMsg().length() > 0){
                        Util.showErrorToast(getActivity(), requestResult.getData().getErrMsg());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(requestResult == null){
                    Util.showInfoToast(requireContext(), "Quick pay failed!");
                }else {
                    storeResponse.setOALRESULT(requestResult.getData().getResult());
                    storeResponse.setOALCASHFEE(String.valueOf(requestResult.getData().getCashFee()));
                    storeResponse.setOALCASHFEETYPE(requestResult.getData().getCashFeeType());
                    storeResponse.setOALKSHERORDERNO(requestResult.getData().getKsherOrderNo());
                    storeResponse.setOALCHANNELORDERNO(requestResult.getData().getChannelOrderNo());
                    storeResponse.setOALRATE(requestResult.getData().getRate());
                    storeResponse.setOALOPENID(requestResult.getData().getOpenid());
                    storeResponse.setOALTIMEEND(requestResult.getData().getTimeEnd());
                    storeResponse.setOALOPERATORID(requestResult.getData().getOperatorId());

                    storeResponse.setOALERRCODE(requestResult.getData().getErrCode());
                    storeResponse.setOALERRMSG(requestResult.getData().getErrMsg());

                    try {
                        if(requestResult.getData().getResult().equalsIgnoreCase("SUCCESS")){
                            storeResponse.setOALSTSYN("Y");
                            transactionId = requestResult.getData().getMchOrderNo();
                        } else if(requestResult.getData().getResult().equalsIgnoreCase("NOTSURE"))
                            storeResponse.setOALSTSYN("Q");
                        else
                            storeResponse.setOALSTSYN("X");
                    }catch (Exception e){
                        Util.showErrorToast(requireContext(), requestResult.getMsg());
                        storeResponse.setOALERRMSG(requestResult.getMsg());
                        storeResponse.setOALSTSYN("X");
                    }

                    boolean updateStatus = updateTransactions(storeResponse);
                    try {
                        if(updateStatus && requestResult.getData().getResult() != null){
                            com.alibaba.fastjson.JSONObject json = com.alibaba.fastjson.JSONObject.parseObject(new Gson().toJson(requestResult));
                            boolean b = WPRSAUtil.ksherVerify(json.getJSONObject("data"), requestResult.getSign(), fetch.getODEVKSHERPUBKEY());
                            if(b){
                                Util.showInfoToast(requireContext(), requestResult.getData().getResult());
                                rxResult = requestResult.getData().getResult();
                            } else{
                                Util.showErrorToast(requireContext(), "sign verification failed");
                                rxResult = "DO_REVOKE";
                            }
                        }else {
                            if(!updateStatus)
                                Util.showErrorToast(requireContext(), "ServerError");
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        Util.showErrorToast(requireContext(), "sign verification failed");
                        rxResult = "DO_REVOKE";
                    }
                }
            }else{
                Util.showErrorToast(requireContext(), "Store Transaction failed");
            }

            emitter.onSuccess(rxResult);
        }) .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(String s) {
                        Util.closeProgressDialog();
                        if(s.equalsIgnoreCase("NOTSURE")){
                            orderLoop(authCode, store);
                        }else if(s.equalsIgnoreCase("DO_REVOKE")){
                            revokeOrder(authCode, store);
                        }else if(s.equalsIgnoreCase("SUCCESS")){
                            editAmount.setText("0.00");
                            resetButtons();
                            getTransactionById(transactionId);
                        }else {
                            editAmount.setText("0.00");
                            resetButtons();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Util.closeProgressDialog();
                        resetButtons();
                    }
                });
    }

    private StoreTransactionRequest storeTransactions(String authCode, String oalType){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK);
        StoreTransactionRequest storeRequest = new StoreTransactionRequest();
        storeRequest.setOALAUTHCODE(authCode);
        storeRequest.setOALPDEVICEID(GeneralUtil.getDeviceSerial());
        storeRequest.setOALAPPID(fetch.getODEVKSHERAPPID());
        storeRequest.setOALATTACH("");
        storeRequest.setOALCHANNEL(fetch.getChannel());
        storeRequest.setOALCHANNELORDERNO(null);
        storeRequest.setOALFEETYPE(fetch.getFeeType());
        storeRequest.setOALKSHERORDERNO(null);
        storeRequest.setOALMCHORDERNO(fetch.getMchOrderNo());
        storeRequest.setOALREFUNDFEE(0);
        storeRequest.setOALMCHREFUNDNO(null);
        storeRequest.setOALOPERATORID("0");
        storeRequest.setOALTOTALFEE(fetch.getTotalFee());
        storeRequest.setOALCRDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALUPDDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALSTDT(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALTIMESTART(sdf.format(Calendar.getInstance().getTime()));
        storeRequest.setOALCRUID("1");
        storeRequest.setOALUPDUID("1");
        storeRequest.setOALBATCHNO(fetch.getODEVTXNBATCHNO());
        storeRequest.setOALTYPE(oalType);
        storeRequest.setOALSTSYN("N"); // set y if success, x if failed


        Call<StoreTransactionResponse> call = componentTerminal.storeTransaction(storeRequest);
        try {
            Response<StoreTransactionResponse> response = call.execute();
            StoreTransactionResponse res = response.body();
            if(res != null && res.getIsSuccess()){
               return res.getResponseData().get(0);
            }else
                Util.showErrorToast(getActivity(), "Store Transaction failed");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private boolean updateTransactions(StoreTransactionRequest storeRequest){
        Call<StoreTransactionResponse> call = componentTerminal.updateTransaction(storeRequest);
        try {
            Response<StoreTransactionResponse> response = call.execute();
            StoreTransactionResponse res = response.body();
            if(res != null && res.getIsSuccess()){
               // Util.showInfoToast(getActivity(), "Transaction Updated");
                return true;
            }else
                Util.showErrorToast(getActivity(), "Update Transaction failed");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private OrderRequestResponse quickPay(String authCode) throws IOException {
        long amount = fetch.getTotalFee();
        String fee_type = fetch.getFeeType();
        String channel = fetch.getChannel().toLowerCase();
        String appid = fetch.getODEVKSHERAPPID();
        String nonceStr = Util.randomAlphanumeric(4);
        SimpleDateFormat timeStampFormat = new java.text.SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
        String timeStamp = timeStampFormat.format(new java.util.Date());
        String mchOrderNo = fetch.getMchOrderNo();

        Map<String, String> paras = new HashMap<>();
        paras.put("mch_order_no", mchOrderNo);
        paras.put("total_fee", String.valueOf(amount));
        paras.put("fee_type", fee_type);
        paras.put("auth_code", authCode);
        paras.put("channel", channel);
        paras.put("operator_id", "");
        paras.put("appid", appid);
        paras.put("nonce_str", nonceStr);
        paras.put("time_stamp", timeStamp);

        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());

        if(appid.isEmpty() || nonceStr.isEmpty() || amount == 0 ||
                fee_type.isEmpty() || mchOrderNo.isEmpty() || authCode.isEmpty() || sign == null || sign.isEmpty()){
            return null;
        }

        Call<OrderRequestResponse> call = componentApliPay.orderRequest(mchOrderNo, String.valueOf(amount), fee_type,
                authCode, channel,  "", appid, nonceStr, timeStamp, sign);
        Response<OrderRequestResponse> response = call.execute();

        OrderRequestResponse res = response.body();
        if(res != null && res.getData() != null){
            return res;
        }

        return null;
    }


    private String queryLoopResult = "";
    private void orderLoop(String authCode, OrderRequestResponseData store){
        showAlert(authCode, store);
        int count = Integer.valueOf(fetch.getQueryCount());
        Integer[] loopCount = new Integer[count];
        for (int i = 1; i <= count; i++)
            loopCount[i-1] = i;
        loopCancel = false;
        Observable.fromArray(loopCount)
                .delay(5000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(Integer integer) {
                        Log.d(TAG, "onNext: " + integer);
                        Log.d(TAG, "thread id: " + Thread.currentThread().getId());
                        requireActivity().runOnUiThread(() -> {
                            alert.setMessage("Waiting for the Host Response [" + integer + "of " + count + " ] ,Press Cancel to Quit The Transaction");
                        });
                        StoreTransactionRequest storeResponse = storeTransactions(authCode, "Q");
                        if(storeResponse != null){
                            try {
                                OrderRequestResponse requestResult = orderQuery(store);
                                if(requestResult == null){
                                    Util.showInfoToast(requireContext(), "Query failed!");
                                }else {
                                    storeResponse.setOALRESULT(requestResult.getData().getResult());
                                    storeResponse.setOALCASHFEE(String.valueOf(requestResult.getData().getCashFee()));
                                    storeResponse.setOALCASHFEETYPE(requestResult.getData().getCashFeeType());
                                    storeResponse.setOALKSHERORDERNO(requestResult.getData().getKsherOrderNo());
                                    storeResponse.setOALCHANNELORDERNO(requestResult.getData().getChannelOrderNo());
                                    storeResponse.setOALRATE(requestResult.getData().getRate());
                                    storeResponse.setOALOPENID(requestResult.getData().getOpenid());
                                    storeResponse.setOALTIMEEND(requestResult.getData().getTimeEnd());
                                    storeResponse.setOALOPERATORID(requestResult.getData().getOperatorId());

                                    storeResponse.setOALERRCODE(requestResult.getData().getErrCode());
                                    storeResponse.setOALERRMSG(requestResult.getData().getErrMsg());

                                    if(integer == count && requestResult.getData().getResult().equalsIgnoreCase("NOTSURE")){
                                        storeResponse.setOALSTSYN("Q");
                                        storeResponse.setOALERRCODE("REV");
                                    } else if (requestResult.getData().getResult().equalsIgnoreCase("SUCCESS")){
                                        queryLoopResult = "SUCCESS";
                                        storeResponse.setOALSTSYN("Y");
                                        transactionId = requestResult.getData().getMchOrderNo();
                                    } else if (requestResult.getData().getResult().equalsIgnoreCase("FAIL"))
                                        storeResponse.setOALSTSYN("X");
                                    else if (requestResult.getData().getResult().equalsIgnoreCase("NOTSURE"))
                                        storeResponse.setOALSTSYN("Q");


                                        updateTransactions(storeResponse);
                                }
                                if(!(requestResult.getData().getResult().equalsIgnoreCase("NOTSURE")) || loopCancel){
                                    if(loopCancel)
                                        queryLoopResult = "DO_REVOKE";

                                    if(disposable != null && !disposable.isDisposed()){
                                        requireActivity().runOnUiThread(() ->  {
                                            if(alert != null && alert.isShowing())
                                                alert.dismiss();
                                            if(loopCancel){
                                                revokeOrder(authCode, store);
                                            }else if (requestResult.getData().getResult().equalsIgnoreCase("SUCCESS")){
                                                getTransactionById(transactionId);
                                            }
                                            editAmount.setText("0.00");
                                            resetButtons();
                                        });


                                        disposable.dispose();
                                    }
                                }

                                if(count == integer)
                                    queryLoopResult = "DO_REVOKE";

                            } catch (Exception e) {
                                e.printStackTrace();
                                queryLoopResult = "DO_REVOKE";
                            }
                        }else {
                            queryLoopResult = "DO_REVOKE";
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        requireActivity().runOnUiThread(() ->  {
                            if(alert != null && alert.isShowing())
                                alert.dismiss();
                            if(queryLoopResult.equals("DO_REVOKE"))
                                revokeOrder(authCode, store);
                        });
                    }

                    @Override
                    public void onComplete() {
                        requireActivity().runOnUiThread(() ->  {
                            if(alert != null && alert.isShowing())
                                alert.dismiss();
                            System.out.println("<<<<>>>>  "+ queryLoopResult);
                            if(queryLoopResult.equals("DO_REVOKE"))
                                revokeOrder(authCode, store);
                            else if(queryLoopResult.equalsIgnoreCase("SUCCESS")){
                                editAmount.setText("0.00");
                                resetButtons();
                                getTransactionById(transactionId);
                            } else {
                                editAmount.setText("0.00");
                                resetButtons();
                            }
                        });
                    }
                });
    }

    private OrderRequestResponse orderQuery(OrderRequestResponseData store) throws IOException {

        String channel = fetch.getChannel().toLowerCase();
        String appid = fetch.getODEVKSHERAPPID();
        String nonceStr = Util.randomAlphanumeric(4);

        Map<String, String> paras = new HashMap<>();
        paras.put("appid", appid);
        paras.put("channel", channel);
        paras.put("mch_order_no", store.getMchOrderNo());
        if(store.getKsherOrderNo() != null)
            paras.put("ksher_order_no", store.getKsherOrderNo());
        if(store.getChannelOrderNo() != null)
            paras.put("channel_order_no", store.getChannelOrderNo());
        paras.put("nonce_str", nonceStr);
        paras.put("version", "3.0.0");

        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());

        if(appid.isEmpty() || nonceStr.isEmpty() ||  sign == null || sign.isEmpty()){
            return null;
        }

        Call<OrderRequestResponse> call = componentApliPay.orderQuery(appid, channel, store.getMchOrderNo(),
                store.getKsherOrderNo(), store.getChannelOrderNo(), nonceStr, "3.0.0", sign);

        Response<OrderRequestResponse> response = call.execute();

        OrderRequestResponse res = response.body();
        if(res != null && res.getData() != null){
            return res;
        }

        return null;
    }

    private void showAlert(String authCode, OrderRequestResponseData store){
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setMessage("please wait...")
                .setCancelable(false)
                .setNegativeButton("Cancel", (dialog, id) -> {
                    loopCancel = true;
                });
        builder.setCancelable(false);
        alert = builder.create();
        alert.show();
    }


    private OrderRefundResponse orderRefund(OrderRequestResponseData store) throws IOException {
        Util.showProgressDialog(requireContext(), "Processing Transaction");

        long amount = fetch.getTotalFee();
        String channel = fetch.getChannel().toLowerCase();
        String appid = fetch.getODEVKSHERAPPID();
        String nonceStr = Util.randomAlphanumeric(4);
        String fee_type = fetch.getFeeType();
        String mchRefundNo = Util.getNextRefundNo();

        java.util.Map<String, String> paras = new java.util.HashMap<>();
        paras.put("appid", appid);
        paras.put("channel", channel);
        paras.put("mch_order_no", store.getMchOrderNo());
        paras.put("ksher_order_no", store.getKsherOrderNo());
        paras.put("channel_order_no", store.getChannelOrderNo());
        paras.put("fee_type", fee_type);
        paras.put("total_fee", "" + amount);
        paras.put("refund_fee", "" + amount);
        paras.put("mch_refund_no", mchRefundNo);
        paras.put("device_id", GeneralUtil.getDeviceSerial());
        paras.put("operator_id", "");
        paras.put("attach", "");
        paras.put("nonce_str", nonceStr);
        paras.put("version", "3.0.0");


        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());


        Call<OrderRefundResponse> call = componentApliPay.orderRefund(appid, channel, store.getMchOrderNo(), store.getKsherOrderNo(),
                store.getChannelOrderNo(), fee_type, ""+ fee_type, "" + amount, mchRefundNo, GeneralUtil.getDeviceSerial(), "",
                "", nonceStr, "3.0.0", sign);

        Response<OrderRefundResponse> response = call.execute();

        OrderRefundResponse res = response.body();
        if(res != null && res.getData() != null){
            return res;
        }

        return null;
    }

    private void revokeOrder(String authCode, OrderRequestResponseData store){
        Util.showProgressDialog(requireActivity(), "Revoking Order");
        Single.create((SingleOnSubscribe<String>) emitter -> {
            StoreTransactionRequest storeResponse = storeTransactions(authCode, "V");
            if(storeResponse != null){
                OrderRevokeResponse requestResult = null;
                try {
                    requestResult = orderRevoke(store);
                }catch (Exception ignore){ }

                if(requestResult == null){
                    Util.showInfoToast(requireContext(), "Revoke failed!");
                }else {
                    storeResponse.setOALRESULT(requestResult.getData().getResult());
                    storeResponse.setOALCASHFEE(String.valueOf(requestResult.getData().getCashFee()));
                    storeResponse.setOALCASHFEETYPE(requestResult.getData().getCashFeeType());
                    storeResponse.setOALKSHERORDERNO(requestResult.getData().getKsherOrderNo());
                    storeResponse.setOALCHANNELORDERNO(requestResult.getData().getChannelOrderNo());
                    storeResponse.setOALRATE(requestResult.getData().getRate());
                    storeResponse.setOALOPENID(requestResult.getData().getOpenid());
                    storeResponse.setOALTIMEEND(requestResult.getData().getTimeEnd());
                    storeResponse.setOALOPERATORID(requestResult.getData().getOperatorId());

                    storeResponse.setOALERRCODE(requestResult.getData().getErrCode());
                    storeResponse.setOALERRMSG(requestResult.getData().getErrMsg());

                    if(requestResult.getData().getResult().equalsIgnoreCase("SUCCESS"))
                        storeResponse.setOALSTSYN("Y");
                    else
                        storeResponse.setOALSTSYN("X");

                    boolean updateStatus = updateTransactions(storeResponse);
                    if(updateStatus){
                        Util.showInfoToast(requireContext(), "Order Revoked");
                    }else {
                        Util.showErrorToast(requireContext(), "ServerError");
                    }
                }
            }else
                Util.showErrorToast(requireContext(), "Revoke failed");

            emitter.onSuccess("Result");
        }) .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(String s) {
                        Util.closeProgressDialog();
                        editAmount.setText("0.00");
                        resetButtons();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Util.closeProgressDialog();
                        editAmount.setText("0.00");
                        resetButtons();
                    }
                });
    }

    private OrderRevokeResponse orderRevoke(OrderRequestResponseData store) throws IOException {
        String channel = fetch.getChannel().toLowerCase();
        String appid = fetch.getODEVKSHERAPPID();
        String nonceStr = Util.randomAlphanumeric(4);

        java.util.Map<String, String> paras = new java.util.HashMap<>();
        paras.put("appid", appid);
        paras.put("channel", channel);
        paras.put("mch_order_no", store.getMchOrderNo());
        paras.put("ksher_order_no", store.getKsherOrderNo());
        if(store.getChannelOrderNo() != null)
            paras.put("channel_order_no", store.getChannelOrderNo());
        paras.put("nonce_str", nonceStr);
        paras.put("version", "3.0.0");

        String sign = WPRSAUtil.ksherSign(paras, fetch.getODEVKSHERPVTKEY());

        if(appid.isEmpty() || nonceStr.isEmpty() || sign == null || sign.isEmpty()){
            return null;
        }

        Call<OrderRevokeResponse> call = componentApliPay.orderRevoke(appid, channel, store.getMchOrderNo(),
                store.getKsherOrderNo(), store.getChannelOrderNo(), nonceStr, "3.0.0", sign);

        Response<OrderRevokeResponse> response = call.execute();

        OrderRevokeResponse res = response.body();
        return res;

    }

    public void syncUserData() {
        Util.dTag(1, TAG, "syncUserData");
        WebServices.getInstance().getUsersData(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String response) {
                if (response != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject responseData = new JSONObject(response);
                                if (responseData.getBoolean("IsSuccess")) {
                                    JSONArray jsonArray = responseData.getJSONArray("ResponseData");
                                    List<UserData> list = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        String userCode = obj.getString("usercode");
                                        String username = obj.getString("username");
                                        String userrole = obj.getString("userrole");
                                        String userPin = obj.getString("userpin");
                                        String userauthpin = obj.optString("userauthpin");
                                        list.add(new UserData(userCode, username, userrole, userPin, userauthpin));
                                    }

                                    JSONArray otherData = responseData.getJSONArray("OtherData1");
                                    String settlePINKey = "SETTLE_PIN_YN";
                                    String settleTypeKey = "SETTLE_TYPE";
                                    String settleCashKey = "SETTLE_CASH_YN";
                                    String settleCardKey = "SETTLE_CARD_YN";
                                    String settleName = "Ask PIN for Settle?";
                                    String settleVal = "N";
                                    String settleValY = "Y";
                                    String settleType = "MANUAL";
                                    if (otherData != null) {
                                        for (int i = 0; i < otherData.length(); i++) {
                                            JSONObject obj = otherData.getJSONObject(i);
                                            String sType = obj.optString("stype");
                                            if (sType.equals(settlePINKey)) {
                                                settleName = obj.optString("sname");
                                                settleVal = obj.optString("sval");
                                            } else if (sType.equals(settleTypeKey)) {
                                                settleType = obj.optString("sval");
                                            } else if (sType.equals(settleCashKey)) {
                                                AppPreferences.getInstance().storeCash(getActivity(), settleCashKey, obj.optString("sval"));
                                            } else if (sType.equals(settleCardKey)) {
                                                AppPreferences.getInstance().storeCard(getActivity(), settleCardKey, obj.optString("sval"));
                                            }
                                        }
                                        String setCash = AppPreferences.getInstance().getCashTrans(getActivity());
                                        String setCard = AppPreferences.getInstance().getCardTrans(getActivity());
                                        setButton(setCash, setCard);
                                    } else {
                                        setButton("N", "N");
                                    }
                                    AppPreferences.getInstance().storeUserdata(getActivity(), list, true, Util.getCurrentTimeStamp(), settleName, settleVal, settleType);
                                    Util.showSuccessToast(getActivity(), "Data Successfully Synced.");
                                    String lastUpdate = Util.getDateString(AppPreferences.getInstance().getLastupdateTime(getActivity()), "dd-MMM-yyyy hh:mm a");

                                    if (settleVal.toLowerCase().equals("y")) {
                                        settleVal = "YES";
                                    } else {
                                        settleVal = "NO";
                                    }
                                } else {
                                    Util.showErrorToast(getActivity(), responseData.getString("ErrMsg"));
                                }
                            } catch (JSONException e) {
                                Util.closeProgressDialog();
                                Util.showErrorDialog(getActivity(), "Settings not found", new ErrorListener() {
                                    @Override
                                    public void doAction() {
                                        //do nothing
                                    }
                                });
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.closeProgressDialog();
                        Util.showErrorToast(getActivity(), message);
                    }
                });
            }
        });
    }

    @OnClick({R.id.btn_one, R.id.btn_two, R.id.btn_three, R.id.btn_four,
            R.id.btn_five, R.id.btn_six, R.id.btn_seven, R.id.btn_eight,
            R.id.btn_nine, R.id.btn_zero, R.id.btn_dzero, R.id.btn_clear,
            R.id.btn_erasevalue})
    public void enterAmount(View v) {
        switch (v.getId()) {
            case R.id.btn_one:
                insert("1");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;
            case R.id.btn_two:
                insert("2");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_three:
                insert("3");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_four:
                insert("4");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_five:
                insert("5");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_six:
                insert("6");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_seven:
                insert("7");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_eight:
                insert("8");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;

            case R.id.btn_nine:
                insert("9");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;
            case R.id.btn_clear:
                if (editAmount.getText().toString().equals("0.00")) {

                } else {
                    strNum = "0.00";
                    btnErasevalue.setVisibility(View.GONE);
                    num = 0;
                    mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    editAmount.setText(strNum);
                }
                break;
            case R.id.btn_zero:
                insert("0");
                mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                break;
            case R.id.btn_dzero:
                if (!strNum.equals("0.00")) {
                    insert("00");
                    mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                break;
            case R.id.btn_erasevalue:
                strNum = strNum.replace(",", "");
                if (editAmount.getText().toString().equals("0.00")) {
                    btnErasevalue.setVisibility(View.GONE);
                } else {
                    int textLength = editAmount.getText().toString().length();
                    editAmount.setSelection(textLength, textLength);
                    if (strNum.length() >= 1) {
                        int decimalIndex = strNum.indexOf(".");
                        if (decimalIndex >= 1) {
                            strNum = strNum.substring(0, decimalIndex - 1) + "." +
                                    strNum.substring(decimalIndex - 1, decimalIndex) +
                                    strNum.substring(decimalIndex + 1, strNum.length() - 1);
                            editAmount.setText(strNum);
                            if (strNum.equals(".00") || strNum.equals("0.00")) {
                                btnErasevalue.setVisibility(View.GONE);
                            }
                        } else {
                            strNum = "0.0" + strNum.substring(decimalIndex + 1, strNum.length() - 1);
                            editAmount.setText(strNum);
                            if (strNum.equals("0.00")) {
                                btnErasevalue.setVisibility(View.GONE);
                            }
                        }
                    } else if (strNum.length() <= 1) {
                        btnErasevalue.setVisibility(View.GONE);
                        editAmount.setText("0.00");
                    }
                }
                break;
        }
    }


    @OnClick({R.id.btn_cash, R.id.btn_card, R.id.btn_alipay})
    public void makePayment(View v) {
        switch (v.getId()) {
            case R.id.btn_cash:
                try {
                    disableButtons();
                    if (validateAmount()) {
                        launchPayment(0);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Util.showErrorToast(getActivity(), "Something Went Wrong");
                    Crashlytics.logException(e);
                }
                break;
            case R.id.btn_card:
                try{
                    checkSettleType();
                    disableButtons();
                    if (validateAmount()) {
                        launchPayment(1);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Util.showErrorToast(getActivity(), "Something Went Wrong");
                    Crashlytics.logException(e);
                }
                break;

            case R.id.btn_alipay:
                try{
                    checkSettleType();
                    disableButtons();
                    if (validateAmount()) {
                        long amount = getAmount();
                        fetchDeviceDetails(amount);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Util.showErrorToast(getActivity(), "Something Went Wrong");
                    Crashlytics.logException(e);
                }
                break;
        }
    }


    public void fetchDeviceDetails(long amount){
        Util.showProgressDialog(requireContext(), "fetching device details");

        Map<String, String> params = new LinkedHashMap<>();
        params.put("DeviceID", GeneralUtil.getDeviceSerial());
        params.put("Amount", String.valueOf(amount));

        Call<FetchDeviceDetailsResponse> call = componentTerminal.fetchDeviceDetails(params);
        call.enqueue(new Callback<FetchDeviceDetailsResponse>() {
            @Override @ParametersAreNonnullByDefault
            public void onResponse(Call<FetchDeviceDetailsResponse> call, Response<FetchDeviceDetailsResponse> response) {
                Util.closeProgressDialog();
                FetchDeviceDetailsResponse res = response.body();
                if(res != null && res.getIsSuccess()){
                    if(res.getResponseData() != null &&  res.getResponseData().size() > 0){
                        fetch = res.getResponseData().get(0);
                        fetch.setMchOrderNo(Util.getNextOrderNo());
                        if(res.getOtherData1() != null && res.getOtherData1().size() > 0){
                            checkOrderState(res.getOtherData1().get(0));
                        }else {
                            Intent intent = new Intent("poynt.intent.action.SCANNER");
                            intent.putExtra("MODE", "SINGLE");
                            startActivityForResult(intent, SCANNER_REQUEST_CODE);
                        }
                    }else{
                        Util.showErrorToast(getActivity(), "fetching device details null");
                        resetButtons();
                    }
                }else{
                    if(res != null)
                        Util.showErrorToast(getActivity(), res.getErrMsg());
                    else
                        Util.showErrorToast(getActivity(), "fetching device details null");
                    resetButtons();
                }
            }

            @Override @ParametersAreNonnullByDefault
            public void onFailure(Call<FetchDeviceDetailsResponse> call, Throwable t) {
                t.printStackTrace();
                Util.closeProgressDialog();
                Util.showErrorToast(getActivity(), "fetching device details failed");
                resetButtons();
            }
        });
    }


    @OnClick(R.id.btn_invoicedate)
    public void pickDate() {
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                btnInvoiceDate.setText(date);
            }
        }, year, month, dayOfMonth);
        dpd.show();
    }

    @OnClick(R.id.btn_back)
    public void hideDetails() {
        hideDetailsLayout();
    }

    @OnClick(R.id.btn_done)
    public void doneDetails() {
        if (!btnInvoiceDate.getText().toString().equals("Click Here")) {
            orderInvoiceDate = btnInvoiceDate.getText().toString();
        }
        orderInvoiceNote = edit_invoicenote.getText().toString();
        orderInvoiceNum = "#" + edit_invoiceno.getText().toString();
        hideDetailsLayout();
    }

    @OnClick(R.id.btn_details)
    public void showDetails() {
        showDetailsLayout();
    }

    private void insert(String j) {
//        Log.e(TAG, " function: insert" + j);
        if (editAmount.getText().toString().length() <= 13) {
//            Log.e("edit_length", String.valueOf(editAmount.getText().toString().length()));
            String temp = editAmount.getText().toString();
            int decimalIndex = temp.indexOf('.');
//            Log.e("decimalIndex", String.valueOf(decimalIndex));
            if (decimalIndex >= 0) {
                String bfString = temp.substring(0, decimalIndex);
                if (bfString.length() > 5) {
                    Util.showErrorToast(getActivity(), "Amount Exceeded!");
                    return;
                }
                if (j.equals("00")) {
                    String afString = temp.substring(decimalIndex + 1, temp.length());
                    strNum = bfString + afString + ".00";
                } else {
                    if (temp.startsWith("0")) {
                        String afterDecimalStr = temp.substring(decimalIndex + 1, temp.length());
                        if (afterDecimalStr.startsWith("0")) {
                            int finalChar = Integer.parseInt(afterDecimalStr);
                            if (finalChar > 0) {
                                String firstAfDec = afterDecimalStr.substring(1, afterDecimalStr.length());
                                strNum = "0." + firstAfDec + j;
                            } else {
                                strNum = "0.0" + j;
                            }
                        } else {
                            String beforeDeciStr = afterDecimalStr.substring(0, 1);
                            strNum = beforeDeciStr + "." + afterDecimalStr.substring(1, 2) + j;
                        }
                    } else {
                        String beforeDecimalStr = temp.substring(0, decimalIndex);
                        String afterDecimalStr = temp.substring(decimalIndex + 1, temp.length());
                        if (afterDecimalStr.startsWith("0")) {
                            int secondStringVal = Integer.parseInt(afterDecimalStr.substring(1));
                            if (secondStringVal > 0) {
                                strNum = beforeDecimalStr + afterDecimalStr.substring(0, 1) + "." + afterDecimalStr.substring(1) + j;
                            } else {
//                                if (j.equals("0")) {
                                Log.e(TAG, secondStringVal + "");
                                strNum = beforeDecimalStr + afterDecimalStr.substring(0, 1) + "." + afterDecimalStr.substring(1, afterDecimalStr.length()) + j;
//                                } else {
//                                    strNum = beforeDecimalStr + "." + afterDecimalStr.substring(0, 1) + j;
//                                }
                            }
                        } else {
                            strNum = beforeDecimalStr + afterDecimalStr.substring(0, 1) + "." + afterDecimalStr.substring(1, afterDecimalStr.length()) + j;
                        }
                    }
                }
            }
//            Log.i("strNum", strNum);
            String separateNumStr = strNum;
            double separateNumDouble = Double.parseDouble(separateNumStr.replace(",", ""));
            num = separateNumDouble;
            editAmount.setText(strNum);
            InputMethodManager mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(editAmount.getWindowToken(), 0);
        }
    }

    public void launchPayment(int mode) {
        Payment payment = new Payment();
        payment.setCurrency("AED");
        if (mode == 1) {
            payment.setMultiTender(false);
            if (AppPreferences.getInstance().getManualStatus(getActivity()) == 1) {
                payment.setDisableManual(false);
            } else {
                payment.setDisableManual(true);
            }
        } else {
            payment.setMultiTender(false);
            payment.setCashOnly(true);
        }
        long amount = getAmount();
        if (orderInvoiceNum.equals("")) {
            orderInvoiceNum = UUID.randomUUID().toString();
        }
        Log.i("amount", String.valueOf(amount));
        String wholeNotes = orderInvoiceNum + "~" + orderInvoiceDate + "~" + orderInvoiceNote;
        payment.setAmount(amount);
        payment.setNotes(wholeNotes);
        payment.setSkipReceiptScreen(true);
        payment.setSkipPaymentConfirmationScreen(true);
        try {
            Intent collectPaymentIntent = new Intent(Intents.ACTION_COLLECT_PAYMENT);
            collectPaymentIntent.putExtra(Intents.INTENT_EXTRAS_PAYMENT, payment);
            startActivityForResult(collectPaymentIntent, COLLECT_PAYMENT_REQUEST);
        } catch (ActivityNotFoundException ex) {
            Log.e(TAG, ex.getMessage());
            resetButtons();
        }
    }

    public void logData(final String data) {
        Log.i("Transaction Data#", data);
    }

    public boolean validateAmount() {
        long amount = getAmount();
        if (amount > 0) {
            return true;
        } else {
            Util.showErrorToast(getActivity(), "Enter the valid amount");
            resetButtons();
            return false;
        }
    }

    public void hideDetailsLayout() {
        mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(edit_invoiceno.getWindowToken(), 0);
        if (orderInvoiceNum.equals("") || orderInvoiceNum.equals("#")) {
            txtInvoiceId.setVisibility(View.GONE);
        } else {
            if (orderInvoiceNum.length() > 8) {
                String invNum = "#" + orderInvoiceNum.substring(0, 8).replace("#", "") + "..";
                txtInvoiceId.setText(invNum);
            } else {
                txtInvoiceId.setText("#" + orderInvoiceNum.replace("#", ""));
            }
            txtInvoiceId.setVisibility(View.VISIBLE);
        }

        if (orderInvoiceNote.equals("")) {
            txtNotes.setVisibility(View.GONE);
        } else {
            if (orderInvoiceNote.length() > 8) {
                txtNotes.setText(orderInvoiceNote.substring(0, 8) + "...");
                txtNotes.setVisibility(View.VISIBLE);
            } else {
                txtNotes.setText(orderInvoiceNote + "...");
                txtNotes.setVisibility(View.VISIBLE);
            }
        }
        layout_details.setVisibility(View.GONE);
        btnBack.setVisibility(View.GONE);
        btnDone.setVisibility(View.GONE);

        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_UP)
                .repeatCount(0)
                .duration(1000)
                .createFor(layout_numbers)
                .start();

        btnDetails.setVisibility(View.VISIBLE);
        btnErasevalue.setVisibility(View.VISIBLE);
        layout_numbers.setVisibility(View.VISIBLE);
        layout_btn.setVisibility(View.VISIBLE);
    }

    public void showDetailsLayout() {
        btnDetails.setVisibility(View.GONE);
        btnErasevalue.setVisibility(View.GONE);
        layout_numbers.setVisibility(View.GONE);
        layout_btn.setVisibility(View.GONE);
        layout_details.setVisibility(View.VISIBLE);
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_UP)
                .repeatCount(0)
                .duration(1000)
                .createFor(layout_details)
                .start();
        btnBack.setVisibility(View.VISIBLE);
        btnDone.setVisibility(View.VISIBLE);

        edit_invoicenote.setText(orderInvoiceNote);

        String finalInvNum = orderInvoiceNum.replace("#", "");
        edit_invoiceno.setText(finalInvNum);


        if (!orderInvoiceDate.equals("")) {
            btnInvoiceDate.setText(orderInvoiceDate);
        }


    }

    public void clearValues() {
        Util.transactionList.clear();
        Util.settleAmount = "0.00";
        Util.totalTipAmount = "0.00";
        Util.totalCardAmount = "0.00";
        Util.totalCashAmount = "0.00";

        txtInvoiceId.setText("");
        txtInvoiceId.setVisibility(View.GONE);
        txtNotes.setText("");
        txtNotes.setVisibility(View.GONE);
        edit_invoiceno.setText("");
        edit_invoicenote.setText("");
        btnInvoiceDate.setText("Click Here");
        orderInvoiceNum = "";
        orderInvoiceNote = "";
        orderInvoiceDate = "";
        strNum = "0.00";
        num = 0;
        editAmount.setText("0.00");
    }

    public void storeSuccessfulTransaction(Payment payment) {
        String transId = "";
        if (payment.getTransactions() != null && payment.getTransactions().size() > 0) {
            AppPreferences.getInstance().clearLastTransaction(getActivity());
            for (int i = 0; i < payment.getTransactions().size(); i++) {
                if (payment.getTransactions().get(i).getStatus().equals(TransactionStatus.AUTHORIZED) ||
                        payment.getTransactions().get(i).getStatus().equals(TransactionStatus.CAPTURED)) {
                    Transaction transaction = payment.getTransactions().get(i);
                    transId = transaction.getId().toString();
                    storeLastTransaction(transaction);

                    String cardHolderName = "";
                    if (transaction.getFundingSource().getType().toString().equals("CARD")) {
                        cardHolderName = transaction.getFundingSource().getCard().getCardHolderFullName();
                    }

                    double netAmount = (double)transaction.getAmounts().getTransactionAmount() / 100;
                    TransactionModel tModel = new TransactionModel(
                            transId, System.currentTimeMillis(),
                            "S", transaction.getFundingSource().getType().toString(),
                            transaction.getFundingSource().getType().toString(), "",
                            cardHolderName, "",
                            netAmount, transaction.getStatus().toString(),
                            null);

                    tModel.newTransaction = true;
                    terminalListener.showSuccessTransaction(tModel);
                }
            }
            //delayedProcessed(transId);
        }
    }

    public void storeDeclinedTransaction(Payment payment) {
        if (payment.getTransactions() != null && payment.getTransactions().size() > 0) {
            AppPreferences.getInstance().clearLastTransaction(getActivity());
            storeLastTransaction(payment.getTransactions().get(0));
        }
    }

    public long getAmount() {
        NumberFormat nf = NumberFormat.getInstance();
        Number number = null;
        try {
            number = nf.parse(editAmount.getText().toString());
            //Long retval = Long.parseLong(editAmount.getText().toString());
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        long amount = (long) (Math.round(number.doubleValue() * 100));
        return amount;
    }

    public void getTransactionById(String id) {
        Log.d(TAG, "function - getTransactionById");
        WebServices.getInstance().getTxnById(getActivity(), id, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
//                Util.logLargeString("GetTxnByID", responseStr);
                Util.closeProgressDialog();
                try {
                    JSONObject obj = new JSONObject(responseStr);
                    if (obj.getBoolean("IsSuccess")) {
                        JSONObject responseData = obj.getJSONObject("ResponseData");
                        JSONArray tArray = responseData.getJSONArray("Transactions");
                        TransactionModel tModel = null;
                        if (tArray != null && tArray.length() > 0) {
                            for (int i = 0; i < tArray.length(); i++) {
                                JSONObject tObject = tArray.getJSONObject(i);
                                double amount = tObject.optDouble("mAmount");
                                double actVal = 0;
                                if (amount > 0) {
                                    actVal = amount / 100;
                                }
                                String cardNumber = tObject.optString("mCardNumber");
                                String cardType = tObject.optString("mCardType");
                                String cardImgType = tObject.optString("mCardTypeImg");
                                String transactionId = tObject.optString("mTransactionId");
                                String transactionStatus = tObject.optString("mTransactionStatus");
                                String type = tObject.optString("mType");
                                String cardHolderName = tObject.optString("mCardHolderName");
                                String mDisplayNo = tObject.optString("mDisplayNo");
                                long date = tObject.optLong("mDate");
                                String transactionType = tObject.optString("mPayType");
                                tModel = new TransactionModel(transactionId, date, type, transactionType, cardType,
                                        cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo);
                            }
                            if (tModel != null) {
//                                Util.logLargeString("successed_trans", new Gson().toJson(tModel));
                                terminalListener.showSuccessTransaction(tModel);
                            }
                        } else {
                            Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again to view the transaction");
                        }
                    } else {
                        Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again to view the transaction");
                    }
                } catch (JSONException e) {
                    Util.showErrorToast(getActivity(), "Unable fetch transaction, Please try again to view the transaction");
                }
            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    public void delayedProcessed(final String id) {
        Util.showProgressDialog(getContext(), "Fetching Transaction");
        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Util.closeProgressDialog();
                getTransactionById(id);
            }
        }, 5000);
    }

    public void showEmptyScreen() {
        Log.i("method", "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void resetButtons() {
        btnCash.setEnabled(true);
        btnCard.setEnabled(true);
        btnAlipay.setEnabled(true);
    }

    public void disableButtons() {
        btnCash.setEnabled(false);
        btnCard.setEnabled(false);
        btnAlipay.setEnabled(false);
    }

    public void firePaymentFromNotificaction(final long amount, final String... args) {
        Log.d(TAG, "function - firePaymentFromNotificaction");
//        Util.logLargeString("args", new Gson().toJson(args));
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (amount != 0) {
                    Long val = amount;
                    double finalAmt = val.doubleValue() / 100;
                    num = finalAmt;
                    NumberFormat numberFormat = NumberFormat.getNumberInstance();
                    numberFormat.setMinimumFractionDigits(2);
                    numberFormat.setMaximumFractionDigits(2);
                    strNum = numberFormat.format(finalAmt);
//                    num.format(finalAmt);
                    editAmount.setText(numberFormat.format(finalAmt));

                    Log.d("finalAmt", String.valueOf(finalAmt));
                    if (finalAmt > 0) {
                        btnErasevalue.setVisibility(View.VISIBLE);
                    }
                    String type = args[0];
                    String notes = args[1];
                    String Id = args[2];
                    String url = args[3];
                    orderId = Id;
                    callbackUrl = url;
                    if (type.equals("CARD")) {
                        isCallBack = true;
                        launchPayment(1);
                    } else {
                        isCallBack = true;
                        launchPayment(0);
                    }
                }
            }
        });

    }

    public void fireCallBackService(final String status) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!callbackUrl.equals("")) {
                    String finalUrl = callbackUrl + GeneralUtil.getDeviceSerial() + "&settleStatus=" + status;
                    WebRequest.getInstance().getRequest(finalUrl, new ResponseListener() {
                        @Override
                        public void onResponse(String responseStr) {
                            Util.logLargeString("callback#", responseStr);
                            isCallBack = true;
                        }

                        @Override
                        public void onFailure(String message) {
                            callbackUrl = "";
                            isCallBack = true;
                            Util.showSuccessToast(getActivity(), message);
                        }
                    });
                }
            }
        });
    }

    public void storeLastTransaction(Transaction transaction) {
        Log.d(TAG, "function - storeLastTransaction");
        if (AppPreferences.getInstance().getSettleType(getActivity()).equals("AUTO")) {
            AppPreferences.getInstance().storeLastTransaction(getActivity(), transaction);
        }
    }

    /**
     * If settle type is auto, check the last transaction date is less than current date, delete the batch value.
     */
    public void checkSettleType() {
        Log.d(TAG, "function - checkSettleType");
        if (AppPreferences.getInstance().getSettleType(getActivity()).equals("AUTO")) {
            Log.i("type", "auto");
            long currentTime = System.currentTimeMillis();
//            long currentTime = 1539028800000L;
            long cDate = Util.getFormattedTimeStamp(currentTime, "dd/MMM/yyyy");
            Log.i("currentDate", String.valueOf(cDate));
            long lastTransTime = AppPreferences.getInstance().getLastTransactionDate(getActivity());
            Log.i("transactionDate", String.valueOf(lastTransTime));
            if (cDate > lastTransTime) {
                Util.deleteBatchValue(getActivity());
            }
        }
    }


    OrderRequestResponseData generateResponseData(StoreTransactionRequest request){
        OrderRequestResponseData response = new OrderRequestResponseData();
        response.setAppid(request.getOALAPPID());
        response.setAttach(request.getOALATTACH());
        if(request.getOALCASHFEE() != null)
            response.setCashFee(Integer.parseInt(request.getOALCASHFEE()));
        response.setCashFeeType(request.getOALCASHFEETYPE());
        response.setChannel(request.getOALCHANNEL());
        response.setChannelOrderNo(request.getOALCHANNELORDERNO());
        response.setDeviceId(request.getOALPDEVICEID());
        response.setErrCode(request.getOALERRCODE());
        response.setErrMsg(request.getOALERRMSG());
        response.setFeeType(request.getOALFEETYPE());
        response.setKsherOrderNo(request.getOALKSHERORDERNO());
        response.setMchOrderNo(request.getOALMCHORDERNO());
        response.setOpenid(request.getOALOPENID());
        response.setOperation(request.getOALOPERATION());
        response.setOperatorId(request.getOALOPERATORID());
        response.setRate(request.getOALRATE());
        response.setResult(request.getOALRESULT());
        response.setTimeEnd(request.getOALTIMEEND());
        response.setTotalFee(request.getOALTOTALFEE());
        return response;
    }

    private void checkOrderState(StoreTransactionRequest storeResponse){
        Util.showProgressDialog(requireActivity(), "Processing Pending Transaction");
        OrderRequestResponseData storedRequest = generateResponseData(storeResponse);
        Single.create((SingleOnSubscribe<String>) emitter -> {
            String rxResult = "Failed to update pending transaction";
            try {
                OrderRequestResponse requestResult = orderQuery(storedRequest);
                if(requestResult == null){
                    Util.showInfoToast(requireContext(), "Query failed!");
                }else {
                    storeResponse.setOALRESULT(requestResult.getData().getResult());
                    storeResponse.setOALCASHFEE(String.valueOf(requestResult.getData().getCashFee()));
                    storeResponse.setOALCASHFEETYPE(requestResult.getData().getCashFeeType());
                    storeResponse.setOALKSHERORDERNO(requestResult.getData().getKsherOrderNo());
                    storeResponse.setOALCHANNELORDERNO(requestResult.getData().getChannelOrderNo());
                    storeResponse.setOALRATE(requestResult.getData().getRate());
                    storeResponse.setOALOPENID(requestResult.getData().getOpenid());
                    storeResponse.setOALTIMEEND(requestResult.getData().getTimeEnd());
                    storeResponse.setOALOPERATORID(requestResult.getData().getOperatorId());

                    storeResponse.setOALERRCODE(requestResult.getData().getErrCode());
                    storeResponse.setOALERRMSG(requestResult.getData().getErrMsg());

                    if(requestResult.getData().getResult().equalsIgnoreCase("SUCCESS"))
                        storeResponse.setOALSTSYN("Y");
                    else
                        storeResponse.setOALSTSYN("X");


                    updateTransactions(storeResponse);
                    rxResult = "Pending transaction updated";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            emitter.onSuccess(rxResult);
        }) .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(String s) {
                        Util.closeProgressDialog();
                        Util.showInfoToast(requireContext(), s);
                        Intent intent = new Intent("poynt.intent.action.SCANNER");
                        intent.putExtra("MODE", "SINGLE");
                        startActivityForResult(intent, SCANNER_REQUEST_CODE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Util.closeProgressDialog();
                        resetButtons();
                    }
                });
    }

}