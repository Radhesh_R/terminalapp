package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 20-Sep-18.
 */

public class TxtRobotoThin extends androidx.appcompat.widget.AppCompatTextView {
    public TxtRobotoThin(Context context) {
        super(context);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Thin.ttf"));
        init();
    }

    public TxtRobotoThin(Context context, AttributeSet attrs) {
        super(context, attrs);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Thin.ttf"));
    }

    public TxtRobotoThin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Thin.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Thin.ttf"));
    }


    private void init() {
        if (!isInEditMode()) {
           // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Thin.ttf");
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/Roboto-Thin.ttf"));
        }
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
