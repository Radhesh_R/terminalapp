package com.wps.terminalapp.custom;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.wps.terminalapp.R;

/**
 * Created by vignaraj.r on 27-Aug-18.
 */

public class DotProgressDialog extends Dialog {
    String message;
    TextView txt;

    public DotProgressDialog(Context context, String message) {
        super(context);
        this.message = message;
//        setMessage(message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.alert_progress);
        txt = (TextView) findViewById(R.id.txt_message);
        txt.setText(message);
    }

    public void setMessage(String message) {
        txt.setText(message);
    }
}
