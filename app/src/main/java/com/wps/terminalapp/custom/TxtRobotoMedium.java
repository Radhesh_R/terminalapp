package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 20-Sep-18.
 */

public class TxtRobotoMedium extends androidx.appcompat.widget.AppCompatTextView {
    public TxtRobotoMedium(Context context) {
        super(context);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Medium.ttf"));
        init();
    }

    public TxtRobotoMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Medium.ttf"));
    }

    public TxtRobotoMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Medium.ttf"));
    }


    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Medium.ttf");
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/Roboto-Medium.ttf"));
        }
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
