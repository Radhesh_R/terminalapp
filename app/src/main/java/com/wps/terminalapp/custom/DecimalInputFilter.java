package com.wps.terminalapp.custom;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vignaraj.r on 12-Aug-18.
 */

public class DecimalInputFilter implements InputFilter {

    Pattern mPattern;

    public DecimalInputFilter(int digitsBeforeZero, int digitsAfterZero) {
        mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//        Log.i("source", source.toString());
//        Log.i("start", String.valueOf(start));
//        Log.i("end", String.valueOf(end));
//        Log.i("dstart", String.valueOf(dstart));
//        Log.i("dend", String.valueOf(dend));
        Matcher matcher = mPattern.matcher(dest);
        if (!matcher.matches())
            return "";
        return null;
    }

}