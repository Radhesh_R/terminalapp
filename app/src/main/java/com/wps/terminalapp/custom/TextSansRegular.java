package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 09-Aug-18.
 */

public class TextSansRegular extends androidx.appcompat.widget.AppCompatTextView {
    public TextSansRegular(Context context) {
        super(context);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Regular.ttf"));
        init();
    }

    public TextSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Regular.ttf"));
    }

    public TextSansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Regular.ttf"));
    }


    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GoogleSans-Regular.ttf");
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/GoogleSans-Regular.ttf"));
        }
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}