package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 09-Aug-18.
 */

public class ButtonSansRegular extends Button {

    public ButtonSansRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonSansRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonSansRegular(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/GoogleSans-Regular.ttf"));
        }
    }
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }
}