package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 09-Aug-18.
 */

public class ButtonSansBold extends Button {

    public ButtonSansBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonSansBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
           // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GoogleSans-Bold.ttf");
           // setTypeface(tf);
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/GoogleSans-Bold.ttf"));
        }
    }
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }
}
