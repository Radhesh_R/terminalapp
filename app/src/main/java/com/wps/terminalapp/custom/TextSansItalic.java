package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 09-Aug-18.
 */

public class TextSansItalic extends androidx.appcompat.widget.AppCompatTextView {
    public TextSansItalic(Context context) {
        super(context);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Italic.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Italic.ttf"));
        init();
    }

    public TextSansItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Italic.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Italic.ttf"));
    }

    public TextSansItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GoogleSans-Italic.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/GoogleSans-Italic.ttf"));
    }


    private void init() {
        if (!isInEditMode()) {
            //Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/GoogleSans-Italic.ttf");
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/GoogleSans-Italic.ttf"));
        }
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}
