package com.wps.terminalapp.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.wps.terminalapp.utils.Util;

/**
 * Created by vignaraj.r on 20-Sep-18.
 */

public class TxtRobotoRegular extends androidx.appcompat.widget.AppCompatTextView {
    public TxtRobotoRegular(Context context) {
        super(context);
        //Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Regular.ttf"));
        init();
    }

    public TxtRobotoRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Regular.ttf"));
    }

    public TxtRobotoRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
       // Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        this.setTypeface(Util.GetFont(context.getAssets(), "fonts/Roboto-Regular.ttf"));
    }


    private void init() {
        if (!isInEditMode()) {
           // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
            setTypeface(Util.GetFont(getContext().getAssets(), "fonts/Roboto-Regular.ttf"));
        }
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
