package com.wps.terminalapp.history;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.appolica.flubber.Flubber;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.adapters.TransactionAdapter;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.custom.WrapLayoutManager;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.listeners.ViewTransactionListener;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.wps.terminalapp.utils.Util.currentFilter;
import static com.wps.terminalapp.utils.Util.historyList;
import static com.wps.terminalapp.utils.Util.lastSelectedFilter;

/**
 * Created by vignaraj.r on 19-Aug-18.
 */

public class SearchFragment extends Fragment {
    @BindView(R.id.search_trans)
    SearchView searchTrans;
    @BindView(R.id.list_stransactions)
    RecyclerView list_transactions;

    @BindView(R.id.layout_filter)
    RelativeLayout layout_filter;
    @BindView(R.id.img_close)
    ImageView img_close;
    @BindView(R.id.txt_filter)
    TextSansRegular txt_filter;
    @BindView(R.id.txt_message)
    TextSansRegular txt_message;
    @BindView(R.id.swipe_history)
    SwipeRefreshLayout swipeHistory;

    //
    String page_url = "";
    public static String TAG = SearchFragment.class.getSimpleName();
    Context context;
    InputMethodManager mgr;
    String fromDate = "";
    String toDate = "";
    int fYear, fMonth, fDayOfMonth;
    int tYear, tMonth, tDayOfMonth;
    DatePickerDialog fromDt = null;
    DatePickerDialog toDt = null;

    TransactionAdapter transactionAdapter;

//    LinearLayoutManager layoutManager;

    LinearLayoutManager layoutManager;

    SearchListener searchListener;
    boolean isLoading = false;


    public interface SearchListener {
        void viewSearchedTransaction(TransactionModel model);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        // Log.d(TAG, "call fragment");
        ButterKnife.bind(this, view);
        context = getActivity();
        Util.hideKeyBoard(context, searchTrans);
        showEmptyScreen();
        layoutManager = new WrapLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        list_transactions.setLayoutManager(layoutManager);
        list_transactions.addOnScrollListener(scrollListener);

        searchTrans.setOnQueryTextListener(queryTextListener);
        searchTrans.setOnKeyListener(keyListener);

        final Calendar c = Calendar.getInstance();
        fYear = c.get(Calendar.YEAR);
        fMonth = c.get(Calendar.MONTH);
        fDayOfMonth = c.get(Calendar.DAY_OF_MONTH);

        if (lastSelectedFilter.equals("ALL")) {
            txt_filter.setText(lastSelectedFilter);
            img_close.setVisibility(View.GONE);
        } else {
            txt_filter.setText(lastSelectedFilter);
            img_close.setVisibility(View.VISIBLE);
        }

        swipeHistory.setOnRefreshListener(refreshListener);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Util.hideKeyBoard(context, searchTrans);
                return false;
            }
        });
        manageTransactions();

        Util.freeMemory();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            searchListener = (MainActivity) context;
        } catch (ClassCastException e) {
            //  Log.e(TAG, e.getMessage());
            Util.showErrorToast(context, e.getMessage());
//            throw new ClassCastException(context.toString()
//                    + " must implement TransactionListener");
        }
    }

    @Override
    public void onDestroyView() {
        historyList.clear();
        super.onDestroyView();
        hideKeyBoard();
        Util.freeMemory();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        showEmptyScreen();
    }

    public ViewTransactionListener viewTransactionListener = new ViewTransactionListener() {
        @Override
        public void selectedTransaction(TransactionModel model) {
            // to view the specific tranaction.
            searchListener.viewSearchedTransaction(model);
        }
    };

    public RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int lastVissibleItemPOs = layoutManager.findLastVisibleItemPosition();
            if (!isLoading && lastVissibleItemPOs == historyList.size() - 1) {
                if (!page_url.equals("") && !page_url.equals("null")) {
                    isLoading = true;
                    loadMoreTransaction(lastVissibleItemPOs);
                }
            }
        }
    };

    public SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if (currentFilter.contains("|")) {
                String[] val = currentFilter.split("|");
                loadTransactions("ALL", "ALL", val[0], val[1], "ALL");
            } else {
                loadTransactions("ALL", "ALL", currentFilter, "ALL");
            }
        }
    };

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(final String query) {
            Handler handler = new Handler(context.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    searchByAuthCode(query);
                }
            });
            return false;
        }

        @Override
        public boolean onQueryTextChange(String query) {
            if (query.length() == 0) {
                Util.hideKeyBoard(context, searchTrans);
            }
            return false;
        }
    };

    View.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                searchTrans.clearFocus();
                searchTrans.setIconified(false);
                searchTrans.setFocusable(false);
            }
            return false;
        }
    };

    @OnClick(R.id.search_trans)
    public void iconified() {
        searchTrans.setIconified(false);
    }

    @OnClick(R.id.layout_filter)
    public void doFilter() {
        if (!txt_filter.getText().toString().equals("ALL")) {
            txt_filter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            txt_filter.setText("ALL");
            img_close.setVisibility(View.GONE);
        }
        currentFilter = "ALL";
        loadTransactions("ALL", "ALL", "ALL", "ALL");
    }

    public void selectedValue(String value) {
        Log.i("selected_val", value);
        if (value.equals("SELECT DATE")) {
            fromDt = null;
            toDt = null;
            fromDate = "";
            toDate = "";
            txt_filter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            img_close.setVisibility(View.VISIBLE);
            selectFromDate();
        } else {
            img_close.setVisibility(View.VISIBLE);
            txt_filter.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            lastSelectedFilter = value.toUpperCase();
            txt_filter.setText(lastSelectedFilter);
            currentFilter = value;
            loadTransactions("ALL", "ALL", value, "ALL");
        }
    }

    public void selectFromDate() {
        Log.d(TAG, "function - selectFromDate");
        if (fromDt == null) {
            final int[] i = {0};
            fromDt = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Log.i("pr", "pr1");

                    String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                    i[0]++;
                    Log.i("fromDate_val_I", String.valueOf(i[0]));
                    if (i[0] == 1) {
                        selectedToDate(date);
                    }
                    fromDt = null;
                }
            }, fYear, fMonth, fDayOfMonth);
            fromDt.setTitle("Choose From Date");
            fromDt.show();
        }
    }

    public void selectedToDate(String date) {
        fromDate = date.trim();
        if (!fromDate.equals("") && toDt == null) {
            final int[] i = {0};
            Log.i("fromDate", fromDate);
            String[] frDate = date.split("/");
            tYear = Integer.parseInt(frDate[2]);
            tMonth = Integer.parseInt(frDate[1]);
            tDayOfMonth = Integer.parseInt(frDate[0]);
            final Calendar c = Calendar.getInstance();
            c.set(tYear, tMonth - 1, tDayOfMonth);
            toDt = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int yr, int mon, int dom) {
                    view.setMinDate(c.getTimeInMillis());
                    String tdate = String.valueOf(dom) + "/" + String.valueOf(mon + 1) + "/" + String.valueOf(yr);
                    toDate = tdate;
                    i[0]++;
                    Log.i("toDate_val_I", String.valueOf(i[0]));
                    if (i[0] == 1) {
                        lastSelectedFilter = fromDate + " - " + toDate;
                        txt_filter.setText(lastSelectedFilter);
                        String frDate = Util.convertDate(fromDate, "dd-MMM-yyyy");
                        String trDate = Util.convertDate(toDate, "dd-MMM-yyyy");
                        currentFilter = frDate + "|" + trDate;
                        loadTransactions("ALL", "ALL", "SELECTDATE", "ALL", frDate, trDate);
                    }
                    toDt = null;
                    Log.i("ToDate", toDate);
                }
            }, tYear, tMonth - 1, tDayOfMonth);
            toDt.getDatePicker().setMinDate(c.getTimeInMillis());
            toDt.setTitle("Choose To Date");
            toDt.show();
        }
//        Util.showInfoToast(getActivity(), date);
    }

    public void hideKeyBoard() {
        mgr = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(searchTrans.getWindowToken(), 0);
    }

    public void loadTransactions(String... params) {
        swipeHistory.setRefreshing(true);
        String completeUrl = "";
        if (params[2].equals("SELECTDATE")) {
            completeUrl = GeneralUtil.getDeviceSerial() + "&strBATCH=" + params[0] + "&strTXNTYPE=" + params[1] + "&strDATE=" + params[2] + "&strCARDTYPE=" + params[3] + "&dtFrom=" + params[4] + "&dtTo=" + params[5] + "&recCount=0";
        } else {
            completeUrl = GeneralUtil.getDeviceSerial() + "&strBATCH=" + params[0] + "&strTXNTYPE=" + params[1] + "&strDATE=" + params[2] + "&strCARDTYPE=" + params[3] + "&dtFrom=" + "&dtTo=" + "&recCount=0";
        }

        int transactionSize = historyList.size();

        if (transactionSize > 0) {
            historyList.clear();
        }

        Log.d("completeUrl", completeUrl);

        WebServices.getInstance().getTransactions(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("responses##", responseStr);
                            JSONObject obj = new JSONObject(responseStr);
                            if (obj.getBoolean("IsSuccess")) {
                                JSONObject responseData = obj.getJSONObject("ResponseData");
                                page_url = responseData.optString("fetchUrl");
                                JSONArray tArray = responseData.getJSONArray("Transactions");
                                if (tArray != null && tArray.length() > 0) {
                                    for (int i = 0; i < tArray.length(); i++) {
                                        JSONObject tObject = tArray.getJSONObject(i);
                                        double amount = tObject.optDouble("mAmount");
                                        double actVal = 0;
                                        if (amount > 0) {
                                            actVal = amount / 100;
                                        }
                                        String cardNumber = tObject.optString("mCardNumber");
                                        String cardType = tObject.optString("mCardType");
                                        String cardImgType = tObject.optString("mCardTypeImg");
                                        String transactionId = tObject.optString("mTransactionId");
                                        String transactionStatus = tObject.optString("mTransactionStatus");
                                        String type = tObject.optString("mType");
                                        String cardHolderName = tObject.optString("mCardHolderName");
                                        String mDisplayNo = tObject.optString("mDisplayNo");
                                        long date = tObject.optLong("mDate");
                                        String transactionType = tObject.optString("mPayType");
                                        historyList.add(new TransactionModel(transactionId, date, type, transactionType, cardType,
                                                cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo));
                                    }
                                    if (historyList.size() > 0) {
//                                        mFadingCircleDrawable.stop();
                                        list_transactions.setVisibility(View.VISIBLE);
                                        txt_message.setVisibility(View.GONE);
                                        Flubber.with()
                                                .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                                                .repeatCount(0)
                                                .duration(1000)
                                                .createFor(list_transactions)
                                                .start();
                                        transactionAdapter = new TransactionAdapter(getActivity(), historyList, viewTransactionListener);
                                        list_transactions.setAdapter(transactionAdapter);
                                        transactionAdapter.notifyDataSetChanged();
                                    } else {
                                        list_transactions.setVisibility(View.GONE);
                                        txt_message.setVisibility(View.VISIBLE);
                                        txt_message.setText("No Transaction!");
                                    }
                                } else {
                                    list_transactions.setVisibility(View.GONE);
                                    txt_message.setVisibility(View.VISIBLE);
                                    txt_message.setText("No Transaction!");
                                }
                            } else {
                                String errMsg = obj.optString("ErrMsg");
                                list_transactions.setVisibility(View.GONE);
                                txt_message.setVisibility(View.VISIBLE);
                                txt_message.setText("Faield to load Transaction!\n" + errMsg);
                            }

                            swipeHistory.setRefreshing(false);
                        } catch (JSONException e) {
                            list_transactions.setVisibility(View.GONE);
                            txt_message.setVisibility(View.VISIBLE);
                            swipeHistory.setRefreshing(false);
                            txt_message.setText("Failed to load Transaction!\n" + e.getMessage());
                        }
                        setUnFocusable();
                        Util.freeMemory();
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Util.logLargeString("fail_respons##", message);
                        list_transactions.setVisibility(View.GONE);
                        txt_message.setVisibility(View.VISIBLE);
                        txt_message.setText("Failed to load Transaction!\n" + message);
                        swipeHistory.setRefreshing(false);
                        setUnFocusable();
                    }
                });
            }
        }, completeUrl);
    }

    public void loadMoreTransaction(final int lastPosition) {
        WebServices.getInstance().loadMoreTransactions(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("responses##", responseStr);
                            JSONObject obj = new JSONObject(responseStr);
                            if (obj.getBoolean("IsSuccess")) {
                                JSONObject responseData = obj.getJSONObject("ResponseData");
                                page_url = responseData.optString("fetchUrl");
                                JSONArray tArray = responseData.getJSONArray("Transactions");
                                if (tArray != null && tArray.length() > 0) {
                                    for (int i = 0; i < tArray.length(); i++) {
                                        JSONObject tObject = tArray.getJSONObject(i);
                                        double amount = tObject.optDouble("mAmount");
                                        double actVal = 0;
                                        if (amount > 0) {
                                            actVal = amount / 100;
                                        }
                                        String cardNumber = tObject.optString("mCardNumber");
                                        String cardType = tObject.optString("mCardType");
                                        String cardImgType = tObject.optString("mCardTypeImg");
                                        String cardHolderName = tObject.optString("mCardHolderName");
                                        String transactionId = tObject.optString("mTransactionId");
                                        String transactionStatus = tObject.optString("mTransactionStatus");
                                        String type = tObject.optString("mType");
                                        long date = tObject.optLong("mDate");
                                        String transactionType = tObject.optString("mPayType");
                                        String mDisplayNo = tObject.optString("mDisplayNo");
                                        historyList.add(new TransactionModel(transactionId, date, type, transactionType, cardType,
                                                cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo));
                                    }
                                    Flubber.with()
                                            .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                                            .repeatCount(0)
                                            .duration(1000)
                                            .createFor(list_transactions)
                                            .start();
                                    transactionAdapter = new TransactionAdapter(getActivity(), historyList, viewTransactionListener);
                                    list_transactions.setAdapter(transactionAdapter);
                                    transactionAdapter.notifyDataSetChanged();
                                    list_transactions.scrollToPosition(lastPosition);
                                } else {
                                    Util.showErrorToast(getActivity(), "No Transaction!");
                                }
                            } else {
                                String errMsg = obj.optString("ErrMsg");
                                Util.showErrorToast(getActivity(), "Failed to load Transaction!\n" + errMsg);
                            }
                            swipeHistory.setRefreshing(false);
                        } catch (JSONException e) {
                            swipeHistory.setRefreshing(false);
                            Util.showErrorToast(getActivity(), "Failed to load Transaction!\n" + e.getMessage());
                        }
                        setUnFocusable();
                        isLoading = false;
                        Util.freeMemory();
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        Util.logLargeString("fail_respons##", message);
                        Util.showErrorToast(getActivity(), "Faild to load Transaction!\n" + message);
                        setUnFocusable();
                        swipeHistory.setRefreshing(false);
                        isLoading = false;
                    }
                });
            }
        }, page_url);
    }

    public void searchByAuthCode(String authCode) {
        if (historyList.size() > 0) {
            historyList.clear();
        }
        list_transactions.setAdapter(null);

        WebServices.getInstance().getTxnByAuthCode(context, authCode, new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("responses##", responseStr);
                            JSONObject obj = new JSONObject(responseStr);
                            if (obj.getBoolean("IsSuccess")) {
                                JSONObject responseData = obj.getJSONObject("ResponseData");
                                page_url = responseData.optString("fetchUrl");
                                JSONArray tArray = responseData.getJSONArray("Transactions");
                                if (tArray != null && tArray.length() > 0) {
                                    for (int i = 0; i < tArray.length(); i++) {
                                        JSONObject tObject = tArray.getJSONObject(i);
                                        long amount = tObject.optLong("mAmount");
                                        double actVal = 0;
                                        if (amount > 0) {
                                            actVal = amount / 100;
                                        }
                                        String cardNumber = tObject.optString("mCardNumber");
                                        String cardType = tObject.optString("mCardType");
                                        String cardImgType = tObject.optString("mCardTypeImg");
                                        String transactionId = tObject.optString("mTransactionId");
                                        String transactionStatus = tObject.optString("mTransactionStatus");
                                        String type = tObject.optString("mType");
                                        String cardHolderName = tObject.optString("mCardHolderName");
                                        long date = tObject.optLong("mDate");
                                        String transactionType = tObject.optString("mPayType");
                                        String mDisplayNo = tObject.optString("mDisplayNo");
                                        historyList.add(new TransactionModel(transactionId, date, type, transactionType, cardType,
                                                cardImgType, cardHolderName, cardNumber, actVal, transactionStatus, mDisplayNo));
                                    }
                                    if (historyList.size() > 0) {
                                        list_transactions.setVisibility(View.VISIBLE);
                                        Flubber.with()
                                                .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                                                .repeatCount(0)
                                                .duration(1000)
                                                .createFor(list_transactions)
                                                .start();
                                        transactionAdapter = new TransactionAdapter(getActivity(), historyList, viewTransactionListener);
                                        list_transactions.setAdapter(transactionAdapter);
                                        transactionAdapter.notifyDataSetChanged();
                                    } else {
                                        list_transactions.setVisibility(View.GONE);
                                        txt_message.setVisibility(View.VISIBLE);
                                        txt_message.setText("No Transaction!");
                                    }
                                    setUnFocusable();
                                } else {
                                    list_transactions.setVisibility(View.GONE);
                                    txt_message.setVisibility(View.VISIBLE);
                                    txt_message.setText("No Transaction!");
                                    setUnFocusable();
                                }
                            } else {
                                String errMsg = obj.optString("ErrMsg");
                                list_transactions.setVisibility(View.GONE);
                                txt_message.setVisibility(View.VISIBLE);
                                txt_message.setText("Faild to load Transaction!\n" + errMsg);
                                setUnFocusable();
                            }
                        } catch (JSONException e) {
                            list_transactions.setVisibility(View.GONE);
                            txt_message.setVisibility(View.VISIBLE);
                            txt_message.setText("Faild to load Transaction!\n" + e.getMessage());
                            setUnFocusable();
                        }

                        Util.freeMemory();
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showErrorToast(context, message);
                        list_transactions.setVisibility(View.GONE);
                        txt_message.setVisibility(View.VISIBLE);
                        txt_message.setText(message);
                        Util.hideKeyBoard(context, searchTrans);
                    }
                });
            }
        });

    }

    public void manageTransactions() {
        if (Util.historyList.size() > 0) {
            list_transactions.setVisibility(View.VISIBLE);
            txt_message.setVisibility(View.GONE);
            Flubber.with()
                    .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                    .repeatCount(0)
                    .duration(1000)
                    .createFor(list_transactions)
                    .start();
            transactionAdapter = new TransactionAdapter(getActivity(), Util.historyList, viewTransactionListener);
            list_transactions.setAdapter(transactionAdapter);
            transactionAdapter.notifyDataSetChanged();
        } else {
            loadTransactions("ALL", "ALL", "ALL", "ALL");
        }
    }

    public void setUnFocusable() {
        searchTrans.setQuery("", false);
        searchTrans.clearFocus();
        searchTrans.setFocusable(false);
        Util.hideKeyBoard(context, searchTrans);
    }

    public void showEmptyScreen() {
        Log.i("method", "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        Log.e(TAG, "unable to show screen screen");
                    }
                }
            }
        });
    }

}
