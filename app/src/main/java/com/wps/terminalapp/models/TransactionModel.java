package com.wps.terminalapp.models;

/**
 * Created by vignaraj.r on 26-Aug-18.
 */

public class TransactionModel {
    String mTransactionId = "";
    long mDate = 0L;
    String mType = "";
    String mTransactionType = "";
    String mCardType = "";
    String mCardImage = "";
    String mCardNumber = "";
    double mAmount = 0;
    String mTransactionStatus = "";
    String mCardHolderName = "";
    String mDisplayNo = "";
    public boolean newTransaction = false;

    public TransactionModel() {
    }

    public TransactionModel(String mTransactionId, long mDate, String mType, String mTransactionType, String mCardType, String mCardImage, String mCardHolderName, String mCardNumber, double mAmount, String mTransactionStatus, String mDisplayNo) {
        this.mTransactionId = mTransactionId;
        this.mDate = mDate;
        this.mType = mType;
        this.mTransactionType = mTransactionType;
        this.mCardType = mCardType;
        this.mCardImage = mCardImage;
        this.mCardHolderName = mCardHolderName;
        this.mCardNumber = mCardNumber;
        this.mAmount = mAmount;
        this.mTransactionStatus = mTransactionStatus;
        this.mDisplayNo = mDisplayNo;
    }


    public long getmDate() {
        return mDate;
    }

    public void setmDate(long mDate) {
        this.mDate = mDate;
    }

    public String getmTransactionId() {
        return mTransactionId;
    }

    public void setmTransactionId(String mTransactionId) {
        this.mTransactionId = mTransactionId;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmCardType() {
        return mCardType;
    }

    public void setmCardType(String mCardType) {
        this.mCardType = mCardType;
    }

    public String getmCardNumber() {
        return mCardNumber;
    }

    public void setmCardNumber(String mCardNumber) {
        this.mCardNumber = mCardNumber;
    }

    public double getmAmount() {
        return mAmount;
    }

    public void setmAmount(double mAmount) {
        this.mAmount = mAmount;
    }

    public String getmTransactionStatus() {
        return mTransactionStatus;
    }

    public void setmTransactionStatus(String mTransactionStatus) {
        this.mTransactionStatus = mTransactionStatus;
    }

    public String getmTransactionType() {
        return mTransactionType;
    }

    public void setmTransactionType(String mTransactionType) {
        this.mTransactionType = mTransactionType;
    }

    public String getmCardImage() {
        return mCardImage;
    }

    public void setmCardImage(String mCardImage) {
        this.mCardImage = mCardImage;
    }

    public String getmCardHolderName() {
        return mCardHolderName;
    }

    public void setmCardHolderName(String mCardHolderName) {
        this.mCardHolderName = mCardHolderName;
    }

    public String getmDisplayNo() {
        return mDisplayNo;
    }

    public void setmDisplayNo(String mDisplayNo) {
        this.mDisplayNo = mDisplayNo;
    }
}
