package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderRevokeResponse {
    @SerializedName("code")
    @Expose
    private Integer code;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("sign")
    @Expose
    private String sign;

    @SerializedName("status_code")
    @Expose
    private String statusCode;

    @SerializedName("status_msg")
    @Expose
    private String statusMsg;

    @SerializedName("time_stamp")
    @Expose
    private String timeStamp;

    @SerializedName("version")
    @Expose
    private String version;

    @SerializedName("data")
    @Expose
    private OrderRevokeResponseData data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public OrderRevokeResponseData getData() {
        return data;
    }

    public void setData(OrderRevokeResponseData data) {
        this.data = data;
    }
}
