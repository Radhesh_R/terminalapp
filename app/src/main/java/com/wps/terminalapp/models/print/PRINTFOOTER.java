package com.wps.terminalapp.models.print;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PRINTFOOTER {

    @SerializedName("printType1")
    @Expose
    private String printType1;
    @SerializedName("printData1")
    @Expose
    private String printData1;
    @SerializedName("printAlign1")
    @Expose
    private Integer printAlign1;
    @SerializedName("printFont1")
    @Expose
    private Integer printFont1;
    @SerializedName("printType2")
    @Expose
    private String printType2;
    @SerializedName("printData2")
    @Expose
    private String printData2;
    @SerializedName("printAlign2")
    @Expose
    private Integer printAlign2;
    @SerializedName("printFont2")
    @Expose
    private Integer printFont2;

    public String getPrintType1() {
        return printType1;
    }

    public void setPrintType1(String printType1) {
        this.printType1 = printType1;
    }

    public String getPrintData1() {
        return printData1;
    }

    public void setPrintData1(String printData1) {
        this.printData1 = printData1;
    }

    public Integer getPrintAlign1() {
        return printAlign1;
    }

    public void setPrintAlign1(Integer printAlign1) {
        this.printAlign1 = printAlign1;
    }

    public Integer getPrintFont1() {
        return printFont1;
    }

    public void setPrintFont1(Integer printFont1) {
        this.printFont1 = printFont1;
    }

    public String getPrintType2() {
        return printType2;
    }

    public void setPrintType2(String printType2) {
        this.printType2 = printType2;
    }

    public String getPrintData2() {
        return printData2;
    }

    public void setPrintData2(String printData2) {
        this.printData2 = printData2;
    }

    public Integer getPrintAlign2() {
        return printAlign2;
    }

    public void setPrintAlign2(Integer printAlign2) {
        this.printAlign2 = printAlign2;
    }

    public Integer getPrintFont2() {
        return printFont2;
    }

    public void setPrintFont2(Integer printFont2) {
        this.printFont2 = printFont2;
    }

}
