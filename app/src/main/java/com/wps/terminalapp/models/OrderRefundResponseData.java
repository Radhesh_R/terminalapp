package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderRefundResponseData {
    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("appid")
    @Expose
    private String appid;

    @SerializedName("channel")
    @Expose
    private String channel;

    @SerializedName("nonce_str")
    @Expose
    private String nonceStr;

    @SerializedName("mch_order_no")
    @Expose
    private String mchOrderNo;

    @SerializedName("ksher_order_no")
    @Expose
    private String ksherOrderNo;

    @SerializedName("channel_order_no")
    @Expose
    private String channelOrderNo;

    @SerializedName("total_fee")
    @Expose
    private Integer totalFee;

    @SerializedName("fee_type")
    @Expose
    private String feeType;

    @SerializedName("mch_refund_no")
    @Expose
    private String mchRefundNo;

    @SerializedName("ksher_refund_no")
    @Expose
    private String ksherRefundNo;

    @SerializedName("channel_refund_no")
    @Expose
    private String channelRefundNo;

    @SerializedName("refund_fee")
    @Expose
    private Integer refundFee;

    @SerializedName("cash_refund_fee")
    @Expose
    private Integer cashRefundFee;

    @SerializedName("refund_time")
    @Expose
    private String refundTime;

    @SerializedName("operator_id")
    @Expose
    private String operatorId;

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    @SerializedName("attach")
    @Expose
    private String attach;

    @SerializedName("err_code")
    @Expose
    private String errCode;

    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public String getKsherOrderNo() {
        return ksherOrderNo;
    }

    public void setKsherOrderNo(String ksherOrderNo) {
        this.ksherOrderNo = ksherOrderNo;
    }

    public String getChannelOrderNo() {
        return channelOrderNo;
    }

    public void setChannelOrderNo(String channelOrderNo) {
        this.channelOrderNo = channelOrderNo;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getMchRefundNo() {
        return mchRefundNo;
    }

    public void setMchRefundNo(String mchRefundNo) {
        this.mchRefundNo = mchRefundNo;
    }

    public String getKsherRefundNo() {
        return ksherRefundNo;
    }

    public void setKsherRefundNo(String ksherRefundNo) {
        this.ksherRefundNo = ksherRefundNo;
    }

    public String getChannelRefundNo() {
        return channelRefundNo;
    }

    public void setChannelRefundNo(String channelRefundNo) {
        this.channelRefundNo = channelRefundNo;
    }

    public Integer getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(Integer refundFee) {
        this.refundFee = refundFee;
    }

    public Integer getCashRefundFee() {
        return cashRefundFee;
    }

    public void setCashRefundFee(Integer cashRefundFee) {
        this.cashRefundFee = cashRefundFee;
    }

    public String getRefundTime() {
        return refundTime;
    }

    public void setRefundTime(String refundTime) {
        this.refundTime = refundTime;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
