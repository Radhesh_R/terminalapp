package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderRequestResponseData {

    @SerializedName("appid")
    @Expose
    private String appid;

    @SerializedName("attach")
    @Expose
    private String attach;

    @SerializedName("cash_fee")
    @Expose
    private Integer cashFee;

    @SerializedName("cash_fee_type")
    @Expose
    private String cashFeeType;

    @SerializedName("channel_order_no")
    @Expose
    private String channelOrderNo;

    @SerializedName("device_id")
    @Expose
    private String deviceId;

    @SerializedName("fee_type")
    @Expose
    private String feeType;

    @SerializedName("ksher_order_no")
    @Expose
    private String ksherOrderNo;

    @SerializedName("mch_order_no")
    @Expose
    private String mchOrderNo;

    @SerializedName("nonce_str")
    @Expose
    private String nonceStr;

    @SerializedName("openid")
    @Expose
    private String openid;

    @SerializedName("operation")
    @Expose
    private String operation;

    @SerializedName("operator_id")
    @Expose
    private String operatorId;

    @SerializedName("rate")
    @Expose
    private String rate;

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("time_end")
    @Expose
    private String timeEnd;

    @SerializedName("total_fee")
    @Expose
    private Integer totalFee;

    @SerializedName("channel")
    @Expose
    private String channel;

    @SerializedName("err_msg")
    @Expose
    private String errMsg;

    @SerializedName("err_code")
    @Expose
    private String errCode;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public Integer getCashFee() {
        return cashFee;
    }

    public void setCashFee(Integer cashFee) {
        this.cashFee = cashFee;
    }

    public String getCashFeeType() {
        return cashFeeType;
    }

    public void setCashFeeType(String cashFeeType) {
        this.cashFeeType = cashFeeType;
    }

    public String getChannelOrderNo() {
        return channelOrderNo;
    }

    public void setChannelOrderNo(String channelOrderNo) {
        this.channelOrderNo = channelOrderNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getKsherOrderNo() {
        return ksherOrderNo;
    }

    public void setKsherOrderNo(String ksherOrderNo) {
        this.ksherOrderNo = ksherOrderNo;
    }

    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
}
