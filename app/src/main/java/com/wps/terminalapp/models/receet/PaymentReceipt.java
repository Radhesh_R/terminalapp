package com.wps.terminalapp.models.receet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentReceipt {
    @SerializedName("paymentReceiptTypeId")
    @Expose
    private Integer paymentReceiptTypeId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("cash_batchNo")
    @Expose
    private String cash_batchNo;
    @SerializedName("cash_orderAMT")
    @Expose
    private String cash_orderAMT;
    @SerializedName("cash_tenderAMT")
    @Expose
    private String cash_tenderAMT;
    @SerializedName("cash_changeAMT")
    @Expose
    private String cash_changeAMT;
    @SerializedName("footer")
    @Expose
    private String footer;
    @SerializedName("card_TID")
    @Expose
    private String card_TID;
    @SerializedName("card_MID")
    @Expose
    private String card_MID;
    @SerializedName("card_AMEXMID")
    @Expose
    private String card_AMEXMID;
    @SerializedName("card_transType")
    @Expose
    private String card_transType;
    @SerializedName("card_type")
    @Expose
    private String card_type;
    @SerializedName("card_no")
    @Expose
    private String card_no;
    @SerializedName("card_expDate")
    @Expose
    private String card_expDate;
    @SerializedName("card_holderName")
    @Expose
    private String card_holderName;
    @SerializedName("card_panSeqNo")
    @Expose
    private String card_panSeqNo;
    @SerializedName("card_receiptNo")
    @Expose
    private String card_receiptNo;
    @SerializedName("card_batch")
    @Expose
    private String card_batch;
    @SerializedName("card_approvalCode")
    @Expose
    private String card_approvalCode;
    @SerializedName("card_orderAMT")
    @Expose
    private String card_orderAMT;
    @SerializedName("card_tipAMT")
    @Expose
    private String card_tipAMT;
    @SerializedName("card_netAMT")
    @Expose
    private String card_netAMT;
    @SerializedName("card_amount")
    @Expose
    private String card_amount;
    @SerializedName("card_message")
    @Expose
    private String card_message;
    @SerializedName("card_authorized")
    @Expose
    private String card_authorized;
    @SerializedName("card_emvAID")
    @Expose
    private String card_emvAID;
    @SerializedName("card_emvLabel")
    @Expose
    private String card_emvLabel;
    @SerializedName("card_emvTVR")
    @Expose
    private String card_emvTVR;
    @SerializedName("card_emvTSI")
    @Expose
    private String card_emvTSI;
    @SerializedName("card_emvCID")
    @Expose
    private String card_emvCID;
    @SerializedName("card_emvAC")
    @Expose
    private String card_emvAC;
    @SerializedName("card_emvCVM")
    @Expose
    private String card_emvCVM;
    @SerializedName("card_emvRRN")
    @Expose
    private String card_emvRRN;
    @SerializedName("card_emvTTC")
    @Expose
    private String card_emvTTC;

    public Integer getPaymentReceiptTypeId() {
        return paymentReceiptTypeId;
    }

    public void setPaymentReceiptTypeId(Integer paymentReceiptTypeId) {
        this.paymentReceiptTypeId = paymentReceiptTypeId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCash_batchNo() {
        return cash_batchNo;
    }

    public void setCash_batchNo(String cash_batchNo) {
        this.cash_batchNo = cash_batchNo;
    }

    public String getCash_orderAMT() {
        return cash_orderAMT;
    }

    public void setCash_orderAMT(String cash_orderAMT) {
        this.cash_orderAMT = cash_orderAMT;
    }

    public String getCash_tenderAMT() {
        return cash_tenderAMT;
    }

    public void setCash_tenderAMT(String cash_tenderAMT) {
        this.cash_tenderAMT = cash_tenderAMT;
    }

    public String getCash_changeAMT() {
        return cash_changeAMT;
    }

    public void setCash_changeAMT(String cash_changeAMT) {
        this.cash_changeAMT = cash_changeAMT;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCard_TID() {
        return card_TID;
    }

    public void setCard_TID(String card_TID) {
        this.card_TID = card_TID;
    }

    public String getCard_MID() {
        return card_MID;
    }

    public void setCard_MID(String card_MID) {
        this.card_MID = card_MID;
    }

    public String getCard_AMEXMID() {
        return card_AMEXMID;
    }

    public void setCard_AMEXMID(String card_AMEXMID) {
        this.card_AMEXMID = card_AMEXMID;
    }

    public String getCard_transType() {
        return card_transType;
    }

    public void setCard_transType(String card_transType) {
        this.card_transType = card_transType;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_expDate() {
        return card_expDate;
    }

    public void setCard_expDate(String card_expDate) {
        this.card_expDate = card_expDate;
    }

    public String getCard_holderName() {
        return card_holderName;
    }

    public void setCard_holderName(String card_holderName) {
        this.card_holderName = card_holderName;
    }

    public String getCard_panSeqNo() {
        return card_panSeqNo;
    }

    public void setCard_panSeqNo(String card_panSeqNo) {
        this.card_panSeqNo = card_panSeqNo;
    }

    public String getCard_receiptNo() {
        return card_receiptNo;
    }

    public void setCard_receiptNo(String card_receiptNo) {
        this.card_receiptNo = card_receiptNo;
    }

    public String getCard_batch() {
        return card_batch;
    }

    public void setCard_batch(String card_batch) {
        this.card_batch = card_batch;
    }

    public String getCard_approvalCode() {
        return card_approvalCode;
    }

    public void setCard_approvalCode(String card_approvalCode) {
        this.card_approvalCode = card_approvalCode;
    }

    public String getCard_orderAMT() {
        return card_orderAMT;
    }

    public void setCard_orderAMT(String card_orderAMT) {
        this.card_orderAMT = card_orderAMT;
    }

    public String getCard_tipAMT() {
        return card_tipAMT;
    }

    public void setCard_tipAMT(String card_tipAMT) {
        this.card_tipAMT = card_tipAMT;
    }

    public String getCard_netAMT() {
        return card_netAMT;
    }

    public void setCard_netAMT(String card_netAMT) {
        this.card_netAMT = card_netAMT;
    }

    public String getCard_amount() {
        return card_amount;
    }

    public void setCard_amount(String card_amount) {
        this.card_amount = card_amount;
    }

    public String getCard_message() {
        return card_message;
    }

    public void setCard_message(String card_message) {
        this.card_message = card_message;
    }

    public String getCard_authorized() {
        return card_authorized;
    }

    public void setCard_authorized(String card_authorized) {
        this.card_authorized = card_authorized;
    }

    public String getCard_emvAID() {
        return card_emvAID;
    }

    public void setCard_emvAID(String card_emvAID) {
        this.card_emvAID = card_emvAID;
    }

    public String getCard_emvLabel() {
        return card_emvLabel;
    }

    public void setCard_emvLabel(String card_emvLabel) {
        this.card_emvLabel = card_emvLabel;
    }

    public String getCard_emvTVR() {
        return card_emvTVR;
    }

    public void setCard_emvTVR(String card_emvTVR) {
        this.card_emvTVR = card_emvTVR;
    }

    public String getCard_emvTSI() {
        return card_emvTSI;
    }

    public void setCard_emvTSI(String card_emvTSI) {
        this.card_emvTSI = card_emvTSI;
    }

    public String getCard_emvCID() {
        return card_emvCID;
    }

    public void setCard_emvCID(String card_emvCID) {
        this.card_emvCID = card_emvCID;
    }

    public String getCard_emvAC() {
        return card_emvAC;
    }

    public void setCard_emvAC(String card_emvAC) {
        this.card_emvAC = card_emvAC;
    }

    public String getCard_emvCVM() {
        return card_emvCVM;
    }

    public void setCard_emvCVM(String card_emvCVM) {
        this.card_emvCVM = card_emvCVM;
    }

    public String getCard_emvRRN() {
        return card_emvRRN;
    }

    public void setCard_emvRRN(String card_emvRRN) {
        this.card_emvRRN = card_emvRRN;
    }

    public String getCard_emvTTC() {
        return card_emvTTC;
    }

    public void setCard_emvTTC(String card_emvTTC) {
        this.card_emvTTC = card_emvTTC;
    }
}
