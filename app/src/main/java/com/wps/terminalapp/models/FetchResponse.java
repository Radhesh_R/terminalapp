package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchResponse {

    @SerializedName("DEVICE_ID")
    @Expose
    private String dEVICEID;

    @SerializedName("ODEV_KSHER_APPID")
    @Expose
    private String oDEVKSHERAPPID;

    @SerializedName("ODEV_KSHER_PVT_KEY")
    @Expose
    private String oDEVKSHERPVTKEY;

    @SerializedName("ODEV_KSHER_PUB_KEY")
    @Expose
    private String oDEVKSHERPUBKEY;

    @SerializedName("ODEV_KSHER_TRACK_ALL")
    @Expose
    private String oDEVKSHERTRACKALL;

    @SerializedName("ODEV_TXN_BATCH_NO")
    @Expose
    private String oDEVTXNBATCHNO;

    @SerializedName("Total_fee")
    @Expose
    private Integer totalFee;

    @SerializedName("fee_type")
    @Expose
    private String feeType;

    @SerializedName("channel")
    @Expose
    private String channel;

    @SerializedName("Mch_order_no")
    @Expose
    private String mchOrderNo;

    @SerializedName("INV_NO")
    @Expose
    private String iNVNO;

    @SerializedName("query_count")
    @Expose
    private String queryCount;


    public String getDEVICEID() {
        return dEVICEID;
    }

    public void setDEVICEID(String dEVICEID) {
        this.dEVICEID = dEVICEID;
    }

    public String getODEVKSHERAPPID() {
        return oDEVKSHERAPPID;
    }

    public void setODEVKSHERAPPID(String oDEVKSHERAPPID) {
        this.oDEVKSHERAPPID = oDEVKSHERAPPID;
    }

    public String getODEVKSHERPVTKEY() {
        return oDEVKSHERPVTKEY;
    }

    public void setODEVKSHERPVTKEY(String oDEVKSHERPVTKEY) {
        this.oDEVKSHERPVTKEY = oDEVKSHERPVTKEY;
    }

    public String getODEVKSHERPUBKEY() {
        return oDEVKSHERPUBKEY;
    }

    public void setODEVKSHERPUBKEY(String oDEVKSHERPUBKEY) {
        this.oDEVKSHERPUBKEY = oDEVKSHERPUBKEY;
    }

    public String getODEVKSHERTRACKALL() {
        return oDEVKSHERTRACKALL;
    }

    public void setODEVKSHERTRACKALL(String oDEVKSHERTRACKALL) {
        this.oDEVKSHERTRACKALL = oDEVKSHERTRACKALL;
    }

    public String getODEVTXNBATCHNO() {
        return oDEVTXNBATCHNO;
    }

    public void setODEVTXNBATCHNO(String oDEVTXNBATCHNO) {
        this.oDEVTXNBATCHNO = oDEVTXNBATCHNO;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Integer totalFee) {
        this.totalFee = totalFee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMchOrderNo() {
        return mchOrderNo;
    }

    public void setMchOrderNo(String mchOrderNo) {
        this.mchOrderNo = mchOrderNo;
    }

    public String getINVNO() {
        return iNVNO;
    }

    public void setINVNO(String iNVNO) {
        this.iNVNO = iNVNO;
    }

    public String getQueryCount() {
        return queryCount;
    }

    public void setQueryCount(String queryCount) {
        this.queryCount = queryCount;
    }

}