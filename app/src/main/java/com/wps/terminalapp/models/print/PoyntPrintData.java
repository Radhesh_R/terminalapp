package com.wps.terminalapp.models.print;

import android.graphics.Bitmap;

import co.poynt.os.model.PrintedReceipt;

public class PoyntPrintData {

    PrintedReceipt original;
    PrintedReceipt head;
    Bitmap logo;
    Bitmap qr;
    Bitmap barcode;
    PrintedReceipt footer;

    public PrintedReceipt getOriginal() {
        return original;
    }

    public void setOriginal(PrintedReceipt original) {
        this.original = original;
    }

    public PrintedReceipt getHead() {
        return head;
    }

    public void setHead(PrintedReceipt head) {
        this.head = head;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap logo) {
        this.logo = logo;
    }

    public Bitmap getQr() {
        return qr;
    }

    public void setQr(Bitmap qr) {
        this.qr = qr;
    }

    public Bitmap getBarcode() {
        return barcode;
    }

    public void setBarcode(Bitmap barcode) {
        this.barcode = barcode;
    }

    public PrintedReceipt getFooter() {
        return footer;
    }

    public void setFooter(PrintedReceipt footer) {
        this.footer = footer;
    }
}
