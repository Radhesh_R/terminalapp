package com.wps.terminalapp.models.print;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrintData {

    @SerializedName("PRINTER_IP")
    @Expose
    private String PRINTER_IP;

    @SerializedName("PRINTER_PORT")
    @Expose
    private String PRINTER_PORT;

    @SerializedName("PRINTER_NAME")
    @Expose
    private String PRINTER_NAME;

    @SerializedName("PRINTER_TYPE")
    @Expose
    private String PRINTER_TYPE;

    @SerializedName("PRINTER_LISTED_YN")
    @Expose
    private String PRINTER_LISTED_YN;

    @SerializedName("PRINT_JOB_ID")
    @Expose
    private String PRINT_JOB_ID;

    @SerializedName("PRINT_ORDER_ID")
    @Expose
    private String PRINT_ORDER_ID;

    @SerializedName("PRINT_HEADER")
    @Expose
    private List<PRINTHEADER> pRINTHEADER = null;

    @SerializedName("PRINT_BODY")
    @Expose
    private List<PRINTBODY> pRINTBODY = null;

    @SerializedName("PRINT_FOOTER")
    @Expose
    private List<PRINTFOOTER> pRINTFOOTER = null;


    public String getPRINTER_IP() {
        return PRINTER_IP;
    }

    public void setPRINTER_IP(String PRINTER_IP) {
        this.PRINTER_IP = PRINTER_IP;
    }

    public String getPRINTER_PORT() {
        return PRINTER_PORT;
    }

    public void setPRINTER_PORT(String PRINTER_PORT) {
        this.PRINTER_PORT = PRINTER_PORT;
    }

    public String getPRINTER_NAME() {
        return PRINTER_NAME;
    }

    public void setPRINTER_NAME(String PRINTER_NAME) {
        this.PRINTER_NAME = PRINTER_NAME;
    }

    public String getPRINTER_TYPE() {
        return PRINTER_TYPE;
    }

    public void setPRINTER_TYPE(String PRINTER_TYPE) {
        this.PRINTER_TYPE = PRINTER_TYPE;
    }

    public String getPRINTER_LISTED_YN() {
        return PRINTER_LISTED_YN;
    }

    public void setPRINTER_LISTED_YN(String PRINTER_LISTED_YN) {
        this.PRINTER_LISTED_YN = PRINTER_LISTED_YN;
    }

    public String getPRINT_JOB_ID() {
        return PRINT_JOB_ID;
    }

    public void setPRINT_JOB_ID(String PRINT_JOB_ID) {
        this.PRINT_JOB_ID = PRINT_JOB_ID;
    }

    public String getPRINT_ORDER_ID() {
        return PRINT_ORDER_ID;
    }

    public void setPRINT_ORDER_ID(String PRINT_ORDER_ID) {
        this.PRINT_ORDER_ID = PRINT_ORDER_ID;
    }

    public List<PRINTHEADER> getpRINTHEADER() {
        return pRINTHEADER;
    }

    public void setpRINTHEADER(List<PRINTHEADER> pRINTHEADER) {
        this.pRINTHEADER = pRINTHEADER;
    }

    public List<PRINTBODY> getpRINTBODY() {
        return pRINTBODY;
    }

    public void setpRINTBODY(List<PRINTBODY> pRINTBODY) {
        this.pRINTBODY = pRINTBODY;
    }

    public List<PRINTFOOTER> getpRINTFOOTER() {
        return pRINTFOOTER;
    }

    public void setpRINTFOOTER(List<PRINTFOOTER> pRINTFOOTER) {
        this.pRINTFOOTER = pRINTFOOTER;
    }
}
