package com.wps.terminalapp.models.receet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transactions {
    @SerializedName("mAmount")
    @Expose
    private Double mAmount;
    @SerializedName("mCardNumber")
    @Expose
    private Object mCardNumber;
    @SerializedName("mCardType")
    @Expose
    private String mCardType;
    @SerializedName("mCardTypeImg")
    @Expose
    private String mCardTypeImg;
    @SerializedName("mCardHolderName")
    @Expose
    private String mCardHolderName;
    @SerializedName("mTransactionId")
    @Expose
    private String mTransactionId;
    @SerializedName("mTransactionStatus")
    @Expose
    private String mTransactionStatus;
    @SerializedName("mType")
    @Expose
    private String mType;
    @SerializedName("mPayType")
    @Expose
    private String mPayType;
    @SerializedName("mDate")
    @Expose
    private long mDate;
    @SerializedName("mDisplayNo")
    @Expose
    private Object mDisplayNo;

    public Double getmAmount() {
        return mAmount;
    }

    public void setmAmount(Double mAmount) {
        this.mAmount = mAmount;
    }

    public Object getmCardNumber() {
        return mCardNumber;
    }

    public void setmCardNumber(Object mCardNumber) {
        this.mCardNumber = mCardNumber;
    }

    public String getmCardType() {
        return mCardType;
    }

    public void setmCardType(String mCardType) {
        this.mCardType = mCardType;
    }

    public String getmCardTypeImg() {
        return mCardTypeImg;
    }

    public void setmCardTypeImg(String mCardTypeImg) {
        this.mCardTypeImg = mCardTypeImg;
    }

    public String getmCardHolderName() {
        return mCardHolderName;
    }

    public void setmCardHolderName(String mCardHolderName) {
        this.mCardHolderName = mCardHolderName;
    }

    public String getmTransactionId() {
        return mTransactionId;
    }

    public void setmTransactionId(String mTransactionId) {
        this.mTransactionId = mTransactionId;
    }

    public String getmTransactionStatus() {
        return mTransactionStatus;
    }

    public void setmTransactionStatus(String mTransactionStatus) {
        this.mTransactionStatus = mTransactionStatus;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmPayType() {
        return mPayType;
    }

    public void setmPayType(String mPayType) {
        this.mPayType = mPayType;
    }

    public long getmDate() {
        return mDate;
    }

    public void setmDate(long mDate) {
        this.mDate = mDate;
    }

    public Object getmDisplayNo() {
        return mDisplayNo;
    }

    public void setmDisplayNo(Object mDisplayNo) {
        this.mDisplayNo = mDisplayNo;
    }
}
