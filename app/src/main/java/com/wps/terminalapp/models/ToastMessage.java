package com.wps.terminalapp.models;

import com.shashank.sony.fancytoastlib.FancyToast;

public class ToastMessage {

    public static final int LENGTH_LONG = FancyToast.LENGTH_LONG;
    public static final int LENGTH_SHORT = FancyToast.LENGTH_SHORT;

    public static final int SUCCESS = FancyToast.SUCCESS;
    public static final int ERROR = FancyToast.ERROR;
    public static final int INFO = FancyToast.INFO;
    public static final int WARNING = FancyToast.WARNING;
    public static final int DEFAULT = FancyToast.DEFAULT;

    private String message = "";
    private int length = 0;
    private int mode = 0;
    private int messageResource = 0;

    public ToastMessage(String message, int length, int mode) {
        this.message = message;
        this.length = length;
        this.mode = mode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getMessageResource() {
        return messageResource;
    }

    public void setMessageResource(int messageResource) {
        this.messageResource = messageResource;
    }
}
