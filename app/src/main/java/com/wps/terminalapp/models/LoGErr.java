package com.wps.terminalapp.models;

/**
 * Created by vignaraj.r on 01-Mar-18.
 */

public class LoGErr {
    String devcSrNo;
    String errMsg;

    public String getDevcSrNo() {
        return devcSrNo;
    }

    public void setDevcSrNo(String devcSrNo) {
        this.devcSrNo = devcSrNo;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

}
