package com.wps.terminalapp.models.receet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OdrTxnJSON {

    @SerializedName("media")
    @Expose
    private String media;

    @SerializedName("action")
    @Expose
    private Integer action;

    @SerializedName("languageId")
    @Expose
    private Integer languageId;

    @SerializedName("address")
    @Expose
    private Address address;

    @SerializedName("paymentReceipt")
    @Expose
    private PaymentReceipt paymentReceipt;

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public PaymentReceipt getPaymentReceipt() {
        return paymentReceipt;
    }

    public void setPaymentReceipt(PaymentReceipt paymentReceipt) {
        this.paymentReceipt = paymentReceipt;
    }
}
