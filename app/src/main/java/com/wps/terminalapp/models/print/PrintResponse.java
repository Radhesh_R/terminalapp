package com.wps.terminalapp.models.print;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrintResponse {

    @SerializedName("IsSuccess")
    @Expose
    private Boolean isSuccess;

    @SerializedName("ErrCode")
    @Expose
    private String errCode;

    @SerializedName("ErrMsg")
    @Expose
    private String errMsg;

    @SerializedName("ResponseData")
    @Expose
    private List<PrintData> responseData;

    @SerializedName("OtherData1")
    @Expose
    private String otherData1 = null;

    @SerializedName("OtherData2")
    @Expose
    private Object otherData2;

    @SerializedName("MID")
    @Expose
    private Object mID;

    @SerializedName("TID")
    @Expose
    private Object tID;

    public Boolean getSuccess() {
        return isSuccess;
    }

    public void setSuccess(Boolean success) {
        isSuccess = success;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<PrintData> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<PrintData> responseData) {
        this.responseData = responseData;
    }

    public String getOtherData1() {
        return otherData1;
    }

    public void setOtherData1(String otherData1) {
        this.otherData1 = otherData1;
    }

    public Object getOtherData2() {
        return otherData2;
    }

    public void setOtherData2(Object otherData2) {
        this.otherData2 = otherData2;
    }

    public Object getmID() {
        return mID;
    }

    public void setmID(Object mID) {
        this.mID = mID;
    }

    public Object gettID() {
        return tID;
    }

    public void settID(Object tID) {
        this.tID = tID;
    }
}
