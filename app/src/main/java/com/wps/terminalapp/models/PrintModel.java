package com.wps.terminalapp.models;

/**
 * Created by vignaraj.r on 11-Jun-18.
 */

public class PrintModel {
    // for type1
    public String printType1 = "";
    public String printData1 = "";
    public int printAlign1 = 0;
    public int printFont1 = 0;
    // for type2
    public String printType2 = "";
    public String printData2 = "";
    public int printAlign2 = 0;
    public int printFont2 = 0;

    public PrintModel(String printType1, String printData1, int printAlign1, int printFont1, String printType2, String printData2, int printAlign2, int printFont2) {
        this.printType1 = printType1;
        this.printData1 = printData1;
        this.printAlign1 = printAlign1;
        this.printFont1 = printFont1;
        this.printType2 = printType2;
        this.printData2 = printData2;
        this.printAlign2 = printAlign2;
        this.printFont2 = printFont2;
    }

    public PrintModel(String printData1, int printAlign1) {
        this.printData1 = printData1;
        this.printAlign1 = printAlign1;
    }

    public String getPrintType1() {
        return printType1;
    }

    public void setPrintType1(String printType1) {
        this.printType1 = printType1;
    }

    public String getPrintData1() {
        return printData1;
    }

    public void setPrintData1(String printData1) {
        this.printData1 = printData1;
    }

    public int getPrintAlign1() {
        return printAlign1;
    }

    public void setPrintAlign1(int printAlign1) {
        this.printAlign1 = printAlign1;
    }

    public int getPrintFont1() {
        return printFont1;
    }

    public void setPrintFont1(int printFont1) {
        this.printFont1 = printFont1;
    }

    public String getPrintType2() {
        return printType2;
    }

    public void setPrintType2(String printType2) {
        this.printType2 = printType2;
    }

    public String getPrintData2() {
        return printData2;
    }

    public void setPrintData2(String printData2) {
        this.printData2 = printData2;
    }

    public int getPrintAlign2() {
        return printAlign2;
    }

    public void setPrintAlign2(int printAlign2) {
        this.printAlign2 = printAlign2;
    }

    public int getPrintFont2() {
        return printFont2;
    }

    public void setPrintFont2(int printFont2) {
        this.printFont2 = printFont2;
    }
}
