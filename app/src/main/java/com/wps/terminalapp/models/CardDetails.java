package com.wps.terminalapp.models;

/**
 * Created by vignaraj.r on 04-Sep-18.
 */

public class CardDetails {
    String mCardType = "";
    String mCardImage = "";
    double mAmount = 0;

    public CardDetails(String mCardType, String mCardImage, double mAmount) {
        this.mCardType = mCardType;
        this.mCardImage = mCardImage;
        this.mAmount = mAmount;
    }

    public String getmCardType() {
        return mCardType;
    }

    public void setmCardType(String mCardType) {
        this.mCardType = mCardType;
    }

    public String getmCardImage() {
        return mCardImage;
    }

    public void setmCardImage(String mCardImage) {
        this.mCardImage = mCardImage;
    }

    public double getmAmount() {
        return mAmount;
    }

    public void setmAmount(double mAmount) {
        this.mAmount = mAmount;
    }
}
