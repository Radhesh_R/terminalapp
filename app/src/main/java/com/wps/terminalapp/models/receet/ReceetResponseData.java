package com.wps.terminalapp.models.receet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceetResponseData {
    @SerializedName("Transactions")
    @Expose
    private List<Transactions> transactions = null;
    @SerializedName("decSettleAmt")
    @Expose
    private Double decSettleAmt;
    @SerializedName("decCardAmt")
    @Expose
    private Double decCardAmt;
    @SerializedName("decCashAmt")
    @Expose
    private Double decCashAmt;
    @SerializedName("decTipAmt")
    @Expose
    private Double decTipAmt;
    @SerializedName("fetchUrl")
    @Expose
    private Object fetchUrl;
    @SerializedName("settle_yn")
    @Expose
    private Object settleYn;

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    public Double getDecSettleAmt() {
        return decSettleAmt;
    }

    public void setDecSettleAmt(Double decSettleAmt) {
        this.decSettleAmt = decSettleAmt;
    }

    public Double getDecCardAmt() {
        return decCardAmt;
    }

    public void setDecCardAmt(Double decCardAmt) {
        this.decCardAmt = decCardAmt;
    }

    public Double getDecCashAmt() {
        return decCashAmt;
    }

    public void setDecCashAmt(Double decCashAmt) {
        this.decCashAmt = decCashAmt;
    }

    public Double getDecTipAmt() {
        return decTipAmt;
    }

    public void setDecTipAmt(Double decTipAmt) {
        this.decTipAmt = decTipAmt;
    }

    public Object getFetchUrl() {
        return fetchUrl;
    }

    public void setFetchUrl(Object fetchUrl) {
        this.fetchUrl = fetchUrl;
    }

    public Object getSettleYn() {
        return settleYn;
    }

    public void setSettleYn(Object settleYn) {
        this.settleYn = settleYn;
    }
}
