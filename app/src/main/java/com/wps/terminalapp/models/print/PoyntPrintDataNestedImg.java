package com.wps.terminalapp.models.print;

import android.graphics.Bitmap;

import co.poynt.os.model.PrintedReceipt;

public class PoyntPrintDataNestedImg {

    PrintedReceipt original;
    PrintedReceipt head;
    Bitmap image;
    Bitmap logos;
    PrintedReceipt body;
    PrintedReceipt footer;

    public PrintedReceipt getOriginal() {
        return original;
    }

    public void setOriginal(PrintedReceipt original) {
        this.original = original;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Bitmap getLogos() {
        return logos;
    }

    public PrintedReceipt getBody() {
        return body;
    }

    public void setBody(PrintedReceipt body) {
        this.body = body;
    }

    public PrintedReceipt getFooter() {
        return footer;
    }

    public void setFooter(PrintedReceipt footer) {
        this.footer = footer;
    }

    public PrintedReceipt getHead() {
        return head;
    }

    public void setHead(PrintedReceipt head) {
        this.head = head;
    }

    public void setLogos(Bitmap logos) {
        this.logos = logos;
    }
}
