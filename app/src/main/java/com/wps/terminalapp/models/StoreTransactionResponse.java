package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoreTransactionResponse {

    @SerializedName("IsSuccess")
    @Expose
    private Boolean isSuccess;

    @SerializedName("ErrCode")
    @Expose
    private String errCode;

    @SerializedName("ErrMsg")
    @Expose
    private String errMsg;

    @SerializedName("ResponseData")
    @Expose
    private List<StoreTransactionRequest> responseData = null;

    @SerializedName("OtherData1")
    @Expose
    private String otherData1;

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<StoreTransactionRequest> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<StoreTransactionRequest> responseData) {
        this.responseData = responseData;
    }

    public String getOtherData1() {
        return otherData1;
    }

    public void setOtherData1(String otherData1) {
        this.otherData1 = otherData1;
    }
}
