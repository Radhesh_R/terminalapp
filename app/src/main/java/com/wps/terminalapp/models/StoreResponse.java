package com.wps.terminalapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreResponse {
    @SerializedName("OAL_SYS_ID")
    @Expose
    private Integer oALSYSID;

    @SerializedName("OAL_TYPE")
    @Expose
    private String oALTYPE;

    @SerializedName("OAL_PDEVICE_ID")
    @Expose
    private String oALPDEVICEID;

    @SerializedName("OAL_ST_DT")
    @Expose
    private String oALSTDT;

    @SerializedName("OAL_BATCH_NO")
    @Expose
    private String oALBATCHNO;

    @SerializedName("OAL_APPID")
    @Expose
    private String oALAPPID;

    @SerializedName("OAL_ATTACH")
    @Expose
    private String oALATTACH;

    @SerializedName("OAL_CHANNEL")
    @Expose
    private String oALCHANNEL;

    @SerializedName("OAL_MCH_ORDER_NO")
    @Expose
    private String oALMCHORDERNO;

    @SerializedName("OAL_FEE_TYPE")
    @Expose
    private String oALFEETYPE;

    @SerializedName("OAL_TOTAL_FEE")
    @Expose
    private Integer oALTOTALFEE;

    @SerializedName("OAL_AUTH_CODE")
    @Expose
    private String oALAUTHCODE;

    @SerializedName("OAL_OPERATOR_ID")
    @Expose
    private Integer oALOPERATORID;

    @SerializedName("OAL_TIME_START")
    @Expose
    private String oALTIMESTART;

    @SerializedName("OAL_REFUND_FEE")
    @Expose
    private String oALREFUNDFEE;

    @SerializedName("OAL_MCH_REFUND_NO")
    @Expose
    private String oALMCHREFUNDNO;

    @SerializedName("OAL_STS_YN")
    @Expose
    private String oALSTSYN;

    @SerializedName("OAL_RESULT")
    @Expose
    private String oALRESULT;

    @SerializedName("OAL_CASH_FEE")
    @Expose
    private String oALCASHFEE;

    @SerializedName("OAL_CASH_FEE_TYPE")
    @Expose
    private String oALCASHFEETYPE;

    @SerializedName("OAL_KSHER_ORDER_NO")
    @Expose
    private String oALKSHERORDERNO;

    @SerializedName("OAL_CHANNEL_ORDER_NO")
    @Expose
    private String oALCHANNELORDERNO;

    @SerializedName("OAL_RATE")
    @Expose
    private String oALRATE;

    @SerializedName("OAL_OPENID")
    @Expose
    private String oALOPENID;

    @SerializedName("OAL_TIME_END")
    @Expose
    private String oALTIMEEND;

    @SerializedName("OAL_OPERATION")
    @Expose
    private String oALOPERATION;

    @SerializedName("OAL_KSHER_REFUND_NO")
    @Expose
    private String oALKSHERREFUNDNO;

    @SerializedName("OAL_CHANNEL_REFUND_NO")
    @Expose
    private String oALCHANNELREFUNDNO;

    @SerializedName("OAL_CASH_REFUND_FEE")
    @Expose
    private String oALCASHREFUNDFEE;

    @SerializedName("OAL_REFUND_TIME")
    @Expose
    private String oALREFUNDTIME;

    @SerializedName("OAL_ERR_CODE")
    @Expose
    private String oALERRCODE;

    @SerializedName("OAL_ERR_MSG")
    @Expose
    private String oALERRMSG;

    @SerializedName("OAL_CR_DT")
    @Expose
    private String oALCRDT;

    @SerializedName("OAL_CR_UID")
    @Expose
    private String oALCRUID;

    @SerializedName("OAL_UPD_DT")
    @Expose
    private String oALUPDDT;

    @SerializedName("OAL_UPD_UID")
    @Expose
    private String oALUPDUID;

    @SerializedName("OAL_PRINT_YN")
    @Expose
    private String oALPRINTYN;

    @SerializedName("OAL_PRINT_DT")
    @Expose
    private String oALPRINTDT;

    public Integer getOALSYSID() {
        return oALSYSID;
    }

    public void setOALSYSID(Integer oALSYSID) {
        this.oALSYSID = oALSYSID;
    }

    public String getOALTYPE() {
        return oALTYPE;
    }

    public void setOALTYPE(String oALTYPE) {
        this.oALTYPE = oALTYPE;
    }

    public String getOALPDEVICEID() {
        return oALPDEVICEID;
    }

    public void setOALPDEVICEID(String oALPDEVICEID) {
        this.oALPDEVICEID = oALPDEVICEID;
    }

    public String getOALSTDT() {
        return oALSTDT;
    }

    public void setOALSTDT(String oALSTDT) {
        this.oALSTDT = oALSTDT;
    }

    public String getOALBATCHNO() {
        return oALBATCHNO;
    }

    public void setOALBATCHNO(String oALBATCHNO) {
        this.oALBATCHNO = oALBATCHNO;
    }

    public String getOALAPPID() {
        return oALAPPID;
    }

    public void setOALAPPID(String oALAPPID) {
        this.oALAPPID = oALAPPID;
    }

    public String getOALATTACH() {
        return oALATTACH;
    }

    public void setOALATTACH(String oALATTACH) {
        this.oALATTACH = oALATTACH;
    }

    public String getOALCHANNEL() {
        return oALCHANNEL;
    }

    public void setOALCHANNEL(String oALCHANNEL) {
        this.oALCHANNEL = oALCHANNEL;
    }

    public String getOALMCHORDERNO() {
        return oALMCHORDERNO;
    }

    public void setOALMCHORDERNO(String oALMCHORDERNO) {
        this.oALMCHORDERNO = oALMCHORDERNO;
    }

    public String getOALFEETYPE() {
        return oALFEETYPE;
    }

    public void setOALFEETYPE(String oALFEETYPE) {
        this.oALFEETYPE = oALFEETYPE;
    }

    public Integer getOALTOTALFEE() {
        return oALTOTALFEE;
    }

    public void setOALTOTALFEE(Integer oALTOTALFEE) {
        this.oALTOTALFEE = oALTOTALFEE;
    }

    public String getOALAUTHCODE() {
        return oALAUTHCODE;
    }

    public void setOALAUTHCODE(String oALAUTHCODE) {
        this.oALAUTHCODE = oALAUTHCODE;
    }

    public Integer getOALOPERATORID() {
        return oALOPERATORID;
    }

    public void setOALOPERATORID(Integer oALOPERATORID) {
        this.oALOPERATORID = oALOPERATORID;
    }

    public String getOALTIMESTART() {
        return oALTIMESTART;
    }

    public void setOALTIMESTART(String oALTIMESTART) {
        this.oALTIMESTART = oALTIMESTART;
    }

    public String getOALREFUNDFEE() {
        return oALREFUNDFEE;
    }

    public void setOALREFUNDFEE(String oALREFUNDFEE) {
        this.oALREFUNDFEE = oALREFUNDFEE;
    }

    public String getOALMCHREFUNDNO() {
        return oALMCHREFUNDNO;
    }

    public void setOALMCHREFUNDNO(String oALMCHREFUNDNO) {
        this.oALMCHREFUNDNO = oALMCHREFUNDNO;
    }

    public String getOALSTSYN() {
        return oALSTSYN;
    }

    public void setOALSTSYN(String oALSTSYN) {
        this.oALSTSYN = oALSTSYN;
    }

    public String getOALRESULT() {
        return oALRESULT;
    }

    public void setOALRESULT(String oALRESULT) {
        this.oALRESULT = oALRESULT;
    }

    public String getOALCASHFEE() {
        return oALCASHFEE;
    }

    public void setOALCASHFEE(String oALCASHFEE) {
        this.oALCASHFEE = oALCASHFEE;
    }

    public String getOALCASHFEETYPE() {
        return oALCASHFEETYPE;
    }

    public void setOALCASHFEETYPE(String oALCASHFEETYPE) {
        this.oALCASHFEETYPE = oALCASHFEETYPE;
    }

    public String getOALKSHERORDERNO() {
        return oALKSHERORDERNO;
    }

    public void setOALKSHERORDERNO(String oALKSHERORDERNO) {
        this.oALKSHERORDERNO = oALKSHERORDERNO;
    }

    public String getOALCHANNELORDERNO() {
        return oALCHANNELORDERNO;
    }

    public void setOALCHANNELORDERNO(String oALCHANNELORDERNO) {
        this.oALCHANNELORDERNO = oALCHANNELORDERNO;
    }

    public String getOALRATE() {
        return oALRATE;
    }

    public void setOALRATE(String oALRATE) {
        this.oALRATE = oALRATE;
    }

    public String getOALOPENID() {
        return oALOPENID;
    }

    public void setOALOPENID(String oALOPENID) {
        this.oALOPENID = oALOPENID;
    }

    public String getOALTIMEEND() {
        return oALTIMEEND;
    }

    public void setOALTIMEEND(String oALTIMEEND) {
        this.oALTIMEEND = oALTIMEEND;
    }

    public String getOALOPERATION() {
        return oALOPERATION;
    }

    public void setOALOPERATION(String oALOPERATION) {
        this.oALOPERATION = oALOPERATION;
    }

    public String getOALKSHERREFUNDNO() {
        return oALKSHERREFUNDNO;
    }

    public void setOALKSHERREFUNDNO(String oALKSHERREFUNDNO) {
        this.oALKSHERREFUNDNO = oALKSHERREFUNDNO;
    }

    public String getOALCHANNELREFUNDNO() {
        return oALCHANNELREFUNDNO;
    }

    public void setOALCHANNELREFUNDNO(String oALCHANNELREFUNDNO) {
        this.oALCHANNELREFUNDNO = oALCHANNELREFUNDNO;
    }

    public String getOALCASHREFUNDFEE() {
        return oALCASHREFUNDFEE;
    }

    public void setOALCASHREFUNDFEE(String oALCASHREFUNDFEE) {
        this.oALCASHREFUNDFEE = oALCASHREFUNDFEE;
    }

    public String getOALREFUNDTIME() {
        return oALREFUNDTIME;
    }

    public void setOALREFUNDTIME(String oALREFUNDTIME) {
        this.oALREFUNDTIME = oALREFUNDTIME;
    }

    public String getOALERRCODE() {
        return oALERRCODE;
    }

    public void setOALERRCODE(String oALERRCODE) {
        this.oALERRCODE = oALERRCODE;
    }

    public String getOALERRMSG() {
        return oALERRMSG;
    }

    public void setOALERRMSG(String oALERRMSG) {
        this.oALERRMSG = oALERRMSG;
    }

    public String getOALCRDT() {
        return oALCRDT;
    }

    public void setOALCRDT(String oALCRDT) {
        this.oALCRDT = oALCRDT;
    }

    public String getOALCRUID() {
        return oALCRUID;
    }

    public void setOALCRUID(String oALCRUID) {
        this.oALCRUID = oALCRUID;
    }

    public String getOALUPDDT() {
        return oALUPDDT;
    }

    public void setOALUPDDT(String oALUPDDT) {
        this.oALUPDDT = oALUPDDT;
    }

    public String getOALUPDUID() {
        return oALUPDUID;
    }

    public void setOALUPDUID(String oALUPDUID) {
        this.oALUPDUID = oALUPDUID;
    }

    public String getOALPRINTYN() {
        return oALPRINTYN;
    }

    public void setOALPRINTYN(String oALPRINTYN) {
        this.oALPRINTYN = oALPRINTYN;
    }

    public String getOALPRINTDT() {
        return oALPRINTDT;
    }

    public void setOALPRINTDT(String oALPRINTDT) {
        this.oALPRINTDT = oALPRINTDT;
    }
}
