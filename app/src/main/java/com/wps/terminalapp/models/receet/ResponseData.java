package com.wps.terminalapp.models.receet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseData {

    @SerializedName("WebSocketURL")
    @Expose
    private String webSocketURL;
    @SerializedName("FetchURL")
    @Expose
    private String fetchURL;
    @SerializedName("TokenKey")
    @Expose
    private String tokenKey;
    @SerializedName("QRDispTimeSec")
    @Expose
    private String qRDispTimeSec;
    @SerializedName("OdrTxnJSON")
    @Expose
    private OdrTxnJSON odrTxnJSON;
    @SerializedName("OtherData1")
    @Expose
    private String OtherData1;

    public String getWebSocketURL() {
        return webSocketURL;
    }

    public void setWebSocketURL(String webSocketURL) {
        this.webSocketURL = webSocketURL;
    }

    public String getFetchURL() {
        return fetchURL;
    }

    public void setFetchURL(String fetchURL) {
        this.fetchURL = fetchURL;
    }

    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    public String getQRDispTimeSec() {
        return qRDispTimeSec;
    }

    public void setQRDispTimeSec(String qRDispTimeSec) {
        this.qRDispTimeSec = qRDispTimeSec;
    }

    public OdrTxnJSON getOdrTxnJSON() {
        return odrTxnJSON;
    }

    public void setOdrTxnJSON(OdrTxnJSON odrTxnJSON) {
        this.odrTxnJSON = odrTxnJSON;
    }

    public String getOtherData1() {
        return OtherData1;
    }

    public void setOtherData1(String otherData1) {
        OtherData1 = otherData1;
    }
}
