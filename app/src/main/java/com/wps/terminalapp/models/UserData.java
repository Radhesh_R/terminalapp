package com.wps.terminalapp.models;

/**
 * Created by vignaraj.r on 4/24/2017.
 */

public class UserData {
    String mUserCode = "";
    String mName = "";
    String mRole = "";
    String mLogin_Pin = "";
    String mAuthenticate_Pin = "";

    public UserData(String mUserCode, String mName, String mRole, String mLogin_Pin, String mAuthenticate_Pin) {
        this.mUserCode = mUserCode;
        this.mName = mName;
        this.mRole = mRole;
        this.mLogin_Pin = mLogin_Pin;
        this.mAuthenticate_Pin = mAuthenticate_Pin;
    }

    public String getmUserCode() {
        return mUserCode;
    }

    public void setmUserCode(String mUserCode) {
        this.mUserCode = mUserCode;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmLogin_Pin() {
        return mLogin_Pin;
    }

    public void setmLogin_Pin(String mLogin_Pin) {
        this.mLogin_Pin = mLogin_Pin;
    }

    public String getmAuthenticate_Pin() {
        return mAuthenticate_Pin;
    }

    public void setmAuthenticate_Pin(String mAuthenticate_Pin) {
        this.mAuthenticate_Pin = mAuthenticate_Pin;
    }

    public String toString() {
        return "UserCode : " + this.getmUserCode() +
                " Name : " + this.getmName() +
                " Role : " + this.getmRole() +
                " Login_Pin : " + this.getmLogin_Pin() +
                " Authenticate_Pin : " + this.getmAuthenticate_Pin();
    }
}
