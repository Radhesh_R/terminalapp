package com.wps.terminalapp.listeners;

/**
 * Created by vignaraj.r on 25-Sep-18.
 */

public interface AlertListener {
    void done();
    void cancel();
}
