package com.wps.terminalapp.listeners;

/**
 * Created by vignaraj.r on 1/7/2018.
 */

public interface ResponseListener {
    void onResponse(String responseStr);
    void onFailure(String message);
}
