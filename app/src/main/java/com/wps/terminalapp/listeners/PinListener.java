package com.wps.terminalapp.listeners;

/**
 * Created by vignaraj.r on 03-Sep-18.
 */

public interface PinListener {
    void isValidUser(String pin, boolean valid);
}
