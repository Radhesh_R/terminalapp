package com.wps.terminalapp.listeners;

/**
 * Created by vignaraj.r on 27-Aug-18.
 */

public interface ActivityListener {
    void selectedDate(String date);
}
