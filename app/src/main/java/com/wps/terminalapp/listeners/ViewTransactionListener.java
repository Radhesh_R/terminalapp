package com.wps.terminalapp.listeners;

import com.wps.terminalapp.models.TransactionModel;

/**
 * Created by vignaraj.r on 29-Aug-18.
 */

public interface ViewTransactionListener {
    void selectedTransaction(TransactionModel model);
}
