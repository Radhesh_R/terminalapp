package com.wps.terminalapp.listeners;

/**
 * Created by vignaraj.r on 16-Sep-18.
 */

public interface PopSelectionListener {
    void selectedValue(String value);
}
