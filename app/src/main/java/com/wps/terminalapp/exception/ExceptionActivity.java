package com.wps.terminalapp.exception;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import com.wps.terminalapp.R;

import java.io.PrintWriter;
import java.io.StringWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExceptionActivity extends AppCompatActivity {

    @BindView(R.id.stack_trace) TextView stackTrace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exception);

        ButterKnife.bind(this);

        Throwable t = (Throwable) getIntent().getSerializableExtra("exception");

        StringWriter trace = new StringWriter();
        t.printStackTrace(new PrintWriter(trace));

        stackTrace.setText(trace.toString());
        t.printStackTrace();
    }

    @OnClick(R.id.finish)
    public void closeActivity(){
        finish();
    }
}
