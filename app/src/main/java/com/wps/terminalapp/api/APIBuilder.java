package com.wps.terminalapp.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wps.terminalapp.webrequest.Urls;

import java.io.File;
import java.security.KeyStore;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIBuilder {

    private Context context;

    private static Retrofit retrofitPrint;
    private static Retrofit retrofitApliPay;
    private static Retrofit retrofitTerminal;

    public APIBuilder(Context context) {
        this.context = context;
    }

    public  Retrofit retrofitApliPay(){
        if(retrofitApliPay == null){
            retrofitApliPay = new Retrofit.Builder()
                    .client(okHttpClient())
                    .baseUrl(Urls.ksherPayURL)
                    .addConverterFactory(gsonConverterFactory())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofitApliPay;
    }

    public  Retrofit retrofitTerminal(){
        if(retrofitTerminal == null){
            retrofitTerminal = new Retrofit.Builder()
                    .client(okHttpClient().newBuilder().sslSocketFactory( new TLSSocketFactory(), getX509TrustManager()).build())
                    .baseUrl(Urls.newUrl)
                    .addConverterFactory(gsonConverterFactory())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofitTerminal;
    }

    public Retrofit retrofitPrint(){
        if(retrofitPrint == null){
            retrofitPrint = new Retrofit.Builder()
                    .client(okHttpClient().newBuilder().sslSocketFactory( new TLSSocketFactory(), getX509TrustManager()).build())
                    .baseUrl(Urls.mainUrl)
                    .addConverterFactory(gsonConverterFactory())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofitPrint;
    }

    private X509TrustManager getX509TrustManager(){
        X509TrustManager x509TrustManager = null;
        try {
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
            trustManagerFactory.init((KeyStore)null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            x509TrustManager = (X509TrustManager) trustManagers[0];
        }catch (Exception e){
            e.printStackTrace();
        }

        return x509TrustManager;
    }

    private OkHttpClient okHttpClient(){
        return new OkHttpClient()
                .newBuilder()
                .cache(cache())
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor())
                .build();
    }

    private HttpLoggingInterceptor httpLoggingInterceptor(){
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    private Cache cache(){
        return new Cache(file(), 10 * 1000 * 1000); //10 MB
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private File file(){
        File file = new File(context.getCacheDir(), "HttpCache");
        file.mkdirs();
        return file;
    }

    private GsonConverterFactory gsonConverterFactory(){
        return GsonConverterFactory.create(gson());
    }

    private Gson gson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    public OkHttpClient okHttpClientHttps() {
        return new OkHttpClient()
                .newBuilder()
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor())
                .connectionSpecs(Collections.singletonList(new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2).build()))
                .sslSocketFactory(new TLSSocketFactory(), getX509TrustManager())
                .build();
    }

//    public  Retrofit retrofitReceet(){
//        return new Retrofit.Builder()
//                .client(okHttpClient())
//                .baseUrl(Urls.receetUrl)
//                .addConverterFactory(gsonConverterFactory())
//                .build();
//    }
}
