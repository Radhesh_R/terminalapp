package com.wps.terminalapp.api;

import com.wps.terminalapp.models.ErrorLogResponse;
import com.wps.terminalapp.models.FetchDeviceDetailsResponse;
import com.wps.terminalapp.models.OrderRefundResponse;
import com.wps.terminalapp.models.OrderRequestResponse;
import com.wps.terminalapp.models.OrderRevokeResponse;
import com.wps.terminalapp.models.StoreTransactionRequest;
import com.wps.terminalapp.models.StoreTransactionResponse;
import com.wps.terminalapp.models.print.PrintResponse;
import com.wps.terminalapp.models.receet.ReceetResponse;
import com.wps.terminalapp.models.LoGErr;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface APIComponent {

    @FormUrlEncoded
    @POST("quick_pay")
    Call<OrderRequestResponse> orderRequest(@Field("mch_order_no") String mch_order_no, @Field("total_fee") String total_fee,
                                            @Field("fee_type") String fee_type, @Field("auth_code") String auth_code,
                                            @Field("channel") String channel, @Field("operator_id") String operator_id,
                                            @Field("appid") String appid, @Field("nonce_str") String nonce_str,
                                            @Field("time_stamp") String time_stamp, @Field("sign") String sign);



    @FormUrlEncoded
    @POST("order_reverse")
    Call<OrderRevokeResponse> orderRevoke(@Field("appid") String appid, @Field("channel") String channel,
                                          @Field("mch_order_no") String mch_order_no, @Field("ksher_order_no") String ksher_order_no,
                                          @Field("channel_order_no") String channel_order_no, @Field("nonce_str") String nonce_str,
                                          @Field("version") String version, @Field("sign") String sign);


    @FormUrlEncoded
    @POST("order_refund")
    Call<OrderRefundResponse> orderRefund(@Field("appid") String appid, @Field("channel") String channel,
                                          @Field("mch_order_no") String mch_order_no, @Field("ksher_order_no") String ksher_order_no,
                                          @Field("channel_order_no") String channel_order_no, @Field("fee_type") String fee_type,
                                          @Field("total_fee") String total_fee, @Field("refund_fee") String refund_fee,
                                          @Field("mch_refund_no") String mch_refund_no, @Field("device_id") String device_id,
                                          @Field("operator_id") String operator_id, @Field("attach") String attach,
                                          @Field("nonce_str") String nonce_str, @Field("version") String version,
                                          @Field("sign") String sign);


    @FormUrlEncoded
    @POST("order_query")
    Observable<OrderRequestResponse> orderQueryV2(@Field("appid") String appid, @Field("channel") String channel,
                                                  @Field("mch_order_no") String mch_order_no, @Field("ksher_order_no") String ksher_order_no,
                                                  @Field("channel_order_no") String channel_order_no, @Field("nonce_str") String nonce_str,
                                                  @Field("version") String version, @Field("sign") String sign);

    @FormUrlEncoded
    @POST("order_query")
    Call<OrderRequestResponse> orderQuery(@Field("appid") String appid, @Field("channel") String channel,
                                                  @Field("mch_order_no") String mch_order_no, @Field("ksher_order_no") String ksher_order_no,
                                                  @Field("channel_order_no") String channel_order_no, @Field("nonce_str") String nonce_str,
                                                  @Field("version") String version, @Field("sign") String sign);


    @GET("FetchDeviceDetails")
    Call<FetchDeviceDetailsResponse> fetchDeviceDetails(@QueryMap Map<String, String> options);

    @POST("StoreTransaction")
    Call<StoreTransactionResponse> storeTransaction(@Body StoreTransactionRequest request);

    @POST("UpdateTransaction")
    Call<StoreTransactionResponse> updateTransaction(@Body StoreTransactionRequest request);

    @GET("GetAlipayPaymentDetails")
    Call<StoreTransactionResponse> getAlipayPaymentDetails(@QueryMap Map<String, String> options);

    @GET("GetPrintDetails")
    Call<ResponseBody> print(@QueryMap Map<String, String> options);

    @GET("GetTransactionPrint")
    Call<PrintResponse> printDetails(@QueryMap Map<String, String> options);

    @GET("ReceetNew/DeviceEligibilityReceet")
    Call<ReceetResponse> getReceetOrderDetail(@QueryMap Map<String, String> options);

    @GET("GetTxnById")
    Call<ReceetResponse> getTrxnById(@QueryMap Map<String, String> options);

    @POST("LogTxn?log=")
    Call<ErrorLogResponse> updateErrorLog(@Body LoGErr request);
}
