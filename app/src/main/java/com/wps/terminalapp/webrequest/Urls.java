package com.wps.terminalapp.webrequest;

/**
 * Created by vignaraj.r on 27-Aug-18.
 */

public class Urls {
//        public static String mainUrl = "https://waypointfzco.dyndns.org/WPPoyntAppApi/Poynt/";
//        public static String receetUrl = "http://waypointsystems.dyndns.org:25411/CheckoutAlipayAPI/";

    //Test
    public static String mainUrl = "https://checkoutpoynt.dyndns.org/PoyntTestApi/Poynt/";
    public static String newUrl = "https://checkoutpoynt.dyndns.org/PoyntTestApi/Alipay/";
    public static String IMAGE_BASE_URL = "https://checkoutpoynt.dyndns.org/PoyntTestApi";

    //Live
//    public static String mainUrl = "https://checkoutpoynt.dyndns.org/WPPoyntAppApi/Poynt/";
//    public static String newUrl = "https://checkoutpoynt.dyndns.org/WPPoyntAppApi/Alipay/";
//    public static String IMAGE_BASE_URL = "https://checkoutpoynt.dyndns.org/WPPoyntAppApi";


    public static String ksherPayURL = "https://api.mch.ksher.net/KsherPay/";

    public static String getTransactions = mainUrl + "GetTxnHistoryPaged?sDevId=";
    public static String userDataUrl = mainUrl + "GetUserInfo?sdevid=";
    public static String getBatchTTCInfo = mainUrl + "GetCurrentBatchAndTTC?sdevid=";
    public static String doSettleAmount = mainUrl + "SettleHostBatch?strDevSrNo=";
    public static String getTransactionPrint = mainUrl + "GetTransactionPrint?sDevId=";
    public static String getSummaryPrint = mainUrl + "GetBatchSummaryPrint?sDevId=";
    public static String getDetailedPrint = mainUrl + "GetBatchDetailsPrint?sDevId=";
    public static String getTxnByAuthCode = mainUrl + "GetTxnByAuthCode?sDevId=";
    public static String getTxnById = mainUrl + "GetTxnById?sDevId=";

  public static String fetchDeviceDetails = newUrl + "FetchDeviceDetails?DeviceID=";
  public static String storeTransaction = newUrl + "StoreTransaction";


  public static String quickPay = ksherPayURL + "quick_pay";
  public static String orderQuery = ksherPayURL + "order_query";
}
