package com.wps.terminalapp.webrequest;

import android.content.Context;
import android.os.Build;

import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.Util;

import java.util.Map;

/**
 * Created by vignaraj.r on 27-Aug-18.
 */

public class WebServices {

    public static WebServices instance;
    public static String TAG = WebServices.class.getSimpleName();

    public static WebServices getInstance() {
        if (instance == null) {
            instance = new WebServices();
        }
        return instance;
    }

    public void getTransactions(Context context, final ResponseListener responseListener, String... params) {
        Util.dTag(0, TAG, "getTransactions");
        Util.showProgressDialog(context, "Loading Transactions");
        String completeUrl = Urls.getTransactions + params[0];
        Util.logLargeString("Trans_Url", completeUrl);
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void loadMoreTransactions(Context context, final ResponseListener responseListener, String... params) {
        Util.dTag(0, TAG, "loadMoreTransactions");
        Util.showProgressDialog(context, "Loading Transactions");
        String completeUrl = Urls.mainUrl + params[0];
        Util.logLargeString("Trans_Url", completeUrl);
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void getUsersData(Context context, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getUsersData");
        Util.showProgressDialog(context, "Loading Settings");
        String completeUrl = Urls.userDataUrl + GeneralUtil.getDeviceSerial();
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void getCurrentBatchInfo(Context context, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getCurrentBatchInfo");
        Util.showProgressDialog(context, "Loading Information");
        String completeUrl = Urls.getBatchTTCInfo + GeneralUtil.getDeviceSerial();
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void doSettle(Context context, String batchNo, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "doSettle");
        Util.showProgressDialog(context, "Settling Batch");
        String completeUrl = Urls.doSettleAmount + GeneralUtil.getDeviceSerial() + "&strBatchNo=" + batchNo + "&strUser=AUTO";
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void getPrint(Context context, String transactionId, String mode, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getPrint");
        Util.showProgressDialog(context, "Please wait");
        String completeUrl = Urls.getTransactionPrint + GeneralUtil.getDeviceSerial() + "&strTxnid=" + transactionId + "&strMode=" + mode;
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    /**
     * printType 1 -  Summary Report 2 - Detailed Report
     **/
    public void getBatchPrint(Context context, int printType, String batchNo, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getBatchPrint");
        Util.showProgressDialog(context, "Please wait");
        String completeUrl = "";
        if (printType == 1) {
            completeUrl = Urls.getSummaryPrint + GeneralUtil.getDeviceSerial() + "&strBatchNo=" + batchNo;
        } else {
            completeUrl = Urls.getDetailedPrint + GeneralUtil.getDeviceSerial() + "&strBatchNo=" + batchNo;
        }
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void getTxnByAuthCode(Context context, String authCode, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getTxnByAuthCode");
        Util.showProgressDialog(context, "Finding Transactions");
        String completeUrl = Urls.getTxnByAuthCode + GeneralUtil.getDeviceSerial() + "&strAuthCode=" + authCode;
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void getTxnById(Context context, String id, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "getTxnById");
        Util.showProgressDialog(context, "Fetching Transaction");
        String completeUrl = Urls.getTxnById + GeneralUtil.getDeviceSerial() + "&strTxnId=" + id;
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void fetchDeviceDetails(Context context, long amount, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "fetchDeviceDetails");
        Util.showProgressDialog(context, "Fetching Device Details");
        String completeUrl = Urls.fetchDeviceDetails + GeneralUtil.getDeviceSerial() + "&Amount=" + amount;
        WebRequest.getInstance().getRequest(completeUrl, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void updateDetails(Context context, String data, final ResponseListener responseListener) {
        Util.dTag(0, TAG, "fetchDeviceDetails");
        Util.showProgressDialog(context, "Fetching Device Details");
        String completeUrl = Urls.storeTransaction;
        WebRequest.getInstance().postRequest(completeUrl, data, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void quickPay(Context context, Map<String, String> parms, final ResponseListener responseListener){
        Util.dTag(0, TAG, "quickPay");
        Util.showProgressDialog(context, "QuickPay Transaction");
        String completeUrl = Urls.quickPay;
        WebRequest.getInstance().postRequestWParams(completeUrl, parms, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

    public void orderQuery(Context context, Map<String, String> parms, final ResponseListener responseListener){
        Util.dTag(0, TAG, "orderQuery");
        Util.showProgressDialog(context, "Processing Transaction");
        String completeUrl = Urls.orderQuery;
        WebRequest.getInstance().postRequestWParams(completeUrl, parms, new ResponseListener() {
            @Override
            public void onResponse(String responseStr) {
                Util.closeProgressDialog();
                responseListener.onResponse(responseStr);
            }

            @Override
            public void onFailure(String message) {
                Util.closeProgressDialog();
                responseListener.onFailure(message);
            }
        });
    }

}
