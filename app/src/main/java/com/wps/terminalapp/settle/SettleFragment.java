package com.wps.terminalapp.settle;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.appolica.flubber.Flubber;
import com.wps.terminalapp.BuildConfig;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.adapters.CardDetailsAdapter;
import com.wps.terminalapp.adapters.TransactionAdapter;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansMedium;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.listeners.ErrorListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.models.CardDetails;
import com.wps.terminalapp.models.PrintModel;
import com.wps.terminalapp.utils.PrintUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.poynt.os.Constants;
import co.poynt.os.model.AccessoryProvider;
import co.poynt.os.model.AccessoryProviderFilter;
import co.poynt.os.model.AccessoryType;
import co.poynt.os.model.PoyntError;
import co.poynt.os.model.PrintedReceipt;
import co.poynt.os.model.PrintedReceiptV2;
import co.poynt.os.model.PrinterStatus;
import co.poynt.os.printing.ReceiptPrintingPref;
import co.poynt.os.services.v1.IPoyntAccessoryManagerListener;
import co.poynt.os.services.v1.IPoyntPrinterService;
import co.poynt.os.services.v1.IPoyntPrinterServiceListener;
import co.poynt.os.util.AccessoryProviderServiceHelper;

import static com.wps.terminalapp.utils.Util.dTag;
import static com.wps.terminalapp.utils.Util.settleAmount;

/**
 * Created by vignaraj.r on 08-Aug-18.
 */

public class SettleFragment extends Fragment {

    @BindView(R.id.img_settle)
    ImageView imgSettle;
    @BindView(R.id.txt_settletxt)
    TextSansMedium txtSettletxt;
    @BindView(R.id.txt_settleamt)
    TextSansMedium txtSettleamt;
    @BindView(R.id.txt_settledate)
    TextSansRegular txtSettledate;
    @BindView(R.id.txt_settleid)
    TextSansRegular txtSettleid;
    @BindView(R.id.layout_one)
    LinearLayout layoutOne;
    @BindView(R.id.txt_saleamount)
    TextSansBold txtSaleamount;
    @BindView(R.id.txt_voidamount)
    TextSansBold txtVoidamount;
    @BindView(R.id.txt_netamount)
    TextSansBold txtNetamount;
    @BindView(R.id.txt_tipamount)
    TextSansBold txtTipamount;
    @BindView(R.id.layout_two)
    LinearLayout layoutTwo;
    @BindView(R.id.layout_three)
    LinearLayout layoutThree;
    @BindView(R.id.list_cardvalues)
    RecyclerView listCardvalues;
    @BindView(R.id.btn_detailreport)
    ButtonSansRegular btnDetailreport;
    @BindView(R.id.btn_summaryreport)
    ButtonSansRegular btnSummaryreport;
    @BindView(R.id.layout_mainsettle)
    LinearLayout layoutMainsettle;
    @BindView(R.id.txt_viewalltrans)
    TextSansRegular txt_viewallTrans;

    LinearLayoutManager layoutManager;
    CardDetailsAdapter adapter;

    public interface SettleListener {
        void goBackTransaction();
    }

    SettleListener listener;
    View settleView;
    public static String TAG = SettleFragment.class.getSimpleName();
    Context context;
    String currentBatch = "";

    //
    private AccessoryProviderServiceHelper accessoryProviderServiceHelper;
    private HashMap<AccessoryProvider, IBinder> mPrinterServices = new HashMap<>();
    private List<AccessoryProvider> providers;
    private Set<String> printers;

    // this is the accessory manager listener which gets invoked when accessory manager completes
    // scanning for the requested accessories
    private IPoyntAccessoryManagerListener poyntAccessoryManagerListener
            = new IPoyntAccessoryManagerListener.Stub() {

        @Override
        public void onError(PoyntError poyntError) throws RemoteException {
            Log.e(TAG, "Failed to connect to accessory manager: " + poyntError);
        }

        @Override
        public void onSuccess(final List<AccessoryProvider> printers) throws RemoteException {
            // now that we are connected - request service connections to each accessory provider
            if (printers != null && printers.size() > 0) {
                // save it for future reference
                providers = printers;
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    // for each printer accessory - request "service" connections if it's still connected
                    for (AccessoryProvider printer : printers) {
                        Log.d(TAG, "Printer: " + printer.toString());
                        if (printer.isConnected()) {
                            // request service connection binder
                            // IMP: note that this method returns service connection if it already exists
                            // hence the need for both connection callback and the returned value
                            IBinder binder = accessoryProviderServiceHelper.getAccessoryService(
                                    printer, AccessoryType.PRINTER,
                                    providerConnectionCallback);
                            //already cached connection.
                            if (binder != null) {
                                mPrinterServices.put(printer, binder);
                            }
                        }
                    }
                }
            } else {
                Log.e(TAG, "No Printers found");
            }
        }
    };

    // this is the callback for the service connection to each accessory provider service in this case
    // the android service supporting  printer accessory
    private AccessoryProviderServiceHelper.ProviderConnectionCallback providerConnectionCallback
            = new AccessoryProviderServiceHelper.ProviderConnectionCallback() {

        @Override
        public void onConnected(AccessoryProvider provider, IBinder binder) {
            // in some cases multiple accessories of the same type (eg. two printers of same
            // make/model or two star printers) might be supported by the same android service
            // so here we check if we need to share the same service connection for more than
            // one accessory provider
            List<AccessoryProvider> otherProviders = findMatchingProviders(provider);
            // all of them share the same service binder
            for (AccessoryProvider matchedProvider : otherProviders) {
                mPrinterServices.put(matchedProvider, binder);
            }
        }

        @Override
        public void onDisconnected(AccessoryProvider provider, IBinder binder) {
            // set the lookup done to false so we can try looking up again if needed
            if (mPrinterServices != null && mPrinterServices.size() > 0) {
                mPrinterServices.remove(binder);
                // try to renew the connection.
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    IBinder binder2 = accessoryProviderServiceHelper.getAccessoryService(
                            provider, AccessoryType.PRINTER,
                            providerConnectionCallback);
                    if (binder2 != null) {//already cached connection.
                        mPrinterServices.put(provider, binder2);
                    }
                }
            }
        }
    };

    // we do this if there are multiple accessories connected of the same type/provider
    private List<AccessoryProvider> findMatchingProviders(AccessoryProvider provider) {
        ArrayList<AccessoryProvider> matchedProviders = new ArrayList<>();
        if (providers != null) {
            for (AccessoryProvider printer : providers) {
                if (provider.getAccessoryType() == printer.getAccessoryType()
                        && provider.getPackageName().equals(printer.getPackageName())
                        && provider.getClassName().equals(printer.getClassName())) {
                    matchedProviders.add(printer);
                }
            }
        }
        return matchedProviders;
    }
    //

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        settleView = inflater.inflate(R.layout.fragment_settle, container, false);
        ButterKnife.bind(this, settleView);
        context = getActivity();
        initializeAccessories();
        showEmptyScreen();
        settleView.setVisibility(View.GONE);


/*
        List<CardDetails> dtl = new ArrayList<>();
        dtl.add(new CardDetails("VISA", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 1.00));
//        dtl.add(new CardDetails("MASTERCARD", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 2.00));
//        dtl.add(new CardDetails("JCB", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 3.00));
*/
/*        dtl.add(new CardDetails("AMEX", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 4.00));
        dtl.add(new CardDetails("OTHER", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png",5.00));
        dtl.add(new CardDetails("APPLE", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 6.00));
        dtl.add(new CardDetails("SAMSUNG", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 7.00));
//
        dtl.add(new CardDetails("VISA", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 8.00));
        dtl.add(new CardDetails("MASTERCARD", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 9.00));
        dtl.add(new CardDetails("JCB", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 10.00));
        dtl.add(new CardDetails("AMEX", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 11.00));
        dtl.add(new CardDetails("OTHER", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 12.00));
        dtl.add(new CardDetails("APPLE", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 13.00));
        dtl.add(new CardDetails("SAMSUNG", "http://waypointfzco.dyndns.org:3652/DOCUMENTS/CARDTYPES/OTHERS.png", 14.00));*//*


        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        listCardvalues.setLayoutManager(layoutManager);

        listCardvalues.addOnScrollListener(scrollListener);
        adapter = new CardDetailsAdapter(getActivity(), dtl);
        listCardvalues.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if (dtl.size() > 6) {
            listCardvalues.scrollToPosition(0);
        }
*/


//        listCardvalues.scrollToPosition(layoutManager.findLastVisibleItemPosition()-1);

        retrieveBatch();
        return settleView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MainActivity) context;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(context, e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id.txt_viewalltrans)
    public void viewAllTransaction() {
        dTag(1, TAG, "viewAllTransaction");
        showCurrentTransactions();
    }

    @OnClick(R.id.btn_detailreport)
    public void printDetailReport() {
        dTag(1, TAG, "printDetailReport");
        firePrint(2);
    }

    @OnClick(R.id.btn_summaryreport)
    public void printSummaryReport() {
        dTag(1, TAG, "printSummaryReport");
        firePrint(1);
    }

    public RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int lastVisiblePOs = layoutManager.findLastVisibleItemPosition();
            int firstVisiblePos = layoutManager.findFirstVisibleItemPosition();
            Log.i("lastVisPos", String.valueOf(lastVisiblePOs));
            Log.i("firstVisPos", String.valueOf(firstVisiblePos));
//            Log.i("childCount", String.valueOf(Util.transactionList.size() - 1));
        }
    };

    public void retrieveBatch() {
        dTag(0, TAG, "retrieveBatch");
        WebServices.getInstance().getCurrentBatchInfo(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("response : ", "retrieveBatch : " + responseStr);
                            JSONObject response = new JSONObject(responseStr);
                            boolean isSuccess = response.optBoolean("IsSuccess", false);
                            if (isSuccess) {
                                JSONObject responseData = response.getJSONObject("ResponseData");
                                String TTC = responseData.optString("TTC");
                                String BATCH = responseData.optString("BATCH");
                                String MID = responseData.optString("MID");
                                String TID = responseData.optString("TID");
                                makeSettlement(BATCH);
                                currentBatch = BATCH;

                            } else {
                                String errMsg = response.getString("ErrMsg");
                                Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + errMsg, errlistener);
                            }
                        } catch (JSONException e) {
                            Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + e.getMessage(), errlistener);
                        }
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + message, errlistener);
//                        Util.showErrorToast(getActivity(), message);
                    }
                });
            }
        });
    }

    public void makeSettlement(String batchNo) {
        dTag(0, TAG, "makeSettlement");
        Log.d("batchNo : ", batchNo);
        WebServices.getInstance().doSettle(getActivity(), batchNo, new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            Util.logLargeString("response : ", "makeSettlement : " + responseStr);
                            JSONObject response = new JSONObject(responseStr);
                            boolean isSuccess = response.optBoolean("IsSuccess", false);
                            if (isSuccess) {
                                JSONObject responseData = response.optJSONObject("ResponseData");
                                JSONArray cardAmts = responseData.optJSONArray("card_amt");
                                double total_sale = responseData.optDouble("total_sale");
                                double total_void = responseData.optDouble("total_void");
                                double total_netsale = responseData.optDouble("total_netsale");
                                double total_tips = responseData.optDouble("total_tips");
                                String settleId = responseData.optString("settle_id");
                                long settleTime = responseData.optLong("settle_time");
                                String batchId = responseData.optString("new_batch");

                                if (!batchId.equals("")) {
                                    if (BuildConfig.BUILD_TYPE.equals("debug") && Util.isProviderExist(getActivity())) {
                                        Util.showErrorToast(getActivity(), "Please remove the live transaction processor");
                                    } else {
                                        Util.deleteBatchValue(getActivity());
                                        Util.addBatchID(getActivity(), batchId);
                                    }
                                }
                                double val1 = total_sale / 100;
                                double val2 = total_void / 100;
                                double val3 = total_netsale / 100;
                                double val4 = total_tips / 100;

                                List<CardDetails> list = new ArrayList<>();
                                if (cardAmts != null && cardAmts.length() > 0) {
                                    for (int i = 0; i < cardAmts.length(); i++) {
                                        JSONObject obj = cardAmts.getJSONObject(i);
                                        String cardImg = obj.optString("card_img");
                                        String cardType = obj.optString("card_type");
                                        double amount = obj.optDouble("amount");
                                        double final_amt = amount / 100;
                                        list.add(new CardDetails(cardType, cardImg, final_amt));
                                    }
                                }
                                showUI(settleId, settleTime, list, val1, val2, val3, val4);
                                Util.settleAmount = "0.00";
                                Util.currentSettleYN = "";
                                Util.transactionList.clear();
                                Util.totalTipAmount = "0.00";
                                Util.totalCashAmount = "0.00";
                                Util.totalCardAmount = "0.00";
                            } else {
                                String errMsg = response.getString("ErrMsg");
                                Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + errMsg, errlistener);
                            }
                        } catch (JSONException e) {
                            Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + e.getMessage(), errlistener);
                        }
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showErrorDialog(getContext(), "Unable to retrieve the batch.\n" + message, errlistener);
                    }
                });
            }
        });
    }

    public ErrorListener errlistener = new ErrorListener() {
        @Override
        public void doAction() {
            listener.goBackTransaction();
        }
    };

    public void showUI(String settleId, long settleTime, List<CardDetails> details, double... values) {
        dTag(0, TAG, "showUI");
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        listCardvalues.setLayoutManager(layoutManager);
        txtSaleamount.setText(Util.formatAmount(values[0]));
        txtVoidamount.setText(Util.formatAmount(values[1]));
        txtNetamount.setText(Util.formatAmount(values[2]));
        txtTipamount.setText(Util.formatAmount(values[3]));

        settleId = "#" + settleId.substring(0, 8).toUpperCase();
        txtSettleid.setText(settleId);
        String settleDate = Util.getDateString(settleTime, "dd-MMMM-yyyy hh:mm a");
        txtSettledate.setText(settleDate);
        if (!settleAmount.equals("")) {
            settleAmount = settleAmount.replace(",", "");
            int beforeDecimal = Integer.parseInt(settleAmount.substring(0, settleAmount.indexOf(".")));
            int afterDecimal = Integer.parseInt(settleAmount.substring(settleAmount.indexOf(".") + 1, settleAmount.length()));
            Util.animateText(txtSettleamt, beforeDecimal, afterDecimal);
        }

        if (details != null && details.size() > 0) {
            Collections.sort(details, new Comparator<CardDetails>() {
                @Override
                public int compare(CardDetails o1, CardDetails o2) {
                    return Double.compare(o2.getmAmount(), o1.getmAmount());
                }
            });
            listCardvalues.addOnScrollListener(scrollListener);
            adapter = new CardDetailsAdapter(getActivity(), details);
            listCardvalues.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (details.size() > 6) {
                listCardvalues.scrollToPosition(0);
            }
        }
        settleView.setVisibility(View.VISIBLE);
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                .repeatCount(0)
                .duration(2000)
                .createFor(settleView)
                .start();
//        retrieveBatch(0);
    }


    public void showCurrentTransactions() {
        dTag(0, TAG, "showCurrentTransactions");
        final PopupWindow tPop = new PopupWindow(context);
        tPop.setFocusable(true);
        tPop.setWidth(550);
        tPop.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        LayoutInflater inflater = (LayoutInflater) settleView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = inflater.inflate(R.layout.pop_transactions, null);
        tPop.setContentView(v);
        tPop.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RecyclerView list_trans = (RecyclerView) v.findViewById(R.id.trans_poplist);
        ButtonSansRegular btn_close = (ButtonSansRegular) v.findViewById(R.id.btn_closepop);
        if (Util.transactionList.size() > 0) {
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            list_trans.setLayoutManager(manager);
            TransactionAdapter adapter = new TransactionAdapter(getActivity(), Util.transactionList);
            list_trans.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tPop.dismiss();
            }
        });
        tPop.showAsDropDown(txt_viewallTrans, 0, 0);
    }

    public void firePrint(final int type) {
        dTag(0, TAG, "firePrint");
        WebServices.getInstance().getBatchPrint(getActivity(), type, currentBatch, new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
//                Util.logLargeString(TAG, "print_response#" + responseStr);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject object = new JSONObject(responseStr);
                            JSONArray arr = object.getJSONArray("ResponseData");
                            receiptData(arr.toString());
                        } catch (JSONException e) {
//                            Util.logLargeString("json_exception_" + type, e.getMessage());
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, message);
                    }
                });
            }
        });
    }

    public void receiptData(String response) {
        dTag(0, TAG, "receiptData");
        try {
            JSONArray printArray = new JSONArray(response);
            for (int i = 0; i < printArray.length(); i++) {
                JSONObject pObj = printArray.getJSONObject(i);
                final String printerIP = pObj.getString("PRINTER_IP");
                final String printerPort = pObj.getString("PRINTER_PORT");
                final String printerName = pObj.getString("PRINTER_NAME");
                final String orderId = pObj.getString("PRINT_ORDER_ID");
                final String jobId = pObj.getString("PRINT_JOB_ID");
                final String printType = pObj.getString("PRINTER_TYPE");
                final JSONArray printHeader = pObj.getJSONArray("PRINT_HEADER");
                final JSONArray printBody = pObj.getJSONArray("PRINT_BODY");
                final JSONArray printFooter = pObj.getJSONArray("PRINT_FOOTER");

                List<PrintModel> header = new ArrayList<>();
                List<PrintModel> body = new ArrayList<>();
                List<PrintModel> footer = new ArrayList<>();
                Util.addIntoList(header, printHeader);
                Util.addIntoList(body, printBody);
                Util.addIntoList(footer, printFooter);
                if (printerIP.equals("")) {
                    printers = findReceiptPrinter(getActivity());
                    PrintedReceipt receipt = PrintUtil.getInstance(getActivity()).generateReceipt(printHeader, printBody, printFooter, null);
                    printReceipt(receipt, printerName);
                } else {
                    //noinspection unchecked
                    PrintUtil.getInstance(context).PrintNew(printerIP, Integer.valueOf(printerPort), header, body, footer);
                }
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(getActivity(), e.getMessage());
        }
    }

    /**
     * Initialize the print accessories
     **/
    public void initializeAccessories() {
        dTag(0, TAG, "initializeAccessories");
        try {
            // initialize capabilityProviderServiceHelper
            accessoryProviderServiceHelper = new AccessoryProviderServiceHelper(getActivity());
            // connect to accessory manager service
            accessoryProviderServiceHelper.bindAccessoryManager(
                    new AccessoryProviderServiceHelper.AccessoryManagerConnectionCallback() {
                        @Override
                        public void onConnected(AccessoryProviderServiceHelper helper) {
                            // when connected check if we have any printers registered
                            if (helper.getAccessoryServiceManager() != null) {
                                AccessoryProviderFilter filter = new AccessoryProviderFilter(AccessoryType.PRINTER);
                                Log.d(TAG, "trying to get PRINTER accessory...");
                                try {
                                    // look up the printers using the filter
                                    helper.getAccessoryServiceManager().getAccessoryProviders(
                                            filter, poyntAccessoryManagerListener);
                                } catch (RemoteException e) {
                                    Log.e(TAG, "Unable to connect to Accessory Service", e);
                                }
                            } else {
                                Log.e(TAG, "Not connected with accessory service manager");
                            }
                        }

                        @Override
                        public void onDisconnected(AccessoryProviderServiceHelper accessoryProviderServiceHelper) {
                            Log.e(TAG, "Disconnected with accessory service manager");
                        }
                    });
        } catch (SecurityException e) {
            Log.e(TAG, "Failed to connect to capability or accessory manager", e);
        }
    }

    public void printReceipt(PrintedReceipt receipt, String printerName) {
        dTag(0, TAG, "printReceipt");
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrint(binder, receipt);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrint(binder, receipt);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }


    public void printReceiptV2(PrintedReceiptV2 receipt, String printerName) {
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrintV2(binder, receipt, printerName);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrintV2(binder, receipt, printerName);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }

    public void doPrint(IBinder binder, PrintedReceipt receipt) {
        dTag(0, TAG, "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                });
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    public void doPrintV2(IBinder binder, PrintedReceiptV2 receipt, String printerName) {
        dTag(0, TAG, "doPrintV2");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

/*                printerService.printReceipt(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                },null);*/

                printerService.printReceiptForPrinter(printerName, UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }
                }, null);

              /*  printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                });*/
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    /**
     * Find the order receipt printers
     */
    private Set<String> findReceiptPrinter(Context context) {
        dTag(0, TAG, "findReceiptPrinter");
        Set<String> set = ReceiptPrintingPref.readReceiptPrefsFromDb(context, Constants.ReceiptPreference.PREF_PAYMENT_RECEIPT);
        if (set != null && set.size() > 0) {
            for (String s : set) {
                Log.d("_printer_#", s);
            }
        } else {
            Log.d(TAG, "No receipt printers found");
        }
        return set;
    }

    public void showEmptyScreen() {
        dTag(0, TAG, "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        Log.e(TAG, "Unable to show second screen!");
                    }
                }
            }
        });
    }

}