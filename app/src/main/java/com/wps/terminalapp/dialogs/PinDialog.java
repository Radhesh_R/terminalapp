package com.wps.terminalapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.KeyEvent;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wps.terminalapp.R;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.PinView;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.listeners.PinListener;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.utils.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vignaraj.r on 03-Sep-18.
 */

public class PinDialog extends Dialog {
    int flag = 0;
    List<UserData> userData;

    PinListener listener;

    @BindView(R.id.layout_pindialog)
    RelativeLayout layoutPinDialog;
    @BindView(R.id.login_text)
    TextSansBold loginText;
    @BindView(R.id.edit_pin)
    PinView editPin;
    @BindView(R.id.btn_pindone)
    ButtonSansRegular btnPindone;
    @BindView(R.id.btn_pincancel)
    ButtonSansRegular btnPinCancel;

    public PinDialog(@NonNull Context context) {
        super(context);
    }

    public PinDialog(@NonNull Context context, int flag, List<UserData> userData, PinListener listener) {
        super(context);
        this.flag = flag;
        this.userData = userData;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        setContentView(R.layout.dialog_pin);
        ButterKnife.bind(this);
//        Util.logLargeString(TAG, "users "+new Gson().toJson(userData));

        editPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onDone();
                    return true;
                }
                return false;
            }
        });
    }


    @OnClick(R.id.btn_pindone)
    public void onDone() {
        String pin = editPin.getText().toString();
        if (pin.equals("")) {
            Util.showErrorToast(getContext(), "Enter the valid PIN");
            return;
        }
        if (pin.length() < 3) {
            Util.showErrorToast(getContext(), "Enter the valid PIN");
            return;
        }

        if (checkValidUser()) {
            Util.hideKeyBoard(getContext(), editPin);
            listener.isValidUser(pin, checkValidUser());
            dismiss();
        } else {
            Util.showErrorToast(getContext(), "Invalid PIN!");
        }
    }


    @OnClick(R.id.btn_pincancel)
    public void onCancel() {
        Util.hideKeyBoard(getContext(), editPin);
        dismiss();
    }


    private boolean checkValidUser() {
        boolean valid = false;
        if (flag == 0) {

        } else if (flag == 1) {
            if (userData != null && userData.size() > 0) {
                for (int i = 0; i < userData.size(); i++) {
                    if (editPin.getText().toString().equals(userData.get(i).getmAuthenticate_Pin())) {
//                        Util.logLargeString("userdata", new Gson().toJson(userData.get(i)));
                        valid = true;
                    }
                }
            } else {
                Util.showErrorToast(getContext(), "Please sync users first!");
            }
        }
        return valid;
    }


}
