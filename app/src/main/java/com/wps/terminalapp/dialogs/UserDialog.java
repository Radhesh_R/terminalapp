package com.wps.terminalapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Window;
import android.widget.ListView;

import com.wps.terminalapp.R;
import com.wps.terminalapp.adapters.UserAdapter;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.models.UserData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vignaraj.r on 19-Sep-18.
 */

public class UserDialog extends Dialog {
    List<UserData> data;
    @BindView(R.id.txt_head)
    TextSansBold txtHead;
    @BindView(R.id.list_user)
    ListView listUser;
    @BindView(R.id.btn_usercancel)
    ButtonSansRegular btnUsercancel;

    public UserDialog(@NonNull Context context) {
        super(context);
    }

    public UserDialog(@NonNull Context context, List<UserData> data) {
        super(context);
        this.data = data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_user);
        ButterKnife.bind(this);
        listUser.setAdapter(new UserAdapter(getContext(), data));
    }

    @OnClick(R.id.btn_usercancel)
    public void closeDialog(){
        dismiss();
    }

}
