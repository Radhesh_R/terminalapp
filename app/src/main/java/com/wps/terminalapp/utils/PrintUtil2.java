package com.wps.terminalapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.wps.terminalapp.api.APIBuilder;
import com.wps.terminalapp.models.print.PRINTBODY;
import com.wps.terminalapp.models.print.PRINTFOOTER;
import com.wps.terminalapp.models.print.PRINTHEADER;
import com.wps.terminalapp.models.print.PoyntPrintData;
import com.wps.terminalapp.models.print.PoyntPrintDataNestedImg;
import com.wps.terminalapp.models.print.PrintData;
import com.wps.terminalapp.webrequest.Urls;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import co.poynt.os.model.PrintedReceipt;
import co.poynt.os.model.PrintedReceiptLine;


public class PrintUtil2 {

    private static final int PRINTER_TIMEOUT = 3000;

    private static final int MAX_LINE_LENGTH = 26;

    private static final int CENTER = 0;
    private static final int LEFT = 1;
    private static final int RIGHT = 2;

    private Context context;

    public PrintUtil2(Context context) {
        this.context = context;
    }

    /**
     * create PrintedReceiptLine object
     *
     * @param s input string
     * @return PrintedReceiptLine object corresponding to input string
     */
    private PrintedReceiptLine newLine(String s) {
        PrintedReceiptLine line = new PrintedReceiptLine();
        line.setText(s);
        return line;
    }

    /**
     * @param s     input string
     * @param align where should text align in receipt (LEFT or RIGHT)
     * @return List<PrintedReceiptLine> object corresponding to input string
     */
    private List<PrintedReceiptLine> newLine(String s, int align) {
        List<PrintedReceiptLine> printList = new ArrayList<>();
        List<String> splitList = splitParagraph(s, align);

        for (String str : splitList) {
            printList.add(newLine(str));
        }

        return printList;
    }


    private PrintedReceiptLine newLinePoynt(String s, int align) {
        try {
            int length = MAX_LINE_LENGTH - s.length();
            String whiteSpaces = generateSpaces(length);

            if (align == RIGHT) {
                s = whiteSpaces + s;
            } else if (align == LEFT) {
                s = s + whiteSpaces;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newLine(s);
    }


    /**
     * This split the paragraph into lines with length < MAX_LINE_LENGTH
     *
     * @param input paragraph text
     * @param align indicate where should it align
     * @return list of string with string.length() not exceeding MAX_LINE_LENGTH
     */
    private List<String> splitParagraph(String input, int align) {

        StringTokenizer tok = new StringTokenizer(input, " ");
        StringBuilder output = new StringBuilder(input.length());
        int lineLen = 0;


        while (tok.hasMoreTokens()) {
            String word = tok.nextToken() + " ";

            if (lineLen + word.trim().length() > MAX_LINE_LENGTH) {
                output.append("\n");
                lineLen = 0;
            }

            String lastString = "";
            int index = 0;
            while (index < word.length()) {
                if (index != 0)
                    output.append("\n");
                lastString = word.substring(index, Math.min(index + MAX_LINE_LENGTH, word.length()));
                output.append(lastString);
                index += MAX_LINE_LENGTH;
            }

            lineLen += lastString.length();
        }


        StringTokenizer lines = new StringTokenizer(output.toString(), "\n");
        int totalLines = lines.countTokens();
        List<String> split = new ArrayList<>();
        int lineCount = 0;
        while (lines.hasMoreTokens()) {
            String str = lines.nextToken();
            int length = MAX_LINE_LENGTH - str.trim().length();
            String whiteSpaces = generateSpaces(length);

            // lineCount condition is to align first line to left and remaining parts are aligned to right
            // if totalLines greater than 2 means its a paragraph. no need to align right
            if (align == RIGHT || (lineCount > 0 && totalLines < 3)) {
                str = whiteSpaces + str;
            } else if (align == LEFT) {
                str = str + whiteSpaces;
            }

            split.add(str);
            lineCount++;
        }

        return split;
    }


    /**
     * function to generate white spaces
     *
     * @param n number of white space
     * @return return string with n white space
     */
    private String generateSpaces(int n) {
        Log.d("Print", "--" + n);
        return CharBuffer.allocate(n).toString().replace('\0', ' ');
    }


    /**
     * this add white spaces to the input string according to where to align
     *
     * @param str   input string
     * @param align where to align
     * @return result string
     */
    private String alignString(String str, int align) {

        int length = MAX_LINE_LENGTH - str.trim().length();
        String whiteSpaces = generateSpaces(length);

        if (align == RIGHT) {
            str = whiteSpaces + str;
        } else if (align == LEFT) {
            str = str + whiteSpaces;
        }

        return str;
    }

    private byte[] getNetworkPrintAlignment(int val) {
        switch (val) {
            case 0: //left
                return new byte[]{0x1b, 'a', 0x00};
            case 1: //center
                return new byte[]{0x1b, 'a', 0x01};
            case 2: //right
                return new byte[]{0x1b, 'a', 0x02};
            default: //left
                return new byte[]{0x1b, 'a', 0x00};

        }
    }

    private byte[] getNetworkPrintFont(int val) {
        switch (val) {
            case 0: //normal
                return new byte[]{0x1B, 0x21, 0x03};
            case 1: //bold
                return new byte[]{0x1B, 0x21, 0x08};
            case 2: //bold with medium text
                return new byte[]{0x1B, 0x21, 0x20};
            case 3: //bold with large text
                return new byte[]{0x1B, 0x21, 0x10};
            default: //normal
                return new byte[]{0x1B, 0x21, 0x03};

        }
    }


    private int getPoyntPrintAlignment(int val) {
        switch (val) {
            case 0: //left
                return PrintUtil2.LEFT;
            case 1: //center
                return PrintUtil2.CENTER;
            case 2: //right
                return PrintUtil2.RIGHT;
            default: //left
                return PrintUtil2.LEFT;

        }
    }


    public PoyntPrintData printOrderResponse(PrintData otherData1) {
        int barcodeLine = 0;

        PoyntPrintData poyntPrintData = new PoyntPrintData();
        PrintedReceipt printedReceiptHead = new PrintedReceipt();
        PrintedReceipt printedReceiptFoot = new PrintedReceipt();
        List<PrintedReceiptLine> receiptBodyHead = new ArrayList<>();
        List<PrintedReceiptLine> receiptBodyFoot = new ArrayList<>();


        PrintedReceipt printedReceipt = new PrintedReceipt();
        List<PrintedReceiptLine> receiptBody = new ArrayList<>();


        List<PRINTHEADER> printHeader = otherData1.getpRINTHEADER();
        for (PRINTHEADER header : printHeader) {
            if (header.getPrintType1() != null && header.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(header.getPrintData1()));
                receiptBodyHead.add(newLine(header.getPrintData1()));
            } else {
                try {
                    String url = header.getPrintData1();
                    url = Urls.IMAGE_BASE_URL + url;
                    String filename = url.substring(url.lastIndexOf('/') + 1);
                    Bitmap bmp;
                    System.out.println(filename);
                    if (checkFileExists(filename)) {
                        bmp = loadImageBitmap(context, filename);
                    } else {
                        System.out.println(url);
                        bmp = GeneralUtil.getBitmapFromURL(url);
                        saveImage(bmp, filename);
                    }

                    printedReceipt.setHeaderImage(bmp);
                    //printedReceiptHead.setHeaderImage(bmp);
                    if (bmp.getWidth() > 370)
                        bmp = scaleBitmap(bmp, 370, 135);
                    poyntPrintData.setLogo(bmp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (header.getPrintData2() != null && header.getPrintData2().length() > 0) {
                if (header.getPrintType2() != null && header.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(header.getPrintData2()));
                    receiptBodyHead.add(newLine(header.getPrintData2()));
                }
            }
        }

        List<PRINTBODY> printBody = otherData1.getpRINTBODY();
        for (PRINTBODY body : printBody) {
            if (body.getPrintType1() != null && body.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(body.getPrintData1()));

                if (barcodeLine < 2)
                    receiptBodyHead.add(newLine(body.getPrintData1()));
                else
                    receiptBodyFoot.add(newLine(body.getPrintData1()));

                if (barcodeLine == 1) {
                    barcodeLine++;

                    String orderNo = body.getPrintData1().trim();
                    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                    try {
                        BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.QR_CODE, 200, 200);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                        poyntPrintData.setQr(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.CODE_128, 400, 80);
                        BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                        Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                        poyntPrintData.setBarcode(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (body.getPrintData1().contains("Ksher Order") || body.getPrintData1().contains("Ksher order")) {
                    barcodeLine++;
                }

                int pos = printBody.indexOf(body);
                if (body.getPrintData1().trim().equals("order Number:") && printBody.get(pos - 1).getPrintData1().contains("Original Ksher")) {
                    barcodeLine++;
                }
            }
            if (body.getPrintData2() != null && body.getPrintData2().length() > 0) {
                if (body.getPrintType2() != null && body.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(body.getPrintData2()));
                    if (barcodeLine == 0)
                        receiptBodyHead.add(newLine(body.getPrintData2()));
                    else
                        receiptBodyFoot.add(newLine(body.getPrintData2()));
                }
            }
        }

        List<PRINTFOOTER> printFooter = otherData1.getpRINTFOOTER();
        for (PRINTFOOTER footer : printFooter) {
            if (footer.getPrintType1() != null && footer.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(footer.getPrintData1()));
                receiptBodyFoot.add(newLine(footer.getPrintData1()));
            } else {
                try {
                    Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData1());
                    printedReceipt.setFooterImage(bmp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (footer.getPrintData2() != null && footer.getPrintData2().length() > 0) {
                if (footer.getPrintType2() != null && footer.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(footer.getPrintData2()));
                    receiptBodyFoot.add(newLine(footer.getPrintData2()));
                }
            }
        }

        printedReceiptHead.setBody(receiptBodyHead);
        poyntPrintData.setHead(printedReceiptHead);

        printedReceiptFoot.setBody(receiptBodyFoot);
        poyntPrintData.setFooter(printedReceiptFoot);

        printedReceipt.setBody(receiptBody);
        poyntPrintData.setOriginal(printedReceipt);
        return poyntPrintData;
    }


    public PoyntPrintDataNestedImg printOrderResponseOtherMode(PrintData otherData1) {
        int barcodeLine = 0;

        PoyntPrintDataNestedImg poyntPrintData = new PoyntPrintDataNestedImg();
        PrintedReceipt printedReceiptHead = new PrintedReceipt();
        PrintedReceipt printedReceiptFoot = new PrintedReceipt();
        List<PrintedReceiptLine> receiptBodyHead = new ArrayList<>();
        List<PrintedReceiptLine> receiptBodyFoot = new ArrayList<>();


        PrintedReceipt printedReceipt = new PrintedReceipt();
        List<PrintedReceiptLine> receiptBody = new ArrayList<>();


        List<PRINTHEADER> printHeader = otherData1.getpRINTHEADER();
        for (PRINTHEADER header : printHeader) {
            if (header.getPrintType1() != null && header.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(header.getPrintData1()));
                receiptBodyHead.add(newLine(header.getPrintData1()));
            } else {
                try {

                    String url = header.getPrintData1();
                    if (url.startsWith("/")) {
                        url = Urls.IMAGE_BASE_URL + url;
                    }
                    String filename = url.substring(url.lastIndexOf('/') + 1);
                    Bitmap bmp = null;
                    System.out.println(filename);


                    if(url.contains(Urls.IMAGE_BASE_URL)) {

                        if (checkFileExists(filename)) {
                            bmp = loadImageBitmap(context, filename);
                        } else {
                            System.out.println(url);
                            bmp = GeneralUtil.getBitmapFromURL(url);
                            saveImage(bmp, filename);
                        }
                        if (bmp.getWidth() > 370 )
                            bmp = scaleBitmap(bmp, 370, 135);
//                        bmp = GeneralUtil.getBitmapFromURL(url);
                        poyntPrintData.setImage(bmp);
                    } else {
                        Log.d("PrintFooter", "Footer img");
                        bmp = GeneralUtil.getBitmapFromURLForSecure(url);
                        if (bmp.getWidth() > 370)
                            bmp = scaleBitmap(bmp, 370, 135);
                        poyntPrintData.setLogos(bmp);
                    }
                    //printedReceiptHead.setHeaderImage(bmp);

//                    poyntPrintData.setLogo(bmp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (header.getPrintData2() != null && header.getPrintData2().length() > 0) {
                if (header.getPrintType2() != null && header.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(header.getPrintData2()));
                    receiptBodyHead.add(newLine(header.getPrintData2()));
                }
            }
        }

        List<PRINTBODY> printBody = otherData1.getpRINTBODY();
        for (PRINTBODY body : printBody) {
            if (body.getPrintType1() != null && body.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(body.getPrintData1()));

                if (barcodeLine < 2)
                    receiptBodyHead.add(newLine(body.getPrintData1()));
                else
                    receiptBodyFoot.add(newLine(body.getPrintData1()));

                if (barcodeLine == 1) {
                    barcodeLine++;
                }

                if (body.getPrintData1().contains("Ksher Order") || body.getPrintData1().contains("Ksher order")) {
                    barcodeLine++;
                }

                int pos = printBody.indexOf(body);
                if (body.getPrintData1().trim().equals("order Number:") && printBody.get(pos - 1).getPrintData1().contains("Original Ksher")) {
                    barcodeLine++;
                }
            }
            if (body.getPrintData2() != null && body.getPrintData2().length() > 0) {
                if (body.getPrintType2() != null && body.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(body.getPrintData2()));
                    if (barcodeLine == 0)
                        receiptBodyHead.add(newLine(body.getPrintData2()));
                    else
                        receiptBodyFoot.add(newLine(body.getPrintData2()));
                }
            }
        }

        List<PRINTFOOTER> printFooter = otherData1.getpRINTFOOTER();
        for (PRINTFOOTER footer : printFooter) {
            if (footer.getPrintType1() != null && footer.getPrintType1().equalsIgnoreCase("TEXT")) {
                receiptBody.add(newLine(footer.getPrintData1()));
                receiptBodyFoot.add(newLine(footer.getPrintData1()));
            } else {
                try {
                    Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData1());
                    printedReceipt.setFooterImage(bmp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (footer.getPrintData2() != null && footer.getPrintData2().length() > 0) {
                if (footer.getPrintType2() != null && footer.getPrintType2().equalsIgnoreCase("TEXT")) {
                    receiptBody.add(newLine(footer.getPrintData2()));
                    receiptBodyFoot.add(newLine(footer.getPrintData2()));
                }
            }
        }

        printedReceiptHead.setBody(receiptBodyHead);
        poyntPrintData.setBody(printedReceiptHead);

        printedReceiptFoot.setBody(receiptBodyFoot);
        poyntPrintData.setFooter(printedReceiptFoot);

        printedReceipt.setBody(receiptBody);
        poyntPrintData.setOriginal(printedReceipt);
        return poyntPrintData;
    }


    public boolean networkPrint(PrintData otherData1) {
        String ipAddress = otherData1.getPRINTER_IP();
        int port = Integer.parseInt(otherData1.getPRINTER_PORT());
        boolean isAvailable = isPrinterAvailable(ipAddress, port);

        if (!isAvailable) {
            for (int i = 0; i < 3; i++) {
                isAvailable = isPrinterAvailable(ipAddress, port);

                if (isAvailable)
                    break;

                try {
                    Thread.sleep(3000);
                } catch (Exception ignored) {
                }
            }
        }


        if (isAvailable) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {

                List<PRINTHEADER> printHeader = otherData1.getpRINTHEADER();
                for (PRINTHEADER header : printHeader) {
                    if (header.getPrintType1() != null && header.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(header.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(header.getPrintAlign1()));
                        outputStream.write(header.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());
                    } else {
                        try {
                            outputStream.write(getNetworkPrintFont(header.getPrintFont1()));
                            outputStream.write(getNetworkPrintAlignment(1));
                            String url = header.getPrintData1();
                            url = Urls.IMAGE_BASE_URL + url;
                            String filename = url.substring(url.lastIndexOf('/') + 1);
                            Bitmap bmp;
                            System.out.println(filename);
                            if (checkFileExists(filename)) {
                                bmp = loadImageBitmap(context, filename);
                            } else {
                                System.out.println(url);
                                bmp = GeneralUtil.getBitmapFromURL(url);
                                saveImage(bmp, filename);
                            }
                            if (bmp.getWidth() > 450)
                                bmp = scaleBitmap(bmp, 450, 135);
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    if (header.getPrintData2() != null && header.getPrintData2().length() > 0) {
                        if (header.getPrintType2() != null && header.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(header.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(header.getPrintAlign2()));
                            outputStream.write(header.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(header.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(header.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(header.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                List<PRINTBODY> printBody = otherData1.getpRINTBODY();
                for (PRINTBODY body : printBody) {
                    if (body.getPrintType1() != null && body.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(body.getPrintAlign1()));
                        outputStream.write(body.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());

                        if (body.getPrintData1().contains("Ksher Order") || body.getPrintData1().contains("Ksher order")) {
                            String orderNo = body.getPrintData1().substring(body.getPrintData1().lastIndexOf(" ") + 1);
                            orderNo = orderNo.trim();
                            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                            try {
                                BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.QR_CODE, 200, 200);
                                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                                outputStream.write(getNetworkPrintAlignment(1));
                                byte[] command = GeneralUtil.decodeBitmap(bitmap);
                                if (command != null) {
                                    outputStream.write(command);
                                    outputStream.write("\n".getBytes());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.CODE_128, 400, 80);
                                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                                outputStream.write(getNetworkPrintAlignment(1));
                                byte[] command = GeneralUtil.decodeBitmap(bitmap);
                                if (command != null) {
                                    outputStream.write(command);
                                    outputStream.write("\n".getBytes());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    } else {
                        outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(body.getPrintAlign1()));
                        Bitmap bmp = GeneralUtil.getBitmapFromURL(body.getPrintData1());
                        byte[] command = GeneralUtil.decodeBitmap(bmp);
                        if (command != null) {
                            outputStream.write(command);
                            outputStream.write("\n".getBytes());
                        }
                    }

                    if (body.getPrintData2() != null && body.getPrintData2().length() > 0) {
                        if (body.getPrintType2() != null && body.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(body.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(body.getPrintAlign2()));
                            outputStream.write(body.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(body.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(body.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(body.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                List<PRINTFOOTER> printFooter = otherData1.getpRINTFOOTER();
                for (PRINTFOOTER footer : printFooter) {
                    if (footer.getPrintType1() != null && footer.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(footer.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign1()));
                        outputStream.write(footer.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());
                    } else {
                        outputStream.write(getNetworkPrintFont(footer.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign1()));
                        Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData1());
                        byte[] command = GeneralUtil.decodeBitmap(bmp);
                        if (command != null) {
                            outputStream.write(command);
                            outputStream.write("\n".getBytes());
                        }
                    }


                    if (footer.getPrintData2() != null && footer.getPrintData2().length() > 0) {
                        if (footer.getPrintType2() != null && footer.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(footer.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign2()));
                            outputStream.write(footer.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(footer.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                outputStream.write("\n".getBytes());
                outputStream.write(new byte[]{0x1D, 0x56, 66, 0x00});
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            return printWithNetworkPrint(ipAddress, port, outputStream.toByteArray());
        }
        return false;
    }

    public boolean networkPrintOtherMode(PrintData otherData1) {
        String ipAddress = otherData1.getPRINTER_IP();
//        String ipAddress = "192.168.0.5";
//        int port = 9100;
        int port = Integer.parseInt(otherData1.getPRINTER_PORT());
        boolean isAvailable = isPrinterAvailable(ipAddress, port);

        if (!isAvailable) {
            for (int i = 0; i < 3; i++) {
                isAvailable = isPrinterAvailable(ipAddress, port);

                if (isAvailable)
                    break;

                try {
                    Thread.sleep(3000);
                } catch (Exception ignored) {
                }
            }
        }


        if (isAvailable) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            try {

                List<PRINTHEADER> printHeader = otherData1.getpRINTHEADER();
                for (PRINTHEADER header : printHeader) {
                    if (header.getPrintType1() != null && header.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(header.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(header.getPrintAlign1()));
                        outputStream.write(header.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());
                    } else {
                        outputStream.write(getNetworkPrintFont(header.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(1));

                        String url = header.getPrintData1();
                        if (url.startsWith("/")) {
                            url = Urls.IMAGE_BASE_URL + url;
                        }
                        String filename = url.substring(url.lastIndexOf('/') + 1);
                        Bitmap bmp;
                        System.out.println(filename);


                        if(url.contains(Urls.IMAGE_BASE_URL)) {

//                            if (checkFileExists(filename)) {
//                                bmp = loadImageBitmap(context, filename);
//                            } else {
//                                System.out.println(url);
                                bmp = GeneralUtil.getBitmapFromURL(url);
//                                saveImage(bmp, filename);
//                            }
                        } else {
                            Log.d("PrintFooter", "Footer img");
                            bmp = GeneralUtil.getBitmapFromURLForSecure(url);
                        }

                        if (bmp.getWidth() > 450)
                            bmp = scaleBitmap(bmp, 450, 135);
                        byte[] command = GeneralUtil.decodeBitmap(bmp);
                        if (command != null) {
                            outputStream.write(command);
                            outputStream.write("\n".getBytes());
                        }
                    }

                    if (header.getPrintData2() != null && header.getPrintData2().length() > 0) {
                        if (header.getPrintType2() != null && header.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(header.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(header.getPrintAlign2()));
                            outputStream.write(header.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(header.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(header.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(header.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                List<PRINTBODY> printBody = otherData1.getpRINTBODY();
                for (PRINTBODY body : printBody) {
                    if (body.getPrintType1() != null && body.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(body.getPrintAlign1()));
                        outputStream.write(body.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());

                        if (body.getPrintData1().contains("Ksher Order") || body.getPrintData1().contains("Ksher order")) {
                            String orderNo = body.getPrintData1().substring(body.getPrintData1().lastIndexOf(" ") + 1);
                            orderNo = orderNo.trim();
                            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                            try {
                                BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.QR_CODE, 200, 200);
                                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                                outputStream.write(getNetworkPrintAlignment(1));
                                byte[] command = GeneralUtil.decodeBitmap(bitmap);
                                if (command != null) {
                                    outputStream.write(command);
                                    outputStream.write("\n".getBytes());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                BitMatrix bitMatrix = multiFormatWriter.encode(orderNo, BarcodeFormat.CODE_128, 400, 80);
                                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                                outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                                outputStream.write(getNetworkPrintAlignment(1));
                                byte[] command = GeneralUtil.decodeBitmap(bitmap);
                                if (command != null) {
                                    outputStream.write(command);
                                    outputStream.write("\n".getBytes());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    } else {
                        outputStream.write(getNetworkPrintFont(body.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(body.getPrintAlign1()));
                        Bitmap bmp = GeneralUtil.getBitmapFromURL(body.getPrintData1());
                        byte[] command = GeneralUtil.decodeBitmap(bmp);
                        if (command != null) {
                            outputStream.write(command);
                            outputStream.write("\n".getBytes());
                        }
                    }

                    if (body.getPrintData2() != null && body.getPrintData2().length() > 0) {
                        if (body.getPrintType2() != null && body.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(body.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(body.getPrintAlign2()));
                            outputStream.write(body.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(body.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(body.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(body.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                List<PRINTFOOTER> printFooter = otherData1.getpRINTFOOTER();
                for (PRINTFOOTER footer : printFooter) {
                    if (footer.getPrintType1() != null && footer.getPrintType1().equalsIgnoreCase("TEXT")) {
                        outputStream.write(getNetworkPrintFont(footer.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign1()));
                        outputStream.write(footer.getPrintData1().getBytes());
                        outputStream.write("\n".getBytes());
                    } else {
                        outputStream.write(getNetworkPrintFont(footer.getPrintFont1()));
                        outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign1()));
                        Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData1());
                        byte[] command = GeneralUtil.decodeBitmap(bmp);
                        if (command != null) {
                            outputStream.write(command);
                            outputStream.write("\n".getBytes());
                        }
                    }


                    if (footer.getPrintData2() != null && footer.getPrintData2().length() > 0) {
                        if (footer.getPrintType2() != null && footer.getPrintType2().equalsIgnoreCase("TEXT")) {
                            outputStream.write(getNetworkPrintFont(footer.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign2()));
                            outputStream.write(footer.getPrintData2().getBytes());
                            outputStream.write("\n".getBytes());
                        } else {
                            outputStream.write(getNetworkPrintFont(footer.getPrintFont2()));
                            outputStream.write(getNetworkPrintAlignment(footer.getPrintAlign2()));
                            Bitmap bmp = GeneralUtil.getBitmapFromURL(footer.getPrintData2());
                            byte[] command = GeneralUtil.decodeBitmap(bmp);
                            if (command != null) {
                                outputStream.write(command);
                                outputStream.write("\n".getBytes());
                            }
                        }
                    }
                }

                outputStream.write("\n".getBytes());
                outputStream.write(new byte[]{0x1D, 0x56, 66, 0x00});
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            return printWithNetworkPrint(ipAddress, port, outputStream.toByteArray());
        }
        return false;
    }


    /**
     * check whether is printer available, don't call it from UI thread
     *
     * @param ip   - ip address of printer
     * @param port - port address
     * @return true if printer is available
     */
    private boolean isPrinterAvailable(String ip, int port) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), PRINTER_TIMEOUT);
            socket.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * print given buffer to the printer with specified ip and port,  don't call it from UI thread
     *
     * @param ip     ip address of printer
     * @param port   port number of printer
     * @param buffer byte array to print
     * @return true if byte array is successfully send to printer
     */
    private boolean printWithNetworkPrint(String ip, int port, byte[] buffer) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), PRINTER_TIMEOUT);
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(buffer);
            socket.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean checkFileExists(String imageName) {
        boolean exist = false;
        File file = context.getApplicationContext().getFileStreamPath(imageName);
        if (file.exists()) {
            exist = true;
        }
        return exist;
    }

    public void saveImage(Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("saveImage", "Exception 2, Something went wrong!");
        }
    }

    public Bitmap loadImageBitmap(Context context, String imageName) {
        Bitmap bitmap = null;
        FileInputStream fiStream;
        try {
            fiStream = context.openFileInput(imageName);
            bitmap = BitmapFactory.decodeStream(fiStream);
            fiStream.close();
        } catch (Exception e) {
            Log.d("saveImage", "Exception 3, Something went wrong!");
        }
        return bitmap;
    }

    public Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        int currentBitmapWidth = bitmap.getWidth();
        int currentBitmapHeight = bitmap.getHeight();

        int ivWidth = wantedWidth;
        int ivHeight = wantedHeight;

        int newWidth = wantedWidth;

        int newHeight = (int) Math.floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));

        Bitmap newbitMap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);

        return newbitMap;
    }
}
