package com.wps.terminalapp.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;
import com.wps.terminalapp.R;
import com.wps.terminalapp.custom.ButtonSansRegular;
import com.wps.terminalapp.custom.DotProgressDialog;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.listeners.AlertListener;
import com.wps.terminalapp.listeners.ErrorListener;
import com.wps.terminalapp.models.PrintModel;
import com.wps.terminalapp.models.TransactionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by vignaraj.r on 08-Aug-18.
 */

public class Util {

    public static String TAG = Util.class.getSimpleName();
    //for batch
    //Live
    public static final String providerName = "com.chk.mashreqtp.BatchProvider";
    public static final String URL = "content://" + providerName + "/batch";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    //Test
    public static final String testProviderName = "com.wps.mashreqtp.BatchProvider";
    public static final String TESTURL = "content://" + testProviderName + "/batch";
    public static final Uri TESTCONTENT_URI = Uri.parse(TESTURL);
    //

    public static MDToast mdToast;
    static DotProgressDialog progressDialog;
    public static boolean isPopBack = false;
    public static List<TransactionModel> transactionList = new ArrayList<>();
    public static List<TransactionModel> historyList = new ArrayList<>();

    public static String lastSelectedFilter = "ALL";
    public static String currentFilter = "ALL";

    public static String currentAcquirer = "";
    public static boolean isAmexExist = false;
    public static String supportedPayment = "";

    public static String settleAmount = "0.00";
    public static String totalCardAmount = "0.00";
    public static String totalCashAmount = "0.00";
    public static String totalTipAmount = "0.00";
    public static String currentSettleYN = "";

    public static List<PrintModel> header = new ArrayList<>();
    public static List<PrintModel> body = new ArrayList<>();
    public static List<PrintModel> footer = new ArrayList<>();

    private static String[] binaryArray = {"0000", "0001", "0010", "0011",
            "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
            "1100", "1101", "1110", "1111"};

    private static String hexStr = "0123456789ABCDEF";

    public static Typeface GetFont(AssetManager am, String sfont) {
//        if (sfont.equals("fonts/GoogleSans-Bold.ttf"))
//        {
//          return  Typeface.createFromAsset(am, "fonts/GoogleSans-Bold.ttf");
//        }
//        else if(sfont.equals("fonts/GoogleSans-Regular.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/GoogleSans-Regular.ttf");
//        }
//        else if(sfont.equals("fonts/GoogleSans-BoldItalic.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/GoogleSans-BoldItalic.ttf");
//        }
//        else if(sfont.equals("fonts/GoogleSans-Italic.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/GoogleSans-Italic.ttf");
//        }
//        else if(sfont.equals("fonts/GoogleSans-Medium.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/GoogleSans-Medium.ttf");
//        }
//        else if(sfont.equals("fonts/Roboto-Bold.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/Roboto-Bold.ttf");
//        }
//        else if(sfont.equals("fonts/Roboto-Medium.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/Roboto-Medium.ttf");
//        }
//        else if(sfont.equals("fonts/Roboto-Regular.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/Roboto-Regular.ttf");
//        }
//        else if(sfont.equals("fonts/Roboto-Thin.ttf"))
//        {
//            return  Typeface.createFromAsset(am, "fonts/Roboto-Thin.ttf");
//        }
//        else
//        {
//
//        }
        return Typeface.createFromAsset(am, sfont);
        //return Typeface.createFromAsset(am, "fonts/GoogleSans-BoldItalic.ttf");
    }

    public static void freeMemory() {
        //System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }


    public static byte[] decodeBitmap(Bitmap bmp) {
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();

        List<String> list = new ArrayList<String>(); //binaryString list
        StringBuffer sb;


        int bitLen = bmpWidth / 8;
        int zeroCount = bmpWidth % 8;

        String zeroStr = "";
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1;
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = zeroStr + "0";
            }
        }

        for (int i = 0; i < bmpHeight; i++) {
            sb = new StringBuffer();
            for (int j = 0; j < bmpWidth; j++) {
                int color = bmp.getPixel(j, i);

                int r = (color >> 16) & 0xff;
                int g = (color >> 8) & 0xff;
                int b = color & 0xff;

                // if color close to white，bit='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0");
                else
                    sb.append("1");
            }
            if (zeroCount > 0) {
                sb.append(zeroStr);
            }
            list.add(sb.toString());
        }

        List<String> bmpHexList = binaryListToHexStringList(list);
        String commandHexString = "1D763000";
        String widthHexString = Integer
                .toHexString(bmpWidth % 8 == 0 ? bmpWidth / 8
                        : (bmpWidth / 8 + 1));
        if (widthHexString.length() > 2) {
            Log.e("decodeBitmap error", " width is too large");
            return null;
        } else if (widthHexString.length() == 1) {
            widthHexString = "0" + widthHexString;
        }
        widthHexString = widthHexString + "00";

        String heightHexString = Integer.toHexString(bmpHeight);
        if (heightHexString.length() > 2) {
            Log.e("decodeBitmap error", " height is too large");
            return null;
        } else if (heightHexString.length() == 1) {
            heightHexString = "0" + heightHexString;
        }
        heightHexString = heightHexString + "00";

        List<String> commandList = new ArrayList<String>();
        commandList.add(commandHexString + widthHexString + heightHexString);
        commandList.addAll(bmpHexList);

        return hexList2Byte(commandList);
    }

    public static List<String> binaryListToHexStringList(List<String> list) {
        List<String> hexList = new ArrayList<String>();
        for (String binaryStr : list) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < binaryStr.length(); i += 8) {
                String str = binaryStr.substring(i, i + 8);

                String hexString = myBinaryStrToHexString(str);
                sb.append(hexString);
            }
            hexList.add(sb.toString());
        }
        return hexList;

    }

    public static String myBinaryStrToHexString(String binaryStr) {
        String hex = "";
        String f4 = binaryStr.substring(0, 4);
        String b4 = binaryStr.substring(4, 8);
        for (int i = 0; i < binaryArray.length; i++) {
            if (f4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }
        for (int i = 0; i < binaryArray.length; i++) {
            if (b4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }

        return hex;
    }

    public static byte[] hexList2Byte(List<String> list) {
        List<byte[]> commandList = new ArrayList<byte[]>();

        for (String hexStr : list) {
            commandList.add(hexStringToBytes(hexStr));
        }
        byte[] bytes = sysCopy(commandList);
        return bytes;
    }

    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    public static byte[] sysCopy(List<byte[]> srcArrays) {
        int len = 0;
        for (byte[] srcArray : srcArrays) {
            len += srcArray.length;
        }
        byte[] destArray = new byte[len];
        int destLen = 0;
        for (byte[] srcArray : srcArrays) {
            System.arraycopy(srcArray, 0, destArray, destLen, srcArray.length);
            destLen += srcArray.length;
        }
        return destArray;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static void clearValues() {
        header.clear();
        body.clear();
        footer.clear();
    }

    /**
     * Add the print format.
     */
    public static void addIntoList(List<PrintModel> list, JSONArray array) {
        try {
            if (array != null) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    // for type1
                    String printType1 = obj.optString("printType1");
                    String printData1 = obj.optString("printData1");
                    int printAlign1 = obj.optInt("printAlign1");
                    int printFont1 = obj.optInt("printFont1");
                    // for type2
                    String printType2 = obj.optString("printType2");
                    String printData2 = obj.optString("printData2");
                    int printAlign2 = obj.optInt("printAlign2");
                    int printFont2 = obj.optInt("printFont2");

                    list.add(new PrintModel(printType1, printData1, printAlign1, printFont1, printType2, printData2, printAlign2, printFont2));
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static String formatAmount(double Amount) {
        String amount = "0.00";
        if (Amount != 0.0) {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);
            amount = nf.format(Amount);
        }
        return amount;
    }

    /**
     * Get the current date and time as a TimeStamp.
     **/
    public static long getCurrentTimeStamp() {
        long timestamp = 0;
        try {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy hh:mm ss a");
////            String currentDateTime = dateFormat.format(c.getTime()); // Find todays date
            timestamp = c.getTime().getTime();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return timestamp;
    }

    /**
     * Convert TimeStamp into Date
     **/
    public static String getDateString(long timeStamp, String format) {
        try {
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy hh:mm ss a");
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return "xx";
        }
    }

    public static void showErrorDialog(final Context context, final String error, final ErrorListener listener) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Dialog alertDlg = new Dialog(context);
                alertDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDlg.setContentView(R.layout.alert_message);
                alertDlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                TextSansRegular txtError = (TextSansRegular) alertDlg.findViewById(R.id.error_message);
                ButtonSansRegular btnCrossError = (ButtonSansRegular) alertDlg.findViewById(R.id.cross_error_btn);
                btnCrossError.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.doAction();
                        alertDlg.dismiss();
                    }
                });
                txtError.setText(error);
                alertDlg.show();
            }
        });
    }

    public static void showProgressDialog(final Context context, final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    Log.i("show_prg ", "not_null_progress");
                    progressDialog.setMessage(message);
                    progressDialog.show();
                } else {
                    Log.i("show_prg ", "null");
                    progressDialog = new DotProgressDialog(context, message);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    progressDialog.show();
                }
            }
        });
    }

    public static void closeProgressDialog() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                    Log.i("close_prg ", "null");
                }
            }
        });
    }


    /**
     * Print the log with more value.
     **/
    public static void logLargeString(String TAG, String str) {

        if (str.length() > 3000) {
            Log.i(TAG, str.substring(0, 3000));
            logLargeString(TAG, str.substring(3000));
        } else {
            Log.i(TAG, str);
        }
    }

    /**
     * Show the success toast message
     **/
    public static void showSuccessToast(final Context context, final String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mdToast != null) {
                    mdToast.cancel();
                }
                mdToast = MDToast.makeText(context, s, MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS);
                mdToast.show();
            }
        });
    }

    /**
     * Show the Error toast message
     **/
    public static void showErrorToast(final Context context, final String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mdToast != null) {
                    mdToast.cancel();
                }
                mdToast = MDToast.makeText(context, s, MDToast.LENGTH_LONG, MDToast.TYPE_ERROR);
                mdToast.show();
            }
        });
    }

    public static void showInfoToast(final Context context, final String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mdToast != null) {
                    mdToast.cancel();
                }
                mdToast = MDToast.makeText(context, s, MDToast.LENGTH_LONG, MDToast.TYPE_INFO);
                mdToast.show();
            }
        });
    }

    /**
     * Show the Warning toast message
     **/
    public static void showWarningToast(final Context context, final String s) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (mdToast != null) {
                    mdToast.cancel();
                }
                mdToast = MDToast.makeText(context, s, MDToast.LENGTH_LONG, MDToast.TYPE_WARNING);
                mdToast.show();
            }
        });
    }

    /**
     * Animate the TextView by Incrementing the number
     **/
    public static void animateText(final TextView view, int... args) {
        final int endValue = args[0];
        int decimalValue = 0;
        if (args[1] != 0) {
            decimalValue = args[1];
        }
        ValueAnimator animator = ValueAnimator.ofInt(0, endValue);
        animator.setDuration(3000);

        final int finalDecimalValue = decimalValue;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setText(animation.getAnimatedValue().toString() + ".00");
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                String dVal = "00";
                if (finalDecimalValue < 10) {
                    dVal = "0" + String.valueOf(finalDecimalValue);
                } else {
                    dVal = String.valueOf(finalDecimalValue);
                }
                view.setText(String.valueOf(endValue) + "." + dVal);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animator.start();
    }

    public static void hideKeyBoard(Context context, View view) {
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*for batch*/
    public static boolean isProviderExist(Context context) {
        dTag(0, TAG, "isProviderExist");
        boolean isSuccess;
        ContentProviderClient liveProvider = context.getContentResolver().acquireContentProviderClient(
                CONTENT_URI);
        if (liveProvider == null) {
            isSuccess = false;
        } else {
            isSuccess = true;
        }
        Log.d("provider_exist", String.valueOf(isSuccess));
        return isSuccess;
    }

    public static void addBatchID(final Context context, String batchNo) {
        dTag(0, TAG, "addBatchID");
        Log.i("batchId", String.valueOf(batchNo));
        final ContentValues values = new ContentValues();
        values.put("batch_id", batchNo);
        if (isProviderExist(context)) {
            context.getContentResolver().insert(CONTENT_URI, values);
            saveCurrentURI("com.chk.mashreqtp", context);
        } else {
            context.getContentResolver().insert(TESTCONTENT_URI, values);
            saveCurrentURI("com.wps.mashreqtp", context);
        }
        Log.i("processor#", getCurrentURI(context));
    }


    /**
     * Deleting Batch
     */
    public static void deleteBatchValue(Context context) {
        dTag(0, TAG, "deleteBatchValue");
        boolean result;
        if (isProviderExist(context)) {
            context.getContentResolver().delete(CONTENT_URI, null, null);
            saveCurrentURI("com.chk.mashreqtp", context);
        } else {
            context.getContentResolver().delete(TESTCONTENT_URI, null, null);
            saveCurrentURI("com.wps.mashreqtp", context);
        }
    }

    /**
     * Saved Current URI
     **/
    public static void saveCurrentURI(String uri, Context context) {
        dTag(0, TAG, "saveCurrentURI");
        SharedPreferences preferences = context.getSharedPreferences("batch_uri", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("uri", uri);
        editor.commit();
    }

    public static String getCurrentURI(Context context) {
        dTag(0, TAG, "getCurrentURI");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("batch_uri", MODE_PRIVATE);
        result = preferences.getString("uri", "");
        return result;
    }

    /**
     * Last inserted batch
     */
    public static String lastInsertedBatch(Context context) {
        dTag(0, TAG, "lastInsertedBatch");
        String batch = "0";
        Cursor c = context.getContentResolver().query(CONTENT_URI, null, null, null, null);
        if (c != null) {
            if (c.moveToLast()) {
                int position = c.getColumnIndex("batch_id");
                Log.i("position", String.valueOf(position));
                String str = c.getString(position);
                Log.i("l_batc", str);
                batch = c.getString(c.getColumnIndex("batch_id"));
                saveCurrentURI("com.chk.mashreqtp", context);
            }
        } else {
            Cursor c2 = context.getContentResolver().query(TESTCONTENT_URI, null, null, null, null);
            if (c2 != null) {
                if (c2.moveToLast()) {
                    int position = c2.getColumnIndex("batch_id");
                    Log.i("position", String.valueOf(position));
                    String str = c2.getString(position);
                    Log.i("l_batc", str);
                    batch = c2.getString(c2.getColumnIndex("batch_id"));
                    saveCurrentURI("com.wps.mashreqtp", context);
                }
            } else {
                batch = "0";
            }
            Log.d("last_batch", batch);
        }
        return batch;
    }

    public static void deleteCache(Context context) {
        dTag(0, TAG, "deleteCache");
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static void deleteCache1(Context context) {
        dTag(0, TAG, "deleteCache");
        try {
            File dir = context.getFilesDir();
            deleteDir(dir);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static boolean deleteDir(File dir) {
        dTag(0, TAG, "deleteDir");
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    /**
     * Convert the String into Date.
     **/
    public static Date stringToDate(String str) {

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = format.parse(str);
            System.out.println(date);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }
        return date;
    }

    public static String convertDate(String date, String format) {
        Date cDate = stringToDate(date);
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String stringDate = "";
        try {
            stringDate = sdf.format(cDate);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        Log.i("c_date#", stringDate);
        return stringDate;
    }

    /**
     * Convert the date into String.
     **/
    public static String convertDate(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String stringDate = "";
        try {
            stringDate = sdf.format(date);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return stringDate;
    }


    public static Date getDateFromTimeStamp(long timestamp) {
        try {
            Date netDate = (new Date(timestamp));
            return netDate;
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }

    public static long getFormattedTimeStamp(long timestamp, String format) {
        try {
            Date date = getDateFromTimeStamp(timestamp);
            String dateString = convertDate(date, format);
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date netDate = sdf.parse(dateString);
            return netDate.getTime();
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return 0L;
        }
    }


    public static String setDate(long timeStamp) {

        String date = "";
        Date finalDate = getDateFromTimeStamp(timeStamp);
        if (DateUtils.isToday(finalDate.getTime())) {
            date = "Today " + convertDate(finalDate, "hh:mm a");
        } else if (DateUtils.isToday(finalDate.getTime() + DateUtils.DAY_IN_MILLIS)) {
            date = "Yesterday " + convertDate(finalDate, "hh:mm a");
        } else {
            date = convertDate(finalDate, "dd-MMM-yyyy hh:mm a");
        }
        return date;
    }

    public static void dTag(int type, String TAG, String message) {
        String ty = "";
        if (type == 0) {
            ty = "Function - ";
        } else {
            ty = "Event - ";
        }
        Log.d(TAG, ty + message);
    }

    public static void showConfirmation(final Context context, final String message, final AlertListener listener) {
        dTag(0, TAG, "showConfirmation");
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                final Dialog alertDlg = new Dialog(context);
                alertDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDlg.setContentView(R.layout.alert_dialog);
                alertDlg.setCancelable(false);
                alertDlg.setCanceledOnTouchOutside(false);
                alertDlg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                TextSansBold txtMessage = (TextSansBold) alertDlg.findViewById(R.id.txt_message);
                TextSansBold txtOk = (TextSansBold) alertDlg.findViewById(R.id.txt_ok);
                TextSansBold txtCancel = (TextSansBold) alertDlg.findViewById(R.id.txt_cancel);
                txtMessage.setText(message);
                txtOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.done();
                        alertDlg.dismiss();
                    }
                });
                txtCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.cancel();
                        alertDlg.dismiss();
                    }
                });
                alertDlg.show();
            }
        });
    }


    static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    public static String randomAlphanumeric(int length) {
        Random random = new Random();
        StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }

        return builder.toString();
    }

    public static String getNextOrderNo(){
       // return Build.SERIAL + "OT" + System.currentTimeMillis();
        return UUID.randomUUID().toString();
    }


    public static String getNextRefundNo(){
        //return Build.SERIAL + "RT" + System.currentTimeMillis();
        return UUID.randomUUID().toString();
    }

}
