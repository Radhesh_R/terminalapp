package com.wps.terminalapp.utils;

import java.util.HashMap;
import java.util.Map;

public class ErrorCode {
    public static Map<String, String> codes = new HashMap<String, String>() {{
        put("KSHER_AMOUNT_IS_TOO_SMALL", "Order amount is too little/ small.");
        put("KSHER_DUPLICATED_ORDERNO", "Order number has been used.");
        put("KSHER_EXCEED_AMOUNT_LIMIT", "Payment amount exceeds configured amount limit.");
        put("KSHER_FEETYPE_NOT_MATCH", "Currency type does not match.");
        put("KSHER_INVALID_MCHINFO", "Merchant information is incorrect.");
        put("KSHER_INVALID_PARAM", "Missing parameter");
        put("KSHER_SIGN_ERROR", "Signature error");
        put("KSHER_SYSTEMERROR", "System error");
        put("KSHER_API_ERR_PARAM_OVERLENGTH", "Parameter exceeds length limit.");
        put("KSHER_API_ERR_INVALID_CHANNEL_WECHAT", "WeChat channel error");
        put("KSHER_API_ERR_INVALID_CHANNEL_ALIPAY", "Alipay channel error");
        put("KSHER_API_ERR_INVALID_CHANNEL", "Payment channel error");
        put("KSHER_API_ERR_INVALID_PAY_AUTH_OF_MASTER_MCH", "Master merchant appid is not allowed for transaction");

        put("KSHER_INVALID_ORDER_NO", "Invalid order number");
        put("KSHER_ERROR_ORDER_NO", "Order number error");

        put("KSHER_INVALID_REFUND_AUTH", "Merchants have no refund and revoke permission.");
        put("KSHER_REFUND_HAS_INITIATED", "The merchant is initiating a refund or revoke");
        put("KSHER_REVERSE_EXPIRE", "The revoke period has passed");
        put("KSHER_INVALID_REFUND_BALANCE", "Insufficient refund balance");

        put("KSHER_ERROR_REFUND_ORDER_NO", "Merchant refund amount parameter error");
        put("KSHER_INVALID_REFUND_AMOUNT", "Merchant refund amount parameter error");
        put("KSHER_REFUND_EXPIRE", "Refund expired");

        put("KSHER_DUPLICATED_REFUND_ORDERNO", "Duplicate merchant refund number");
        put("KSHER_INVALID_REFUND_FEE_OR_TYPE", "Invalid refund amount or currency");
        put("SYSTEMERROR", "system error ");
        put("INVALID_ORDER_NO", "invalid order no. ");

        put("PARAM_ERROR", "parameters error");
        put("REQUIRE_POST_METHOD", "POST method is required");
        put("SIGNERROR", "signature error ");
        put("KSHER_VERSION_ERROR", "API version used by merchant is not compatible with the API of the vendor");
        put("KSHER_PARAM_OVERLENGTH", "Some parameters are too long.");
        put("KSHER_CHANNEL_RESPONSE_ERROR", "Error occurred during query operation ");
        put("USER_ACCOUNT_ABNORMALE ", "refund failed");
        put("APPID_NOT_EXIST", "Invalid APPID ");
        put("XML_FORMAT_ERROR", "XML format error ");
        put("KSHER_INVALID_REFUND_FEE", "invalid amount to refund");
    }};
}
