package com.wps.terminalapp.utils;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map;

public class WPRSAUtil {

    private static final String privateKeypkcs8 =
            "MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBAIdmb8dYOTiBBBsb\n" +
                    "UtkONjJGxYPP5xH8921ajDqqHeF0G1tYbwvcQGOkvBmzQGtbWHFKRGnpH4Jb3/u5\n" +
                    "R+tEvsNHUkoSkRzONMTgaOfzaOGugvb123caZUA3tY30xuoCoLKq+s5StIs15FGT\n" +
                    "hS2FFF6w+TkvF+4XPhXrVbLXK3c9AgMBAAECgYB9m/xKlH/Q+W9Trk93aYXBsoH9\n" +
                    "vFuDBoiLfBrnOXYq90mBvlKWUCezyNlvhilW7xDnWJyLHGfMyWrK3aFqKEefIYJN\n" +
                    "zmFafGtamC5vtFN6iY1HLgu/CbcaAzc5SYVx/X1TsH5/pACR834oRLVxd4V9uzUE\n" +
                    "JQP4q2GSuVIYHIRWSQJFAIhEx5/hBYGr+1msK+DqYwmFT16VaPplknjSU7qQlNk8\n" +
                    "eo+Fm1+LOd/1md6vcS6WiSm9S5n9SfIK/jIE9kjv2Ra9AXX3Aj0A/l5MAd8JWHUT\n" +
                    "dt4JIIS49vTgjYkld78mmy3eQ8feo52rHC55dq7c5Fd3r1j3ApN0eSQnQcmVtie1\n" +
                    "W99rAkRTBu1GmblcL+ie04uMp+Md+u7IJ2rmHhsnqKdBZjR7RCE872Aiz9a8gY12\n" +
                    "JBlnFGuQVfkJY025vz7wRRu2hwUTmKeaMwI8GzA/FZAecpsI+pKfDR/CmXrxKY+4\n" +
                    "NoVy/fE4KayKapimuntpNjtYlvuWamKq/FaAg5ZOr+R5ISA7bVeDAkRC5CTIzPTv\n" +
                    "OZtNa6JY08/mwVle+wCms5m3FrGg/ioR3V3RaPvOXoe1NCXhENsGbY9Kmth60R70\n" +
                    "fHSUMHrjOTmS7Z9/kw==\n";

    private static final String publicKeypkcs8 = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAL7955OCuN4I8eYNL/mixZWIXIgCvIVEivlxqdpiHPcOLdQ2RPSx/pORpsUu/E9wz0mYS2PY7hNc2mBgBOQT+wUCAwEAAQ==";

    public static String ksherSign(Map params, String privateKey) {
        try {
            byte[] data = getParamsSort(params);
            byte[] privateKeyBytes = android.util.Base64.decode(privateKey.getBytes(),android.util.Base64.DEFAULT );
            //byte[] privateKeyBytes = Base64.decodeBase64(this.privateKey.getBytes());
            PKCS8EncodedKeySpec pkcs = new PKCS8EncodedKeySpec(privateKeyBytes);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PrivateKey key = factory.generatePrivate(pkcs);
            Signature signature = Signature.getInstance("MD5withRSA");
            signature.initSign(key);
            signature.update(data);

            byte[] sign_byte = signature.sign();
            //String sing_str = new String(Base64.encodeBase64(signature.sign()));
            return bytesToHex(sign_byte);
        }catch (Exception ignore){
            return null;
        }

    }

    public static byte[] getParamsSort(Map params)
    {
        java.util.TreeMap<String, String> sortParas = new java.util.TreeMap<>();
        sortParas.putAll(params);
        java.util.Iterator<String> it = sortParas.keySet().iterator();
        StringBuilder encryptedStr = new StringBuilder();
        while (it.hasNext()) {
            String key = it.next();
            encryptedStr.append(key).append("=").append(params.get(key));
        }
        return encryptedStr.toString().getBytes();
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }
        return buf.toString();
    }

    public static boolean ksherVerify(Map data, String sign, String publicKey) throws Exception {
        boolean flag = false;
        byte[] dataByte = getParamsSort(data);

        //byte[] publicKeyBytes = Base64.decodeBase64(publicKey.getBytes());
        byte[] publicKeyBytes = android.util.Base64.decode(publicKey.getBytes(),android.util.Base64.DEFAULT );
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        KeyFactory factory = KeyFactory.getInstance("RSA");
        PublicKey key = factory.generatePublic(keySpec);
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initVerify(key);
        signature.update(dataByte);
        return signature.verify(unHexVerify(sign));
    }

    public static byte[] unHexVerify(String sign) {
        int length = sign.length();
        byte[] result = new byte[length / 2];
        for (int i = 0; i < length; i += 2)
            result[i / 2] = (byte) ((Character.digit(sign.charAt(i), 16) << 4) + Character.digit(sign.charAt(i + 1), 16));
        return result;
    }

}
