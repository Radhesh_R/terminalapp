package com.wps.terminalapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.wps.terminalapp.models.PrintModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import co.poynt.os.model.PrintedReceipt;
import co.poynt.os.model.PrintedReceiptLine;
import co.poynt.os.model.PrintedReceiptLineFont;
import co.poynt.os.model.PrintedReceiptSection;
import co.poynt.os.model.PrintedReceiptV2;

/**
 * Created by vignaraj.r on 05-Sep-18.
 */

public class PrintUtil {

    public static PrintUtil instance;
    private Context context;
    public static String TAG = PrintUtil.class.getSimpleName();

    public static PrintUtil getInstance(Context context) {
        if (instance == null) {
            instance = new PrintUtil(context);
        }
        return instance;
    }

    public PrintUtil(Context context) {
        this.context = context;
    }

    public void PrintNew(final String ipAddress, final int port, final List<PrintModel>... lists) {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Socket ipsocket = null;
                try {
                    ipsocket = new Socket();
                    InetSocketAddress socketAddressip = new InetSocketAddress(ipAddress, 9100);
                    int i = 0;
                    do {
                        try {
                            ipsocket = new Socket();
                            ipsocket.connect(socketAddressip, 2000);
                        } catch (Exception e) {
                            // e.printStackTrace();
                            synchronized (Thread.currentThread()) {
                                try {
                                    Thread.currentThread().wait(1000);
                                } catch (InterruptedException e1) {
                                    Log.e(TAG, e.getMessage());
                                }
                            }
                            //wait(1000);
                            //Thread.currentThread().wait(1000);
                            //Thread.sleep(1000);
                            ipsocket.close();
                            Log.e(TAG, ipAddress + e);
                        }
                        i += 1;
                    } while ((ipsocket.isConnected() == false) & i <= 4);
                    if (ipsocket.isConnected()) {
                        Log.e(TAG, ipAddress + " Connected");
                        writeIntoStreamNew(ipsocket, lists);
                        ipsocket.close();
                        Util.clearValues();
                    } else {
                        Log.e(TAG, ipAddress + " Connection failed");
                    }
                    ipsocket = null;
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "unable to close() socket during connection failure", e);
                } catch (ConnectException e) {
                    Util.showErrorToast(context, "Unable to connect!");
                } catch (UnknownHostException e) {
                    Log.e(TAG, "ConnectThread create() failed", e);
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                } finally {
                    if (ipsocket != null) {
                        try {
                            ipsocket.close();
                        } catch (IOException e2) {
                            Log.e(TAG, e2.getMessage());
                        }
                    }
                }
            }
        });
    }

    public void writeIntoStreamNew(Socket sc, List<PrintModel>... lists) {
        OutputStream osip = null;
        try {
            if (sc != null) {
                osip = sc.getOutputStream();
                byte[] printformat = new byte[]{0x1B, 0x21, 0x03};
                osip.write(printformat);
                for (int i = 0; i < lists.length; i++) {
                    initPrintNew(osip, lists[i]);
                }
                osip.write(PrinterCommands.FEED_PAPER_AND_CUT);
            }
        } catch (IOException e) {
            Log.e(TAG, "ConnectThread create() failed", e);
            Log.e(TAG, e.getMessage());
        } finally {
            if (osip != null) {
                try {
                    osip.flush();
                } catch (IOException e2) {
                    Log.e(TAG, e2.getMessage());
                }
            }
        }
    }

    public void initPrintNew(OutputStream osi, List<PrintModel> list) {
        Log.i("method", "initprint");
        int size = list.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                if (!list.get(i).getPrintData2().equals("")) {
                    if (list.get(i).printType1.equals("IMAGE")) {
                        String url = list.get(i).getPrintData1();
                        String filename = url.substring(url.lastIndexOf('/') + 1);
                        if (checkFileExists(filename)) {
                            Bitmap bmp = loadImageBitmap(context, filename);
                            printPhotoNew(osi, bmp);
                        } else {
                            downloadImageNew(osi, url);
                        }
                    } else {
                        printCustomNew(osi, list.get(i).getPrintData1(), list.get(i).getPrintFont1(), list.get(i).getPrintAlign1(), 1);
                        printCustomNew(osi, list.get(i).getPrintData2(), list.get(i).getPrintFont2(), list.get(i).getPrintAlign2(), 0);
                    }
                } else {
                    if (list.get(i).printType1.equals("IMAGE")) {
                        String url = list.get(i).getPrintData1();
                        String filename = url.substring(url.lastIndexOf('/') + 1);
                        if (checkFileExists(filename)) {
                            Bitmap bmp = loadImageBitmap(context, filename);
                            printPhotoNew(osi, bmp);
                        } else {
                            downloadImageNew(osi, url);
                        }
                    } else {
                        printCustomNew(osi, list.get(i).getPrintData1(), list.get(i).getPrintFont1(), list.get(i).getPrintAlign1(), 0);
                    }
                }
            }
        }
    }

    private void printCustomNew(OutputStream osi, String msg, int size, int align, int opt) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    osi.write(cc);
                    break;
                case 1:
                    osi.write(bb);
                    break;
                case 2:
                    osi.write(bb2);
                    break;
                case 3:
                    osi.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    osi.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    osi.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    osi.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
//            Arabic864 arabic = new Arabic864();
//            byte[] arabicArr = arabic.Convert("للغة العربية", false);
//            outputStream.write(arabicArr);
            osi.write(msg.getBytes());
            if (opt == 0) {
                osi.write(PrinterCommands.LF);
            }
//            outputStream.write(PrinterCommands.FEED_PAPER_AND_CUT);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public Bitmap loadImageBitmap(Context context, String imageName) {
        Bitmap bitmap = null;
        FileInputStream fiStream;
        try {
            fiStream = context.openFileInput(imageName);
            bitmap = BitmapFactory.decodeStream(fiStream);
            fiStream.close();
        } catch (Exception e) {
            Log.d("saveImage", "Exception 3, Something went wrong!");
        }
        return bitmap;
    }

    public void downloadImageNew(final OutputStream osi, final String url) {
        new AsyncTask<String, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(String... params) {
                return downloadImageBitmap(params[0]);
            }

            protected void onPostExecute(Bitmap result) {
                final String filename = url.substring(url.lastIndexOf('/') + 1);
                saveImage(context.getApplicationContext(), result, filename);
                printPhotoNew(osi, result);
               /* Handler handler = new Handler(Looper.myLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bmp = loadImageBitmap(context, filename);
                        printPhotoNew(osi,bmp);
                    }
                }, 2000);*/
            }
        }.execute(url);
    }

    public void saveImage(Context context, Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
            foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();
        } catch (Exception e) {
            Log.d("saveImage", "Exception 2, Something went wrong!");
        }
    }

    private Bitmap downloadImageBitmap(String sUrl) {
        Bitmap bitmap = null;
        try {
            InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
            bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
            inputStream.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception 1, Something went wrong!");
        }
        return bitmap;
    }

    public void printPhotoNew(OutputStream osi, Bitmap bmp) {
        Log.i("method", "printPhoto");
        try {
            if (bmp != null) {
                byte[] command = Util.decodeBitmap(bmp);
                osi.write(PrinterCommands.ESC_ALIGN_CENTER);
                printTextNew(osi, command);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    private void printTextNew(OutputStream osi, byte[] msg) {
        try {
            // Print normal text
            osi.write(msg);
            printNewLineNew(osi);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    //print new line
    private void printNewLineNew(OutputStream osi) {
        try {
            osi.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public boolean checkFileExists(String imageName) {
        boolean exist = false;
        File file = context.getApplicationContext().getFileStreamPath(imageName);
        if (file.exists()) {
            exist = true;
        }
        return exist;
    }

    public PrintedReceipt generateReceipt(JSONArray headerArray, JSONArray bodyArray, JSONArray footerArray, String cardType) {
        Log.i("method", "generateReceipt");
        PrintedReceipt receipt = new PrintedReceipt();
        List<PrintModel> header = new ArrayList<>();
        List<PrintModel> body = new ArrayList<>();
        List<PrintModel> footer = new ArrayList<>();
        try {
            //header
            if (headerArray != null) {
                for (int i = 0; i < headerArray.length(); i++) {
                    JSONObject obj = headerArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        header.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }
            //body
            if (bodyArray != null) {
                for (int i = 0; i < bodyArray.length(); i++) {
                    JSONObject obj = bodyArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        body.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }
            //footer
            if (footerArray != null) {
                for (int i = 0; i < footerArray.length(); i++) {
                    JSONObject obj = footerArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        footer.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        List<PrintedReceiptLine> headerLines = new ArrayList<>();
        int headerLength = header.size();
        if (headerLength > 0) {
            for (int i = 0; i < headerLength; i++) {
                headerLines.add(new PrintedReceiptLine(header.get(i).getPrintData1()));
            }
        }

        //Body
        List<PrintedReceiptLine> bodyLines = new ArrayList<>();
        int bodyLength = body.size();
        if (bodyLength > 0) {
            for (int i = 0; i < bodyLength; i++) {
                bodyLines.add(new PrintedReceiptLine(body.get(i).getPrintData1()));
            }
        }
        //Footer
        List<PrintedReceiptLine> footerLines = new ArrayList<>();
        int footerLength = footer.size();
        if (footerLength > 0) {
            for (int i = 0; i < footerLength; i++) {
                footerLines.add(new PrintedReceiptLine(footer.get(i).getPrintData1()));
            }
        }
        //

//        if (cardType != null && !cardType.equals("CASH")) {
//            Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
//                    R.drawable.mas_100);
//            receipt.setHeaderImage(bm);
//        }

        receipt.setHeader(headerLines);
        receipt.setBody(bodyLines);
        receipt.setFooter(footerLines);

        return receipt;
    }

    public PrintedReceiptV2 generateReceiptV2(JSONArray headerArray, JSONArray bodyArray, JSONArray footerArray) {
        PrintedReceiptV2 receipt = new PrintedReceiptV2();
        List<PrintModel> header = new ArrayList<>();
        List<PrintModel> body = new ArrayList<>();
        List<PrintModel> footer = new ArrayList<>();
        try {
            //header
            if (headerArray != null) {
                for (int i = 0; i < headerArray.length(); i++) {
                    JSONObject obj = headerArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        header.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }
            //body
            if (bodyArray != null) {
                for (int i = 0; i < bodyArray.length(); i++) {
                    JSONObject obj = bodyArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        body.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }
            //footer
            if (footerArray != null) {
                for (int i = 0; i < footerArray.length(); i++) {
                    JSONObject obj = footerArray.getJSONObject(i);
                    String printType1 = obj.getString("printType1");
                    String printData1 = obj.getString("printData1");
                    int printAlign1 = obj.getInt("printAlign1");
                    if (printType1.equals("TEXT")) {
                        footer.add(new PrintModel(printData1, printAlign1));
                    }
                }
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        List<PrintedReceiptLine> headerLines = new ArrayList<>();
        int headerLength = header.size();
        if (header != null && headerLength > 0) {
            for (int i = 0; i < headerLength; i++) {
                headerLines.add(new PrintedReceiptLine(header.get(i).getPrintData1()));
            }
        }
        PrintedReceiptLineFont headerFont = new PrintedReceiptLineFont(PrintedReceiptLineFont.FONT_SIZE.FONT24, 22);
        PrintedReceiptSection headerSection = new PrintedReceiptSection(headerLines, headerFont);
        //Body
        List<PrintedReceiptLine> bodyLines = new ArrayList<>();
        int bodyLength = body.size();
        if (body != null && bodyLength > 0) {
            for (int i = 0; i < bodyLength; i++) {
                bodyLines.add(new PrintedReceiptLine(body.get(i).getPrintData1()));
            }
        }
        PrintedReceiptLineFont bodyFont = new PrintedReceiptLineFont(PrintedReceiptLineFont.FONT_SIZE.FONT24, 20);
        PrintedReceiptSection bodySection = new PrintedReceiptSection(bodyLines, bodyFont);
        //Footer
        List<PrintedReceiptLine> footerLines = new ArrayList<>();
        int footerLength = footer.size();
        if (footer != null && footerLength > 0) {
            for (int i = 0; i < footerLength; i++) {
                footerLines.add(new PrintedReceiptLine(footer.get(i).getPrintData1()));
            }
        }
        PrintedReceiptLineFont footerFont = new PrintedReceiptLineFont(PrintedReceiptLineFont.FONT_SIZE.FONT15, 18);
        PrintedReceiptSection footerSection = new PrintedReceiptSection(footerLines, footerFont);
        //
        receipt.setHeader(headerSection);
        receipt.setBody1(bodySection);
        receipt.setFooter(footerSection);
        return receipt;
    }
}
