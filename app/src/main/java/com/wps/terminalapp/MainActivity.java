package com.wps.terminalapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.StrictMode;
import androidx.annotation.NonNull;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.wps.terminalapp.adapters.CustomDrawerAdapter;
import com.wps.terminalapp.adapters.PopAdapter;
import com.wps.terminalapp.api.APIBuilder;
import com.wps.terminalapp.api.APIComponent;
import com.wps.terminalapp.custom.ButtonSansBold;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.dialogs.PinDialog;
import com.wps.terminalapp.history.SearchFragment;
import com.wps.terminalapp.listeners.PinListener;
import com.wps.terminalapp.listeners.PopSelectionListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.models.DrawerItem;
import com.wps.terminalapp.models.ErrorLogResponse;
import com.wps.terminalapp.models.LoGErr;
import com.wps.terminalapp.models.PrintModel;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.settings.AppPreferences;
import com.wps.terminalapp.settings.SettingsFragment;
import com.wps.terminalapp.settle.SettleFragment;
import com.wps.terminalapp.terminal.TerminalFragment;
import com.wps.terminalapp.transactions.TransactionFragment;
import com.wps.terminalapp.transactions.ViewTransaction;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.PrintUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import co.poynt.api.model.Business;
import co.poynt.os.Constants;
import co.poynt.os.model.AccessoryProvider;
import co.poynt.os.model.AccessoryProviderFilter;
import co.poynt.os.model.AccessoryType;
import co.poynt.os.model.Intents;
import co.poynt.os.model.PoyntError;
import co.poynt.os.model.PrintedReceipt;
import co.poynt.os.model.PrintedReceiptV2;
import co.poynt.os.model.PrinterStatus;
import co.poynt.os.printing.ReceiptPrintingPref;
import co.poynt.os.services.v1.IPoyntAccessoryManagerListener;
import co.poynt.os.services.v1.IPoyntBusinessReadListener;
import co.poynt.os.services.v1.IPoyntBusinessService;
import co.poynt.os.services.v1.IPoyntPrinterService;
import co.poynt.os.services.v1.IPoyntPrinterServiceListener;
import co.poynt.os.services.v1.IPoyntSecondScreenService;
import co.poynt.os.services.v1.IPoyntTransactionService;
import co.poynt.os.util.AccessoryProviderServiceHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.wps.terminalapp.utils.Util.dTag;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        TransactionFragment.TransactionListener, SettleFragment.SettleListener,
        SearchFragment.SearchListener, TerminalFragment.TerminalListener,
        ViewTransaction.ViewTransactionListener {

    final static int REQUEST_LOCATION = 199;

    Context context;

    //Variables
    CustomDrawerAdapter drawerAdapter;
    List<DrawerItem> dataList;

    //UI
    @BindView(R.id.list_drawer)
    ListView mDrawerList;
    @BindView(R.id.txt_versname)
    TextSansRegular txtVersionName;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.txt_title)
    TextSansBold txt_title;
    @BindView(R.id.layout_search)
    RelativeLayout layout_search;
    @BindView(R.id.btn_search)
    TextSansBold btn_search;
    @BindView(R.id.btn_dateview)
    ButtonSansBold btn_showCalendar;
    @BindView(R.id.layout_viewtrans)
    RelativeLayout layoutViewTrans;
    @BindView(R.id.txt_transid)
    TextSansRegular txt_transId;
    @BindView(R.id.layout_settletrans)
    RelativeLayout layoutSettleTrans;
    @BindView(R.id.layout_ftrans)
    RelativeLayout layoutFtrans;
    @BindView(R.id.btn_refresh)
    ImageView btn_refresh;

    String currentTransactionId = "";

    private static FirebaseAnalytics mFirebaseAnalytics;
    private APIComponent retrofitPrint;

    Fragment fragment = null;
    //Services
    public static IPoyntTransactionService transactionService;
    public static boolean isTransactionServiceConnected = false;

    public static IPoyntBusinessService businessService;
    public static boolean isBusinessServiceConnected = false;

    public static IPoyntSecondScreenService poyntSecondScreenService;
    public static boolean isSecondScreenServiceConnected = false;

    public static co.poynt.os.services.v2.IPoyntSecondScreenService poyntSecondScreenServiceV2;
    public static boolean isSecondScreenServiceConnectedv2 = false;

    LoGErr loGErr = new LoGErr();

    private ServiceConnection transactionServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            transactionService = IPoyntTransactionService.Stub.asInterface(iBinder);
            isTransactionServiceConnected = true;
        }

        public void onServiceDisconnected(ComponentName componentName) {
            transactionService = null;
        }
    };

    /*Class for interacting with the business service*/

    public static ServiceConnection bizServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            businessService = IPoyntBusinessService.Stub.asInterface(service);
            isBusinessServiceConnected = true;
            Log.d("businessService#", "Connected ");
            try {
                Log.i("bus_service", "inside");
                businessService.getBusiness(bizListener);
            } catch (RemoteException e) {
                Log.e(TAG, "unable to retrieve the business service");
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i("BusinessService", "Disconnected");
            isBusinessServiceConnected = false;
            Log.d("businessService#", "Disonnected ");
        }
    };

    //business listener
    public static IPoyntBusinessReadListener bizListener = new IPoyntBusinessReadListener.Stub() {
        @Override
        public void onResponse(final Business business, PoyntError poyntError) throws RemoteException {
            if (poyntError == null) {
//                Util.logLargeString("business##", new Gson().toJson(business));
                if (business != null) {
                    Map<String, String> pData = business.getStores().get(0).getAttributes();
                    if (pData.containsKey("amexSENumber")) {
                        String mid = pData.get("amexSENumber");
                        if (!mid.equals("")) {
                            Util.isAmexExist = true;
                        }
                    }

                    if (pData.containsKey("paymentOptionsImageUrl")) {
                        String supportPayment = pData.get("paymentOptionsImageUrl");
                        if (!supportPayment.equals("")) {
                            Util.supportedPayment = supportPayment;
                        }
                    }

                    if (business.getAcquirer() != null) {
                        Log.d("not_null", "acquirer");
                        Util.currentAcquirer = business.getAcquirer().name();
                        Log.d("acq_name", business.getAcquirer().name());
                    }

                }
            }
        }
    };

    /**
     * SecondScreen service
     **/
    public static ServiceConnection secondScreenServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {
            poyntSecondScreenService = IPoyntSecondScreenService.Stub.asInterface(iBinder);
            isSecondScreenServiceConnected = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            poyntSecondScreenService = null;
        }
    };

    /**
     * Second screen service v2
     */
    public static ServiceConnection secondScreenServiceConnectionV2 = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            poyntSecondScreenServiceV2 = co.poynt.os.services.v2.IPoyntSecondScreenService.Stub.asInterface(service);
            isSecondScreenServiceConnectedv2 = true;
            Log.i("second_screen_v2", "connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            poyntSecondScreenServiceV2 = null;
            Log.i("second_screen_v2", "disconnected");
        }
    };


    //
    private AccessoryProviderServiceHelper accessoryProviderServiceHelper;
    private HashMap<AccessoryProvider, IBinder> mPrinterServices = new HashMap<>();
    private List<AccessoryProvider> providers;
    private Set<String> printers;

    // this is the accessory manager listener which gets invoked when accessory manager completes
    // scanning for the requested accessories
    private IPoyntAccessoryManagerListener poyntAccessoryManagerListener
            = new IPoyntAccessoryManagerListener.Stub() {

        @Override
        public void onError(PoyntError poyntError) throws RemoteException {
            Log.e(TAG, "Failed to connect to accessory manager: " + poyntError);
        }

        @Override
        public void onSuccess(final List<AccessoryProvider> printers) throws RemoteException {
            // now that we are connected - request service connections to each accessory provider
            if (printers != null && printers.size() > 0) {
                // save it for future reference
                providers = printers;
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    // for each printer accessory - request "service" connections if it's still connected
                    for (AccessoryProvider printer : printers) {
                        Log.d(TAG, "Printer: " + printer.toString());
                        if (printer.isConnected()) {
                            // request service connection binder
                            // IMP: note that this method returns service connection if it already exists
                            // hence the need for both connection callback and the returned value
                            IBinder binder = accessoryProviderServiceHelper.getAccessoryService(
                                    printer, AccessoryType.PRINTER,
                                    providerConnectionCallback);
                            //already cached connection.
                            if (binder != null) {
                                mPrinterServices.put(printer, binder);
                            }
                        }
                    }
                }
            } else {
                Log.e(TAG, "No Printers found");
            }
        }
    };

    // this is the callback for the service connection to each accessory provider service in this case
    // the android service supporting  printer accessory
    private AccessoryProviderServiceHelper.ProviderConnectionCallback providerConnectionCallback
            = new AccessoryProviderServiceHelper.ProviderConnectionCallback() {

        @Override
        public void onConnected(AccessoryProvider provider, IBinder binder) {
            // in some cases multiple accessories of the same type (eg. two printers of same
            // make/model or two star printers) might be supported by the same android service
            // so here we check if we need to share the same service connection for more than
            // one accessory provider
            List<AccessoryProvider> otherProviders = findMatchingProviders(provider);
            // all of them share the same service binder
            for (AccessoryProvider matchedProvider : otherProviders) {
                mPrinterServices.put(matchedProvider, binder);
            }
        }

        @Override
        public void onDisconnected(AccessoryProvider provider, IBinder binder) {
            // set the lookup done to false so we can try looking up again if needed
            if (mPrinterServices != null && mPrinterServices.size() > 0) {
                mPrinterServices.remove(binder);
                // try to renew the connection.
                if (accessoryProviderServiceHelper.getAccessoryServiceManager() != null) {
                    IBinder binder2 = accessoryProviderServiceHelper.getAccessoryService(
                            provider, AccessoryType.PRINTER,
                            providerConnectionCallback);
                    if (binder2 != null) {//already cached connection.
                        mPrinterServices.put(provider, binder2);
                    }
                }
            }
        }
    };

    // we do this if there are multiple accessories connected of the same type/provider
    private List<AccessoryProvider> findMatchingProviders(AccessoryProvider provider) {
        ArrayList<AccessoryProvider> matchedProviders = new ArrayList<>();
        if (providers != null) {
            for (AccessoryProvider printer : providers) {
                if (provider.getAccessoryType() == printer.getAccessoryType()
                        && provider.getPackageName().equals(printer.getPackageName())
                        && provider.getClassName().equals(printer.getClassName())) {
                    matchedProviders.add(printer);
                }
            }
        }
        return matchedProviders;
    }
    //


    public static String TAG = MainActivity.class.getSimpleName();
    ActionBarDrawerToggle toggle;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    int year, month, dayOfMonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        Stetho.initializeWithDefaults(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ButterKnife.bind(this);
        RelativeLayout mParent = (RelativeLayout) navigationView.getHeaderView(0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        boolean notification = getIntent().getBooleanExtra("notification", false);
        dataList = new ArrayList<>();
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        String versionName = "VER " + BuildConfig.VERSION_NAME;
        String buildTime = Util.getDateString(BuildConfig.TIMESTAMP, "dd-MMM-yyyy");
        String finalBuild = versionName + "\n\n" + buildTime;
        txtVersionName.setText(finalBuild);
        loadAdapter();
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        callBusinessService();
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        initializeAccessories();

        displayFragment(R.id.menu_terminal);
        if (notification) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String type = getIntent().getStringExtra("type");
                    String notes = getIntent().getStringExtra("notes");
                    long amount = getIntent().getLongExtra("amount", 0);
                    String orderId = getIntent().getStringExtra("order_id");
                    String callBackUrl = getIntent().getStringExtra("callback_url");
                    showTerminalFragment(amount, type, notes, orderId, callBackUrl);
//                    ((TerminalFragment) getSupportFragmentManager().findFragmentByTag("TerminalFragment")).firePaymentFromNotificaction(amount, type, notes, orderId, callBackUrl);
                }
            });
        }
        APIBuilder apiBuilder = new APIBuilder(getApplicationContext());
        Retrofit mRetrofitPrint = apiBuilder.retrofitPrint();
        retrofitPrint = mRetrofitPrint.create(APIComponent.class);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //checkGpsStatus();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(Intents.getComponentIntent(Intents.COMPONENT_POYNT_BUSINESS_SERVICE),
                bizServiceConnection, BIND_AUTO_CREATE);
        bindService(Intents.getComponentIntent(Intents.COMPONENT_POYNT_TRANSACTION_SERVICE),
                transactionServiceConnection, BIND_AUTO_CREATE);
        bindService(Intents.getComponentIntent(Intents.COMPONENT_POYNT_SECOND_SCREEN_SERVICE),
                secondScreenServiceConnection, BIND_AUTO_CREATE);
        bindService(Intents.getComponentIntent(Intents.COMPONENT_POYNT_SECOND_SCREEN_SERVICE_V2),
                secondScreenServiceConnectionV2, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        Util.freeMemory();
        super.onPause();
        unbindService(secondScreenServiceConnection);
        unbindService(secondScreenServiceConnectionV2);
    }

    @Override
    protected void onStop() {
        Util.freeMemory();
        super.onStop();
        unbindService(bizServiceConnection);
        unbindService(transactionServiceConnection);
        finishAffinity();
        System.exit(0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        displayFragment(item.getItemId());
        drawer.closeDrawers();
        return true;
    }

    @OnClick(R.id.btn_dateview)
    public void showCalendar() {
        dTag(1, TAG, "showCalendar");
        final PopupWindow popWindow = new PopupWindow(context);
        popWindow.setFocusable(true);
        popWindow.setWidth(280);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        ArrayList<String> menuList = new ArrayList<>();
        menuList.add("TODAY");
        menuList.add("YESTERDAY");
        menuList.add("THIS WEEK");
        menuList.add("LAST WEEK");
        menuList.add("SELECT DATE");
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = inflater.inflate(R.layout.pop_dateselect, null);
        popWindow.setContentView(v);
        popWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ListView list_menuItems = (ListView) v.findViewById(R.id.list_date);
        list_menuItems.setAdapter(new PopAdapter(context, menuList, new PopSelectionListener() {
            @Override
            public void selectedValue(String value) {
                popWindow.dismiss();
                ((SearchFragment) getSupportFragmentManager().findFragmentByTag("SearchFragment")).selectedValue(value);
            }
        }));
        popWindow.showAsDropDown(btn_showCalendar, 0, 0);
    }


    public void loadAdapter() {
        dTag(0, TAG, "loadAdapter");
        Log.d(TAG, "function - loadAdapter");
        dataList.add(new DrawerItem("TERMINAL", R.drawable.terminal_cg));
        dataList.add(new DrawerItem("TRANSACTIONS", R.drawable.transactions_cg));
//        dataList.add(new DrawerItem("CASHBOX", R.drawable.cashbox_32));
        dataList.add(new DrawerItem("SETTINGS", R.drawable.settings_32));
        dataList.add(new DrawerItem("DETAILED REPORT", R.drawable.report_32));
        dataList.add(new DrawerItem("SUMMARY REPORT", R.drawable.report_32));
        drawerAdapter = new CustomDrawerAdapter(this, R.layout.drawer_listitem, dataList);
        mDrawerList.setAdapter(drawerAdapter);
    }

    public void displayFragment(final int id) {
        dTag(0, TAG, "displayFragment");
        Util.freeMemory();
        Util.freeMemory();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Util.freeMemory();
                setupDrawer();
                Fragment fragment = null;
                String fragmentName = "";
                switch (id) {
                    case R.id.menu_terminal:
                        fragment = new TerminalFragment();
                        fragmentName = "TerminalFragment";
                        txt_title.setText("TERMINAL");
                        layoutFtrans.setVisibility(View.GONE);
//                        layout_search.setVisibility(View.GONE);
                        Util.freeMemory();
                        break;
                    case R.id.menu_transaction:
                        fragment = new TransactionFragment();
                        fragmentName = "TransactionFragment";
                        txt_title.setText("TRANSACTIONS");
                        layoutFtrans.setVisibility(View.VISIBLE);
                        Util.freeMemory();
                        break;
                    case R.id.menu_settings:
                        fragment = new SettingsFragment();
                        fragmentName = "SettingsFragment";
                        txt_title.setText("SETTINGS");
                        layoutFtrans.setVisibility(View.GONE);
                        Util.freeMemory();
                        break;
                }
                txt_title.setVisibility(View.VISIBLE);
                if (fragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_navigation, fragment, fragmentName);
                    ft.addToBackStack(null);
//                    ft.commit();
                    ft.commitAllowingStateLoss();
                }
            }
        });
    }


    @OnClick(R.id.btn_refresh)
    public void doRefresh() {
        Util.freeMemory();
        dTag(1, TAG, "doRefresh");
//        ((TransactionFragment) getSupportFragmentManager().findFragmentByTag("ViewTransaction")).showPoyntFragment();
        ((TransactionFragment) getSupportFragmentManager().findFragmentByTag("TransactionFragment")).refreshAll();
    }

    @Override
    public void goHistory() {
        dTag(1, TAG, "goHistory");
        Util.freeMemory();
        showSearchView();
    }

    @Override
    public void viewTransaction(TransactionModel model) {
        Util.freeMemory();
        dTag(1, TAG, "viewTransaction");
        Util.isPopBack = true;
        String transactionStatus = model.getmTransactionStatus();
        currentTransactionId = model.getmTransactionId();
        if (transactionStatus.equals("AUTHORIZED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.auth_blue)));
            txt_transId.setEnabled(true);
        } else if (transactionStatus.equals("CAPTURED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.green_success)));
            txt_transId.setEnabled(false);
        } else if (transactionStatus.equals("DECLINED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.dec_red)));
            txt_transId.setEnabled(false);
        } else if (transactionStatus.equals("VOIDED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.void_grey)));
            txt_transId.setEnabled(false);
        }

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("backPressed", "viewTransaction");
                    Drawable drawable = ContextCompat.getDrawable(MainActivity.this, R.drawable.grad_terminalbg);
                    getSupportActionBar().setBackgroundDrawable(drawable);
                    layoutViewTrans.setVisibility(View.GONE);
                    displayFragment(R.id.menu_transaction);
                }
            });
            mToolBarNavigationListenerIsRegistered = true;
        }
        txt_title.setVisibility(View.GONE);
        layout_search.setVisibility(View.GONE);
        layoutFtrans.setVisibility(View.GONE);
        layoutViewTrans.setVisibility(View.VISIBLE);
        txt_transId.setText("#" + model.getmTransactionId().substring(0, 8).toUpperCase());

        Fragment fragment = new ViewTransaction(model);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_navigation, fragment, "ViewTransaction");
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void settleTransaction() {

        dTag(1, TAG, "settleTransaction");
        String pinForSettle = AppPreferences.getInstance().getSettleValue(MainActivity.this);
        List<UserData> data = AppPreferences.getInstance().getUsersList(MainActivity.this);

        if (pinForSettle.toLowerCase().equals("y")) {
            PinDialog pinDialog = new PinDialog(MainActivity.this, 1, data, new PinListener() {
                @Override
                public void isValidUser(String pin, boolean valid) {
                    if (valid) {
                        showSettleTransaction();
                    }
                }
            });
            pinDialog.show();
        } else {
            showSettleTransaction();
        }
    }

    public void showSettleTransaction() {
        Util.freeMemory();
        Util.isPopBack = true;
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayFragment(R.id.menu_transaction);
                }
            });
            mToolBarNavigationListenerIsRegistered = true;
        }
        txt_title.setVisibility(View.GONE);
        layout_search.setVisibility(View.GONE);
        layoutFtrans.setVisibility(View.GONE);
        layoutSettleTrans.setVisibility(View.VISIBLE);
        Fragment fragment = new SettleFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_navigation, fragment, "SettleFragment");
        ft.addToBackStack(null);
        ft.commit();
    }

    @OnClick(R.id.btn_home)
    public void goTerminal() {
        Util.freeMemory();
        dTag(1, TAG, "goTerminal");
        layoutSettleTrans.setVisibility(View.GONE);
        Util.transactionList.clear();
        displayFragment(R.id.menu_terminal);
    }

    @Override
    public void goBackTransaction() {
        Util.freeMemory();
        dTag(1, TAG, "goBackTransaction");
        layoutSettleTrans.setVisibility(View.GONE);
        displayFragment(R.id.menu_transaction);
    }

    @Override
    public void viewSearchedTransaction(TransactionModel model) {
        Util.freeMemory();
        dTag(1, TAG, "viewSearchedTransaction");
        String transactionStatus = model.getmTransactionStatus();
        currentTransactionId = model.getmTransactionId();

        if (transactionStatus.equals("AUTHORIZED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.auth_blue)));
        } else if (transactionStatus.equals("CAPTURED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.green_success)));
        } else if (transactionStatus.equals("DECLINED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.dec_red)));
        } else if (transactionStatus.equals("VOIDED")) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.void_grey)));
        }
        txt_transId.setEnabled(false);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (mToolBarNavigationListenerIsRegistered) {
            mToolBarNavigationListenerIsRegistered = false;
        }

        Log.i("registered", String.valueOf(mToolBarNavigationListenerIsRegistered));

        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("onBackPressed", "viewSearchedTransaction");
                    Drawable drawable = ContextCompat.getDrawable(MainActivity.this, R.drawable.grad_terminalbg);
                    getSupportActionBar().setBackgroundDrawable(drawable);
                    layoutViewTrans.setVisibility(View.GONE);
                    showSearchView();
                }
            });
            mToolBarNavigationListenerIsRegistered = true;
        }
        txt_title.setVisibility(View.GONE);
        layout_search.setVisibility(View.GONE);
        layoutFtrans.setVisibility(View.GONE);
        layoutViewTrans.setVisibility(View.VISIBLE);
        txt_transId.setText("#" + model.getmTransactionId().substring(0, 8).toUpperCase());

        Fragment fragment = new ViewTransaction(model);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_navigation, fragment, "ViewTransaction");
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void showSuccessTransaction(final TransactionModel model) {
        Util.freeMemory();
        dTag(1, TAG, "showSuccessTransaction");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String transactionStatus = model.getmTransactionStatus();
                currentTransactionId = model.getmTransactionId();
                if (transactionStatus.equals("AUTHORIZED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.auth_blue)));
                    txt_transId.setEnabled(true);
                } else if (transactionStatus.equals("CAPTURED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.green_success)));
                    txt_transId.setEnabled(false);
                } else if (transactionStatus.equals("DECLINED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.dec_red)));
                    txt_transId.setEnabled(false);
                } else if (transactionStatus.equals("VOIDED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.void_grey)));
                    txt_transId.setEnabled(false);
                }

                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                toggle.setDrawerIndicatorEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                if (!mToolBarNavigationListenerIsRegistered) {
                    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.i("backPressed", "viewTransaction");
                            Drawable drawable = ContextCompat.getDrawable(MainActivity.this, R.drawable.grad_terminalbg);
                            getSupportActionBar().setBackgroundDrawable(drawable);
                            layoutViewTrans.setVisibility(View.GONE);
                            displayFragment(R.id.menu_terminal);
                        }
                    });
                    mToolBarNavigationListenerIsRegistered = true;
                }
                txt_title.setVisibility(View.GONE);
                layout_search.setVisibility(View.GONE);
                layoutFtrans.setVisibility(View.GONE);
                layoutViewTrans.setVisibility(View.VISIBLE);
                txt_transId.setText("#" + model.getmTransactionId().substring(0, 8).toUpperCase());
                Fragment fragment = new ViewTransaction(model);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_navigation, fragment, "ViewTransaction");
                ft.addToBackStack(null);
                ft.commit();

                if ("AUTHORIZED".equals(transactionStatus) || "CAPTURED".equals(transactionStatus)) {

                }
            }
        });

    }

    @Override
    public void reloadTransaction(final TransactionModel model) {
        dTag(1, TAG, "reloadTransaction");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String transactionStatus = model.getmTransactionStatus();
                currentTransactionId = model.getmTransactionId();
                if (transactionStatus.equals("AUTHORIZED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.auth_blue)));
                    txt_transId.setEnabled(true);
                } else if (transactionStatus.equals("CAPTURED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.green_success)));
                    txt_transId.setEnabled(false);
                } else if (transactionStatus.equals("DECLINED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.dec_red)));
                    txt_transId.setEnabled(false);
                } else if (transactionStatus.equals("VOIDED")) {
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(MainActivity.this, R.color.void_grey)));
                    txt_transId.setEnabled(false);
                }

                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                toggle.setDrawerIndicatorEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                if (!mToolBarNavigationListenerIsRegistered) {
                    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.i("backPressed", "viewTransaction");
                            Drawable drawable = ContextCompat.getDrawable(MainActivity.this, R.drawable.grad_terminalbg);
                            getSupportActionBar().setBackgroundDrawable(drawable);
                            layoutViewTrans.setVisibility(View.GONE);
                            displayFragment(R.id.menu_terminal);
                        }
                    });
                    mToolBarNavigationListenerIsRegistered = true;
                }
                txt_title.setVisibility(View.GONE);
                layout_search.setVisibility(View.GONE);
                layoutFtrans.setVisibility(View.GONE);
                layoutViewTrans.setVisibility(View.VISIBLE);
                txt_transId.setText("#" + model.getmTransactionId().substring(0, 8).toUpperCase());
                Fragment fragment = new ViewTransaction(model);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_navigation, fragment, "ViewTransaction");
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position,
                                long id) {
            Handler handler = new Handler();
            handler.postDelayed(() -> drawer.closeDrawers(), 20);
            handler.postAtFrontOfQueue(() -> {
                selectItem(dataList.get(position).getItemName(), position);
                Util.freeMemory();
            });
        }
    }

    public void selectItem(String menuItem, int position) {
        String fragmentName = "";
        setupDrawer();
        switch (menuItem) {
            case "TERMINAL":
                fragment = new TerminalFragment();
                fragmentName = "TerminalFragment";
                mDrawerList.setItemChecked(position, true);
                txt_title.setText("TERMINAL");
                layoutFtrans.setVisibility(View.GONE);
//               layout_search.setVisibility(View.GONE);
                break;
            case "TRANSACTIONS":
                fragment = new TransactionFragment();
                fragmentName = "TransactionFragment";
                mDrawerList.setItemChecked(position, true);
                txt_title.setText("TRANSACTIONS");
                layoutFtrans.setVisibility(View.VISIBLE);
//               layout_search.setVisibility(View.VISIBLE);
                break;
            case "SETTINGS":
                fragment = new SettingsFragment();
                fragmentName = "SettingsFragment";
                mDrawerList.setItemChecked(position, true);
                txt_title.setText("SETTINGS");
                layoutFtrans.setVisibility(View.GONE);
                break;
        }


        txt_title.setVisibility(View.VISIBLE);

        if (menuItem.equals("DETAILED REPORT")) {
            DetailReport();
            return;
        }

        if (menuItem.equals("SUMMARY REPORT")) {
            SummaryReport();
            return;
        }


        try {
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_navigation, fragment, fragmentName);
                ft.addToBackStack(null);
                ft.commit();
            }
        }catch (Exception ignore){}

    }

    public void showSearchView() {
        Util.freeMemory();
        dTag(1, TAG, "showSearchView");
        Util.isPopBack = true;
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (mToolBarNavigationListenerIsRegistered) {
            mToolBarNavigationListenerIsRegistered = false;
        }

        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayFragment(R.id.menu_transaction);
                    Util.historyList.clear();
                    Util.lastSelectedFilter = "ALL";
                    Util.currentFilter = "ALL";
                }
            });
            mToolBarNavigationListenerIsRegistered = true;
        }
        txt_title.setVisibility(View.GONE);
        layoutFtrans.setVisibility(View.GONE);
        layout_search.setVisibility(View.VISIBLE);
        Fragment fragment = new SearchFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_navigation, fragment, "SearchFragment");
        ft.addToBackStack(null);
        ft.commit();
    }

    public void setupDrawer() {
        layout_search.setVisibility(View.GONE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.setToolbarNavigationClickListener(null);
        mToolBarNavigationListenerIsRegistered = false;
    }

    public void callBusinessService() {
        dTag(0, TAG, "callBusinessService");
        if (isBusinessServiceConnected) {
            try {
                Log.i("bus_service", "inside");
                businessService.getBusiness(bizListener);
            } catch (RemoteException e) {
                Log.e(TAG, "unable to retreive the business service");
            }
        }
    }

    public void setTransactionTag(String str){
        txt_transId.setTag(str);
    }

    @OnTouch(R.id.txt_transid)
    public boolean openFragment(View v, MotionEvent event) {
        return gd.onTouchEvent(event);
    }

    GestureDetector gd = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            try {
                String transactionType = (String) txt_transId.getTag();
                if(transactionType != null && transactionType.equalsIgnoreCase("Alipay"))
                    ((ViewTransaction) getSupportFragmentManager().findFragmentByTag("ViewTransaction")).alipayPaymentDetailsAPI();
                else
                    ((ViewTransaction) getSupportFragmentManager().findFragmentByTag("ViewTransaction")).showPoyntFragment();
            }catch (Exception er){
                er.printStackTrace();
                Util.showErrorToast(MainActivity.this, "Something Went Wrong");
                Crashlytics.logException(er);
                sendErrorToServer(getApplicationContext(), er);
                loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                loGErr.setErrMsg(er.getMessage());
                retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                    @Override
                    public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                        if (response.body() != null) {
                            if (response.body().getSuccess()) {
                                if (response.body().getResponseData().equals("OK")) {
                                    Log.e("Error send", "OK");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                        Log.e("Error send", "Failed");
                    }
                });
            }
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);

        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    });

    public void showTerminalFragment(long amount, String... vals) {
        Util.freeMemory();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        args.putBoolean("notification", true);
        args.putLong("amount", amount);
        args.putString("type", vals[0]);
        args.putString("notes", vals[1]);
        args.putString("order_id", vals[2]);
        args.putString("callback_url", vals[3]);
        Fragment fragment = new TerminalFragment();
        fragment.setArguments(args);
        ft.replace(R.id.content_navigation, fragment, "TerminalFragment");
        ft.addToBackStack(null);
        ft.commit();
    }


    //  PRINT START
    public void DetailReport() {
        //String str="test";
        firePrint(2);
    }

    public void SummaryReport() {
        // String str="test";
        firePrint(1);
    }


    public void firePrint(final int type) {
        dTag(0, TAG, "firePrint");
        WebServices.getInstance().getBatchPrint(MainActivity.this, type, "CURRENT", new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
//                Util.logLargeString(TAG, "print_response#" + responseStr);
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Util.deleteBatchValue(MainActivity.this);

                            JSONObject object = new JSONObject(responseStr);
                            JSONArray arr = object.getJSONArray("ResponseData");

                            receiptData(arr.toString());
                            if(fragment instanceof TransactionFragment){
                                ((TransactionFragment)fragment).manageTransactions();
                            }
                        } catch (Exception e) {
//                            Util.logLargeString("json_exception_" + type, e.getMessage());
                            Log.e(TAG, e.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            Util.deleteBatchValue(MainActivity.this);
                            if(fragment instanceof TransactionFragment){
                                ((TransactionFragment)fragment).manageTransactions();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        Log.e(TAG, message);
                    }
                });
            }
        });
    }

    public void receiptData(String response) {
        dTag(0, TAG, "receiptData");
        try {
            JSONArray printArray = new JSONArray(response);
            for (int i = 0; i < printArray.length(); i++) {
                JSONObject pObj = printArray.getJSONObject(i);
                final String printerIP = pObj.getString("PRINTER_IP");
                final String printerPort = pObj.getString("PRINTER_PORT");
                final String printerName = pObj.getString("PRINTER_NAME");
                final String orderId = pObj.getString("PRINT_ORDER_ID");
                final String jobId = pObj.getString("PRINT_JOB_ID");
                final String printType = pObj.getString("PRINTER_TYPE");
                final JSONArray printHeader = pObj.getJSONArray("PRINT_HEADER");
                final JSONArray printBody = pObj.getJSONArray("PRINT_BODY");
                final JSONArray printFooter = pObj.getJSONArray("PRINT_FOOTER");

                List<PrintModel> header = new ArrayList<>();
                List<PrintModel> body = new ArrayList<>();
                List<PrintModel> footer = new ArrayList<>();
                Util.addIntoList(header, printHeader);
                Util.addIntoList(body, printBody);
                Util.addIntoList(footer, printFooter);
                if (printerIP.equals("")) {
                    printers = findReceiptPrinter(MainActivity.this);
                    PrintedReceipt receipt = PrintUtil.getInstance(MainActivity.this).generateReceipt(printHeader, printBody, printFooter, null);
                    printReceipt(receipt, printerName);
                } else {
                    //noinspection unchecked
                    PrintUtil.getInstance(context).PrintNew(printerIP, Integer.valueOf(printerPort), header, body, footer);

                }
                Util.freeMemory();
            }

        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
            Util.showErrorToast(MainActivity.this, e.getMessage());
        }
    }


    /**
     * Initialize the print accessories
     **/
    public void initializeAccessories() {
        dTag(0, TAG, "initializeAccessories");
        try {
            // initialize capabilityProviderServiceHelper
            accessoryProviderServiceHelper = new AccessoryProviderServiceHelper(MainActivity.this);
            // connect to accessory manager service
            accessoryProviderServiceHelper.bindAccessoryManager(
                    new AccessoryProviderServiceHelper.AccessoryManagerConnectionCallback() {
                        @Override
                        public void onConnected(AccessoryProviderServiceHelper helper) {
                            // when connected check if we have any printers registered
                            if (helper.getAccessoryServiceManager() != null) {
                                AccessoryProviderFilter filter = new AccessoryProviderFilter(AccessoryType.PRINTER);
                                Log.d(TAG, "trying to get PRINTER accessory...");
                                try {
                                    // look up the printers using the filter
                                    helper.getAccessoryServiceManager().getAccessoryProviders(
                                            filter, poyntAccessoryManagerListener);
                                } catch (RemoteException e) {
                                    Log.e(TAG, "Unable to connect to Accessory Service", e);
                                }
                            } else {
                                Log.e(TAG, "Not connected with accessory service manager");
                            }
                        }

                        @Override
                        public void onDisconnected(AccessoryProviderServiceHelper accessoryProviderServiceHelper) {
                            Log.e(TAG, "Disconnected with accessory service manager");
                        }
                    });
        } catch (SecurityException e) {
            Log.e(TAG, "Failed to connect to capability or accessory manager", e);
        }
    }

    public void printReceipt(PrintedReceipt receipt, String printerName) {
        dTag(0, TAG, "printReceipt");
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrint(binder, receipt);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrint(binder, receipt);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }


    public void printReceiptV2(PrintedReceiptV2 receipt, String printerName) {
        if (providers != null && providers.size() > 0) {
//            Util.logLargeString("providers##", new Gson().toJson(providers));
//            Util.logLargeString("printers###", new Gson().toJson(printers));
            for (AccessoryProvider provider : providers) {
                IBinder binder = mPrinterServices.get(provider);
                if (printers != null) {
//                Log.i("printers#", printers.toString().toLowerCase());
                    Log.i("provider#", provider.getProviderName().toLowerCase());
                    Log.i("printerName#", printerName.toLowerCase());
//                printers.toString().toLowerCase().contains(provider.getProviderName().toLowerCase()) &&
//                if (printers.toString().toLowerCase().contains(printerName.toLowerCase())) {
                    if (provider.getProviderName().toLowerCase().equals(printerName.toLowerCase())) {
                        if (binder != null) {
                            doPrintV2(binder, receipt, printerName);
                            break;
                        } else {
                            Log.e(TAG, "No service connection found");
                            initializeAccessories();
                            doPrintV2(binder, receipt, printerName);
                        }
                    } else {
                        Log.e(TAG, "No printer found");
                    }
                } else {
                    Util.showErrorToast(context, "No Printer found!");
                }
            }
        } else {
            Log.e(TAG, "printer not connected");
        }
    }

    public void doPrint(IBinder binder, PrintedReceipt receipt) {
        dTag(0, TAG, "doPrint");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());

                    }

                });
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    public void doPrintV2(IBinder binder, PrintedReceiptV2 receipt, String printerName) {
        dTag(0, TAG, "doPrintV2");
        IPoyntPrinterService printerService = IPoyntPrinterService.Stub.asInterface(binder);
        if (printerService != null) {
            try {
                /*printerService.printReceipt(UUID.randomUUID().toString(),
                        receipt, new IPoyntPrinterServiceListener.Stub() {
                            @Override
                            public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                                Log.d(TAG, "onPrintResponse code: " + printerStatus.getCode());
                            }
                        }, null);*/

/*                printerService.printReceipt(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                },null);*/

                printerService.printReceiptForPrinter(printerName, UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }
                }, null);

              /*  printerService.printReceiptJob(UUID.randomUUID().toString(), receipt, new IPoyntPrinterServiceListener.Stub() {
                    @Override
                    public void onPrintResponse(PrinterStatus printerStatus, String s) throws RemoteException {
                        Log.d(TAG, "onPrintResponse: " + printerStatus.getMessage());
                    }

                });*/
            } catch (RemoteException e) {
                Log.e(TAG, "Failed to communicate with printer", e);
            }
        }
    }

    /**
     * Find the order receipt printers
     */
    private Set<String> findReceiptPrinter(Context context) {
        dTag(0, TAG, "findReceiptPrinter");
        Set<String> set = ReceiptPrintingPref.readReceiptPrefsFromDb(context, Constants.ReceiptPreference.PREF_PAYMENT_RECEIPT);
        if (set != null && set.size() > 0) {
            for (String s : set) {
                Log.d("_printer_#", s);
            }
        } else {
            Log.d(TAG, "No receipt printers found");
        }
        return set;
    }


    // PRINT   END

    private void checkGpsStatus() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && manager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
            return;
        }

//        if (!hasGPSDevice(this) || manager == null) {
//            Toast.makeText(this, "Gps not Supported", Toast.LENGTH_SHORT).show();
//            return;
//        }

        if (!manager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER)) {
            enableLocation();
        }
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLocation() {
        LocationRequest locationRequest = LocationRequest.create();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> mTask = LocationServices
                .getSettingsClient(this).checkLocationSettings(builder.build());
        mTask.addOnCompleteListener(task -> {
            try {
                LocationSettingsResponse response = task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location
                // requests here.

            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);
                            break;
                        } catch (Exception e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    public static void sendErrorToServer(Context context, Exception ex) {
        Bundle params = new Bundle();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        params.putString("app_name", "TERMNL");
        params.putString("class_name", context.getClass().getName());
        params.putString("date", simpleDateFormat.format(new Date()));
        params.putString("full_exception", ex.getMessage());
        params.putString("version", "20.0");
        mFirebaseAnalytics.logEvent("internal", params);
    }

}
