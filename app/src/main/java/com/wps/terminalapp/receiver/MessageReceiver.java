package com.wps.terminalapp.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.Set;

import co.poynt.os.model.Intents;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by vignaraj.r on 02-Oct-18.
 */

public class MessageReceiver extends BroadcastReceiver{
    public static String TAG = MessageReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "message_received");

        String type = "";
        String title = "";
        long amount = 0L;
        String notes = "";
        String message = "";
        String orderId = "";
        String callbackUrl = "";
        Set keys = intent.getExtras().keySet();
        for (Object key : keys) {
            System.out.println(key + " xx:xx " + intent.getExtras().get((String) key));
        }
        String content = intent.getStringExtra(Intents.INTENT_EXTRA_CLOUD_MESSAGE_BODY);
        System.out.println(content);

        if (content != null) {
            try {
                JSONObject obj = new JSONObject(content);
                title = obj.optString("title");
                message = obj.optString("message");
                type = obj.optString("type");
                amount = obj.optLong("amount");
                notes = obj.optString("notes");
                orderId = obj.optString("order_id");
                callbackUrl = obj.optString("callback_url");
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
        if (amount!=0L) {
            // to open the activity
            Intent myInt = new Intent(context, MainActivity.class);
            myInt.putExtra("notification", true);
            myInt.putExtra("type", type);
            myInt.putExtra("amount", amount);
            myInt.putExtra("notes", notes);
            myInt.putExtra("order_id", orderId);
            myInt.putExtra("callback_url", callbackUrl);

            PendingIntent pendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    myInt,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            RemoteViews customView = new RemoteViews(context.getPackageName(), R.layout.layout_notification);
            customView.setImageViewResource(R.id.mImage, R.drawable.icon_cg_terminal);
            customView.setTextViewText(R.id.mTitle, title);
            customView.setTextViewText(R.id.mMessage, message);

            final int randomId = generateRandom();
            Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.icon_cg_terminal);

            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.bigText(message);
            bigTextStyle.setBigContentTitle(title);

            Notification directReplyNotification = new NotificationCompat.Builder(context)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setLargeIcon(largeIcon)
                    .setSmallIcon(R.drawable.alarm_32)
                    .setStyle(bigTextStyle)
//
//
//                    .setContentTitle(title)
//                    .setContentText(message)
//
//                .addAction(remoteInputAction)
                    .setColor(ContextCompat.getColor(context.getApplicationContext(), R.color.colorPrimary))
                    .setLights(ContextCompat.getColor(
                            context.getApplicationContext(), R.color.colorPrimary), 1000, 1000)
                    .setVibrate(new long[]{800, 800, 800, 800})
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .build();


            final NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(randomId, directReplyNotification);
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    notificationManager.cancel(randomId);
                }
            }, 15000);

        }
    }

    public int generateRandom() {
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }
}
