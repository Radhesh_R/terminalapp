package com.wps.terminalapp;

import android.content.Intent;

import androidx.multidex.MultiDexApplication;

import com.facebook.stetho.Stetho;
import com.wps.terminalapp.exception.ExceptionActivity;

public class Application extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        //LeakCanary.install(this);

        Thread.setDefaultUncaughtExceptionHandler ((thread, e) -> {
            e.printStackTrace();
            Intent intent = new Intent(Application.this, ExceptionActivity.class);
            intent.putExtra("exception", e);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

            System.exit(0);
        });

    }
}
