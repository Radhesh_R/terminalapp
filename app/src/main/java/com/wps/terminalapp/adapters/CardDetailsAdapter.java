package com.wps.terminalapp.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.wps.terminalapp.R;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.models.CardDetails;
import com.wps.terminalapp.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vignaraj.r on 06-Sep-18.
 */

public class CardDetailsAdapter extends RecyclerView.Adapter<CardDetailsAdapter.RowHolder> {
    List<CardDetails> details = new ArrayList<>();
    Context context;


    public class RowHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_cardtype)
        ImageView imgCardtype;
        @BindView(R.id.img_layout)
        RelativeLayout imgLayout;
        @BindView(R.id.txt_cardamt)
        TextSansRegular txtCardamt;

        public RowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public CardDetailsAdapter(Context context, List<CardDetails> details) {
        this.context = context;
        this.details = details;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cards, parent, false);
        return new RowHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RowHolder holder, int position) {
        CardDetails list = details.get(position);
        String cardImage = list.getmCardImage();
        String cardType = list.getmCardType();
        double amount = list.getmAmount();
        Picasso.get().load(cardImage).into(holder.imgCardtype);
        holder.txtCardamt.setText(Util.formatAmount(amount));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }
}
