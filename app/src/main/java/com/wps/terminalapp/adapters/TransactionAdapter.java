package com.wps.terminalapp.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.appolica.flubber.Flubber;
import com.squareup.picasso.Picasso;
import com.wps.terminalapp.R;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.listeners.ViewTransactionListener;
import com.wps.terminalapp.models.TransactionModel;
import com.wps.terminalapp.utils.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vignaraj.r on 29-Aug-18.
 */

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.RowHolder> {
    List<TransactionModel> list = new ArrayList<>();
    ViewTransactionListener listener;
    Context context;
    //


    public class RowHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_tdate)
        TextSansRegular txtTdate;
        @BindView(R.id.txt_ttid)
        TextSansRegular txtTtid;
        @BindView(R.id.img_ttype)
        ImageView imgTtype;
        @BindView(R.id.txt_ttype)
        TextSansRegular txtTtype;
        @BindView(R.id.txt_tamount)
        TextSansRegular txtTamount;
        @BindView(R.id.img_tstatus)
        ImageView imgTstatus;
        @BindView(R.id.layout_transaction)
        LinearLayout layoutTransaction;

        public RowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Flubber.with()
                    .animation(Flubber.AnimationPreset.SQUEEZE_DOWN)
                    .repeatCount(0)
                    .duration(500)
                    .createFor(itemView)
                    .start();
        }
    }

    public TransactionAdapter(Context context, List<TransactionModel> list, ViewTransactionListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public TransactionAdapter(Context context, List<TransactionModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_transaction, parent, false);
        return new RowHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RowHolder holder, int position) {
        TransactionModel model = list.get(position);
        String transactionId = model.getmTransactionId().substring(0, 8);
        String transactionStatus = model.getmTransactionStatus();
        String date = Util.getDateString(model.getmDate(), "dd-MMM-yyyy\nhh:mm a");
        String paymentType = model.getmTransactionType();
        String cardImage = model.getmCardImage();
        String cardNumber = model.getmCardNumber();
        String amount = "AED" + Util.formatAmount(model.getmAmount());
        String paymentMethod = model.getmCardType();

        transactionId = "#" + transactionId;
        String displayNo = model.getmDisplayNo();
        holder.txtTtid.setText(displayNo);
        holder.txtTdate.setText(date);
        holder.txtTamount.setText(amount);
        if (paymentType.equals("CARD")) {
            Picasso.get().load(cardImage).into(holder.imgTtype);
            holder.txtTtype.setText(cardNumber.substring(cardNumber.length() - 4, cardNumber.length()));
        } else {
            holder.imgTtype.setImageResource(R.drawable.cash_b_32);
            holder.txtTtype.setText("CASH");
        }
        holder.txtTtype.setText(paymentMethod);


        if (transactionStatus.equals("AUTHORIZED")) {
            holder.imgTstatus.setImageResource(R.drawable.auth_32);
            holder.txtTamount.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTdate.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTtid.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTtype.setTextColor(ContextCompat.getColor(context, R.color.black));
        } else if (transactionStatus.equals("CAPTURED")) {
            holder.imgTstatus.setImageResource(R.drawable.done_32);
            holder.txtTamount.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTdate.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTtid.setTextColor(ContextCompat.getColor(context, R.color.black));
            holder.txtTtype.setTextColor(ContextCompat.getColor(context, R.color.black));
        } else if (transactionStatus.equals("DECLINED")) {
            holder.imgTstatus.setImageResource(R.drawable.decline_32);
            holder.txtTamount.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTdate.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTtid.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTtype.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
        } else if (transactionStatus.equals("VOIDED")) {
            holder.imgTstatus.setImageResource(R.drawable.void_32);
            holder.txtTamount.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTdate.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTtid.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
            holder.txtTtype.setTextColor(ContextCompat.getColor(context, R.color.btn_grey1));
        }


        holder.layoutTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (list.get(holder.getAdapterPosition()).getmTransactionType().equals("CARD")) {
                if (listener != null) {
                    listener.selectedTransaction(list.get(holder.getAdapterPosition()));
                }
//                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
