package com.wps.terminalapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.wps.terminalapp.R;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.models.UserData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vignaraj.r on 19-Sep-18.
 */

public class UserAdapter extends BaseAdapter{
    List<UserData> data = new ArrayList<>();
    LayoutInflater inflator = null;
    Context context;

    public UserAdapter(Context context, List<UserData> data){
        this.context = context;
        this.data = data;
        inflator = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflator.inflate(R.layout.row_users, null);
            holder = new ViewHolder();
            holder.txt_user = (TextSansRegular) convertView.findViewById(R.id.row_user);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        holder.txt_user.setText(data.get(position).getmName());
        return convertView;
    }

    static class ViewHolder{
        TextSansRegular txt_user;
    }
}
