package com.wps.terminalapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wps.terminalapp.R;
import com.wps.terminalapp.listeners.PopSelectionListener;
import com.wps.terminalapp.transactions.TransactionFragment;

import java.util.List;

/**
 * Created by vignaraj.r on 3/27/2017.
 */

public class PopAdapter extends BaseAdapter {
    Context context;
    List<String> menuList;
    TransactionFragment.PopListener listener;
    PopSelectionListener popSelectionListener;
    private LayoutInflater mInflator = null;

    public PopAdapter(Context context, List<String> menuList, TransactionFragment.PopListener listener) {
        this.context = context;
        this.menuList = menuList;
        this.listener = listener;
        mInflator = LayoutInflater.from(context);
    }

    public PopAdapter(Context context, List<String> menuList, PopSelectionListener popSelectionListener) {
        this.context = context;
        this.menuList = menuList;
        this.popSelectionListener = popSelectionListener;
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return menuList.size();
    }

    @Override
    public Object getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PopHolder holder;
        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.row_popup, null);
            holder = new PopHolder();
            holder.txt_menu_name = (TextView) convertView.findViewById(R.id.ordermenu_name);
            convertView.setTag(holder);
        } else {
            holder = (PopHolder) convertView.getTag();
        }
        holder.txt_menu_name.setText(menuList.get(position).toString());
        holder.txt_menu_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.selectedMenu(menuList.get(position));
                }
                if (popSelectionListener != null) {
                    popSelectionListener.selectedValue(menuList.get(position));
                }
            }
        });
        return convertView;
    }

    static class PopHolder {
        TextView txt_menu_name;
    }
}
