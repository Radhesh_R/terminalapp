package com.wps.terminalapp.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.utils.Util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import co.poynt.api.model.Transaction;

/**
 * Created by vignaraj.r on 03-Sep-18.
 */

public class AppPreferences {
    private static Gson GSON = new Gson();
    public static AppPreferences instance;
    public static String TAG = AppPreferences.class.getSimpleName();

    public static AppPreferences getInstance() {
        if (instance == null) {
            instance = new AppPreferences();
        }
        return instance;
    }

    /**
     * To store the user data in preferences.
     */
    public <T> void storeUserdata(Context context, List<T> list, boolean available, long timestamp, String sName, String sVal, String settleType) {
        Util.dTag(0, TAG, "storeUserData");
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user_data", GSON.toJson(list));
        editor.putBoolean("status", available);
        editor.putLong("last_update", timestamp);
        editor.putString("sVal", sVal);
        editor.putString("sName", sName);
        editor.putString("type", settleType);
        editor.commit();
    }

    /**
     * Check the data is exist or not.
     */
    public boolean checkUserDataExist(Context context) {
        Util.dTag(0, TAG, "checkUserDataExist");
        boolean result;
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        result = preferences.getBoolean("status", false);
        Log.i("user_data_status", String.valueOf(result));
        return result;
    }

    /**
     * Last updated time of users data
     */
    public long getLastupdateTime(Context context) {
        Util.dTag(0, TAG, "getLastupdateTime");
        long result;
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        result = preferences.getLong("last_update", 0);
        return result;
    }

    public String getSettleName(Context context) {
        Util.dTag(0, TAG, "getSettleName");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        result = preferences.getString("sName", "Ask PIN for Settle?");
        Log.i("sName", String.valueOf(result));
        return result;
    }

    public String getSettleValue(Context context) {
        Util.dTag(0, TAG, "getSettleValue");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        result = preferences.getString("sVal", "N");
        Log.i("sval", String.valueOf(result));
        return result;
    }

    public String getSettleType(Context context) {
        Util.dTag(0, TAG, "getSettleValue");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        result = preferences.getString("type", "MANUAL");
        Log.i("type", String.valueOf(result));
        return result;
    }

    /**
     * To getting the list of users.
     */
    public List<UserData> getUsersList(Context context) {
        Util.dTag(0, TAG, "getUsersList");
        SharedPreferences preferences = context.getSharedPreferences("userdata", context.MODE_PRIVATE);
        String userString = preferences.getString("user_data", null);
        List<UserData> list = new ArrayList<>();
        if (userString == null) {
            return list;
        } else {
            try {
                Type type = new TypeToken<List<UserData>>() {
                }.getType();
                list = GSON.fromJson(userString, type);
                return list;
            } catch (Exception e) {
                Log.e("error", e.getMessage());
                Util.showErrorToast(context, e.getMessage());
                return list;
            }
        }
    }

    public void storeManualStatus(Context context, int flag) {
        Util.dTag(0, TAG, "storeManualStatus");
        SharedPreferences preferences = context.getSharedPreferences("manulstatus", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("status", flag);
        editor.commit();
    }

    public int getManualStatus(Context context) {
        Util.dTag(0, TAG, "getManualStatus");
        int result;
        SharedPreferences preferences = context.getSharedPreferences("manulstatus", context.MODE_PRIVATE);
        result = preferences.getInt("status", 0);
        return result;
    }

    public void storeLastTransaction(Context context, Transaction transaction) {
        SharedPreferences preferences = context.getSharedPreferences("t_data", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("transaction", new Gson().toJson(transaction));
        editor.commit();
    }

    public void clearLastTransaction(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences("t_data", Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    public <T> T getLastTransaction(Context context, Class<T> getClass) {
        SharedPreferences preferences = context.getSharedPreferences("t_data", context.MODE_PRIVATE);
        String userString = preferences.getString("transaction", null);
        if (userString == null) {
            return null;
        } else {
            try {

//                System.out.println("Object String  ##" + GSON.fromJson(userString, Transaction.class));
                return GSON.fromJson(userString, getClass);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object stored with key "
                        + "catalog" + " is instance of other class");
            }
        }
    }

    public long getLastTransactionDate(Context context) {
        Transaction transaction = getLastTransaction(context, Transaction.class);
        long date;
        if (transaction != null) {
            try{
                long tDate = transaction.getCreatedAt().getTime().getTime();
                date = Util.getFormattedTimeStamp(tDate, "dd/MMM/yyyy");
            }catch (Exception ignore){
                date = 0L;
            }
        } else {
            date = 0L;
        }
        return date;
    }


    /**
     * To store the cash or card transaction in preferences.
     */
    public <T> void storeCash(Context context, String sName, String sVal) {
        Util.dTag(0, TAG, "storeCash");
        SharedPreferences preferences = context.getSharedPreferences("cashdata", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("cashValue", sVal);
        editor.putString("cashName", sName);
        editor.commit();
    }

    /**
     * To store the cash or card transaction in preferences.
     */
    public <T> void storeCard(Context context, String sName, String sVal) {
        Util.dTag(0, TAG, "storeCard");
        SharedPreferences preferences = context.getSharedPreferences("carddata", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("cardValue", sVal);
        editor.putString("cardName", sName);
        editor.commit();
    }

    public String getCashTrans(Context context) {
        Util.dTag(0, TAG, "storeCash");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("cashdata", context.MODE_PRIVATE);
        result = preferences.getString("cashValue", "N");
        return result;
    }

    public String getCardTrans(Context context) {
        Util.dTag(0, TAG, "storeCard");
        String result;
        SharedPreferences preferences = context.getSharedPreferences("carddata", context.MODE_PRIVATE);
        result = preferences.getString("cardValue", "N");
        return result;
    }

    public void saveScreenType(Context context, String screenType){
        if(screenType != null && !screenType.isEmpty()){
            SharedPreferences preferences = context.getSharedPreferences("terminal_settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("screen_type", screenType);
            editor.apply();
        }
    }

    public String getScreenType(Context context){
        SharedPreferences preferences = context.getSharedPreferences("terminal_settings", Context.MODE_PRIVATE);
        return preferences.getString("screen_type", "DBL");
    }

    public void saveIsReceetEnabled(Context context, String enabled){
        if(enabled != null && !enabled.isEmpty()){
            SharedPreferences preferences = context.getSharedPreferences("terminal_settings", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("receet_enabled", enabled);
            editor.apply();
        }
    }

    public String getIsReceetEnabled(Context context){
        SharedPreferences preferences = context.getSharedPreferences("terminal_settings", Context.MODE_PRIVATE);
        return preferences.getString("receet_enabled", "F");
    }
}
