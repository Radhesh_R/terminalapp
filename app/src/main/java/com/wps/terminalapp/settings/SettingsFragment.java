package com.wps.terminalapp.settings;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.appolica.flubber.Flubber;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;
import com.wps.terminalapp.BuildConfig;
import com.wps.terminalapp.MainActivity;
import com.wps.terminalapp.R;
import com.wps.terminalapp.api.APIBuilder;
import com.wps.terminalapp.api.APIComponent;
import com.wps.terminalapp.custom.TextSansBold;
import com.wps.terminalapp.custom.TextSansRegular;
import com.wps.terminalapp.dialogs.UserDialog;
import com.wps.terminalapp.listeners.ErrorListener;
import com.wps.terminalapp.listeners.ResponseListener;
import com.wps.terminalapp.models.ErrorLogResponse;
import com.wps.terminalapp.models.LoGErr;
import com.wps.terminalapp.models.UserData;
import com.wps.terminalapp.utils.GeneralUtil;
import com.wps.terminalapp.utils.NetworkUtil;
import com.wps.terminalapp.utils.Util;
import com.wps.terminalapp.webrequest.WebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.wps.terminalapp.settle.SettleFragment.TAG;

/**
 * Created by vignaraj.r on 03-Sep-18.
 */

public class SettingsFragment extends Fragment {
    @BindView(R.id.btn_syncuser)
    ImageView btnSyncUser;
    @BindView(R.id.txt_lastupdated)
    TextSansRegular txtLastupdated;
    @BindView(R.id.btn_syncbatch)
    ImageView btnSyncbatch;
    @BindView(R.id.txt_batch)
    TextSansRegular txtBatch;
    @BindView(R.id.txt_ttc)
    TextSansRegular txtTtc;
    @BindView(R.id.txt_tid)
    TextSansRegular txtTid;
    @BindView(R.id.txt_mid)
    TextSansRegular txtMid;
    @BindView(R.id.check_manual)
    CheckBox checkManual;
    @BindView(R.id.layout_clearcache)
    LinearLayout layout_clearcache;
    @BindView(R.id.txt_processor)
    TextSansRegular txt_processor;
    @BindView(R.id.txt_processor_vers)
    TextSansRegular txt_processor_vers;
    @BindView(R.id.txt_acquirer)
    TextSansRegular txt_acquirer;
    @BindView(R.id.txt_buildtype)
    TextSansRegular txt_buildtype;

    @BindView(R.id.layout_supay)
    LinearLayout layoutSupay;
    @BindView(R.id.img_supportpayment)
    ImageView imgSupportPayment;
    @BindView(R.id.img_useinfo)
    ImageView imgUserInfo;

    @BindView(R.id.txt_settlename)
    TextSansBold txtSettleName;
    @BindView(R.id.txt_settleval)
    TextSansBold txtSettleVal;

    @BindView(R.id.layout_settletype)
    LinearLayout layoutSettleType;
    @BindView(R.id.txt_settletype)
    TextSansBold txtSettleType;


    PackageInfo packageInfo;
    LoGErr loGErr = new LoGErr();
    private APIComponent retrofitPrint;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        APIBuilder apiBuilder = new APIBuilder(requireContext());
        Retrofit mRetrofitPrint = apiBuilder.retrofitPrint();
        retrofitPrint = mRetrofitPrint.create(APIComponent.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_RIGHT)
                .repeatCount(0)
                .duration(2000)
                .createFor(view)
                .start();

        assignUI();
        return view;
    }

    public void assignUI() {
        if (AppPreferences.getInstance().checkUserDataExist(getActivity())) {
            String lastUpdate = Util.getDateString(AppPreferences.getInstance().getLastupdateTime(getActivity()), "dd-MMM-yyyy hh:mm a");
            txtLastupdated.setText("Last Updated : " + lastUpdate);
        }
        showEmptyScreen();


        if (AppPreferences.getInstance().getManualStatus(getActivity()) == 1) {
            checkManual.setChecked(true);
        } else {
            checkManual.setChecked(false);
        }
        if (NetworkUtil.getConnectivityState(getActivity()) != 0) {
            syncBatchInfo();
            //TODO new api integration
        } else {
            Util.showErrorToast(getContext(), "Error, Internet Not Connected!");
        }

        try {
            if (Util.isProviderExist(getActivity())) {
                txt_processor.setText("com.chk.mashreqtp");
                packageInfo = getActivity().getPackageManager().getPackageInfo("com.chk.mashreqtp", 0);
                txt_processor_vers.setText(packageInfo.versionName);

            } else {
                txt_processor.setText("com.wps.mashreqtp");
                packageInfo = getActivity().getPackageManager().getPackageInfo("com.wps.mashreqtp", 0);
                txt_processor_vers.setText(packageInfo.versionName);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("error", e.getMessage());
            txt_processor.setText("Processor not installed");
            txt_processor_vers.setText("Not found");
        }

        if (!Util.currentAcquirer.equals("")) {
            txt_acquirer.setText(Util.currentAcquirer);
        } else {
            txt_acquirer.setText("Acquirer Not Found!");
        }

        if (Util.supportedPayment.equals("")) {
            layoutSupay.setVisibility(View.GONE);
        } else {
            layoutSupay.setVisibility(View.VISIBLE);
            Picasso.get().load(Util.supportedPayment).into(imgSupportPayment);
        }

        List<UserData> userData = AppPreferences.getInstance().getUsersList(getActivity());

        if (userData != null && userData.size() > 0) {
            imgUserInfo.setVisibility(View.VISIBLE);
        } else {
            imgUserInfo.setVisibility(View.GONE);
        }


        String settleName = AppPreferences.getInstance().getSettleName(getActivity());
        String settleVal = AppPreferences.getInstance().getSettleValue(getActivity());

        Log.i("settleName", settleName);
        Log.i("settleVal", settleVal);

        if (settleVal.toLowerCase().equals("y")) {
            settleVal = "YES";
        } else {
            settleVal = "NO";
        }

        txtSettleName.setText(settleName);
        txtSettleVal.setText(settleVal);
        txtSettleType.setText(AppPreferences.getInstance().getSettleType(getActivity()));
        txt_buildtype.setText(BuildConfig.BUILD_TYPE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id.btn_syncuser)
    public void syncUserData() {
        doClearCache1();
        Util.dTag(1, TAG, "syncUserData");
        WebServices.getInstance().getUsersData(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String response) {
                if (response != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject responseData = new JSONObject(response);
                                if (responseData.getBoolean("IsSuccess")) {
                                    JSONArray jsonArray = responseData.getJSONArray("ResponseData");
                                    List<UserData> list = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        String userCode = obj.getString("usercode");
                                        String username = obj.getString("username");
                                        String userrole = obj.getString("userrole");
                                        String userPin = obj.getString("userpin");
                                        String userauthpin = obj.optString("userauthpin");
                                        list.add(new UserData(userCode, username, userrole, userPin, userauthpin));
                                    }

                                    JSONArray otherData = responseData.getJSONArray("OtherData1");
                                    String settlePINKey = "SETTLE_PIN_YN";
                                    String settleTypeKey = "SETTLE_TYPE";
                                    String settleCashKey = "SETTLE_CASH_YN";
                                    String settleCardKey = "SETTLE_CARD_YN";
                                    String settleName = "Ask PIN for Settle?";
                                    String settleVal = "N";
                                    String settleValY = "Y";
                                    String settleType = "MANUAL";
                                    if (otherData != null) {
                                        for (int i = 0; i < otherData.length(); i++) {
                                            JSONObject obj = otherData.getJSONObject(i);
                                            String sType = obj.optString("stype");
                                            if (sType.equals(settlePINKey)) {
                                                settleName = obj.optString("sname");
                                                settleVal = obj.optString("sval");
                                            } else if (sType.equals(settleTypeKey)) {
                                                settleType = obj.optString("sval");
                                            } else if (sType.equals(settleCashKey)) {
                                                AppPreferences.getInstance().storeCash(getActivity(), settleCashKey, obj.optString("sval"));
                                            } else if (sType.equals(settleCardKey)) {
                                                AppPreferences.getInstance().storeCard(getActivity(), settleCardKey, obj.optString("sval"));
                                            }
                                        }
                                    }
                                   /* String sName = "Ask PIN for Settle?";
                                    String sVal = "N";
                                    if (otherData != null) {
                                        JSONObject obj = otherData.getJSONObject(0);
                                        sName = obj.optString("sname");
                                        sVal = obj.optString("sval");
                                    }*/
                                    AppPreferences.getInstance().storeUserdata(getActivity(), list, true, Util.getCurrentTimeStamp(), settleName, settleVal, settleType);
//                                    AppPreferences.getInstance().storeUserdata(getActivity(), list, true, Util.getCurrentTimeStamp(), settleName, settleVal, settleType);
                                    Util.showSuccessToast(getActivity(), "Data Successfully Synced.");
                                    String lastUpdate = Util.getDateString(AppPreferences.getInstance().getLastupdateTime(getActivity()), "dd-MMM-yyyy hh:mm a");

                                    if (settleVal.toLowerCase().equals("y")) {
                                        settleVal = "YES";
                                    } else {
                                        settleVal = "NO";
                                    }
                                    txtSettleName.setText(settleName);
                                    txtSettleVal.setText(settleVal);
                                    txtLastupdated.setText(lastUpdate);
                                    txtSettleType.setText(settleType);
                                } else {
                                    Util.showErrorToast(getActivity(), responseData.getString("ErrMsg"));
                                }
                            } catch (JSONException e) {
                                Util.closeProgressDialog();
                                Util.showErrorDialog(getActivity(), "Settings not found", new ErrorListener() {
                                    @Override
                                    public void doAction() {
                                        //do nothing
                                    }
                                });
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.closeProgressDialog();
                        Util.showErrorToast(getActivity(), message);
                    }
                });
            }
        });
    }

    @OnClick(R.id.btn_syncbatch)
    public void syncBatchInfo() {
        doClearCache1();
        Util.dTag(1, TAG, "syncBatchInfo");
        WebServices.getInstance().getCurrentBatchInfo(getActivity(), new ResponseListener() {
            @Override
            public void onResponse(final String responseStr) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                    Utils.logLargeString("response : ", "getBatchInfoData : " + responseStr);
                            JSONObject response = new JSONObject(responseStr);
                            boolean isSuccess = response.optBoolean("IsSuccess", false);
                            if (isSuccess) {
                                JSONObject responseData = response.getJSONObject("ResponseData");
                                String TTC = responseData.optString("TTC");
                                String BATCH = responseData.optString("BATCH");
                                String MID = responseData.optString("MID");
                                String TID = responseData.optString("TID");

                                Util.deleteBatchValue(getActivity());
                                Util.addBatchID(getActivity(), BATCH);

                                txtBatch.setText(BATCH);
                                txtTtc.setText(TTC);
                                txtMid.setText(MID);
                                txtTid.setText(TID);
                                Util.isProviderExist(getActivity());

                                Util.lastInsertedBatch(getActivity());

                                AppPreferences.getInstance().saveScreenType(getActivity(), responseData.optString("SCREEN_TYPE"));
                                AppPreferences.getInstance().saveIsReceetEnabled(getActivity(), responseData.optString("IS_RECEET_ENABLED"));
                            } else {
                                String errMsg = response.getString("ErrMsg");
                                Util.showErrorDialog(getContext(), errMsg, new ErrorListener() {
                                    @Override
                                    public void doAction() {
                                        //do nothing
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, e.getMessage());
                            Util.showErrorToast(getActivity(), e.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onFailure(final String message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Util.showErrorToast(getActivity(), message);
                    }
                });
            }
        });
    }

    @OnClick(R.id.layout_clearcache)
    public void doClearCache() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder.setMessage("Clear app cache will close this application, Do you want to continue?")
                .setCancelable(true)
                .setPositiveButton("Yes", (dialog, id) -> {
                    try {
//                        Intent intent = new Intent();
//                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        Uri uri = Uri.fromParts("package", txt_processor.getText().toString(), null);
//                        intent.setData(uri);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);


                        // clearing app data on kitkat
                        String packageName = getActivity().getPackageName();
                        Runtime runtime = Runtime.getRuntime();
                        runtime.exec("pm clear "+packageName);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Util.showErrorToast(getActivity(), "Something Went Wrong");
                        Crashlytics.logException(e);
                        MainActivity.sendErrorToServer(requireContext(), e);
                        loGErr.setDevcSrNo(GeneralUtil.getDeviceSerial());
                        loGErr.setErrMsg(e.getMessage());
                        retrofitPrint.updateErrorLog(loGErr).enqueue(new Callback<ErrorLogResponse>() {
                            @Override
                            public void onResponse(Call<ErrorLogResponse> call, Response<ErrorLogResponse> response) {
                                if (response.body() != null) {
                                    if (response.body().getSuccess()) {
                                        if (response.body().getResponseData().equals("OK")) {
                                            Log.e("Error send", "OK");
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<ErrorLogResponse> call, Throwable t) {
                                Log.e("Error send", "Failed");
                            }
                        });
                    }
                })
                .setNegativeButton("No", (dialog, id) -> {
                });
        builder.setCancelable(false);
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void doClearCache1() {
        Util.dTag(1, TAG, "doClearCache");
        Util.deleteCache1(getActivity());
        Util.showSuccessToast(getActivity(), "Cache Clear Successfully");
    }

    @OnCheckedChanged(R.id.check_manual)
    public void setCheckManual(CompoundButton button, boolean isChecked) {
        Util.dTag(1, TAG, "setCheckManual");
        if (isChecked) {
            AppPreferences.getInstance().storeManualStatus(getActivity(), 1);
        } else {
            AppPreferences.getInstance().storeManualStatus(getActivity(), 0);
        }
    }

    public void showEmptyScreen() {
        Util.dTag(0, TAG, "showEmptyScreen");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isSecondScreenServiceConnected) {
                    try {
                        MainActivity.poyntSecondScreenService.displayWelcome(null, null, null);
                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });
    }

    @OnClick(R.id.img_useinfo)
    public void showUserInfo() {
        Util.dTag(1, TAG, "showUserInfo");
        List<UserData> xData = new ArrayList<>();
        List<UserData> mainData = AppPreferences.getInstance().getUsersList(getActivity());
        if (mainData != null) {
//            Util.logLargeString("user_list", new Gson().toJson(mainData));
            for (int i = 0; i < mainData.size(); i++) {
                if (mainData.get(i).getmRole().equals("X")) {
                    xData.add(mainData.get(i));
                }
            }
            if (xData.size() > 0) {
                UserDialog dialog = new UserDialog(getActivity(), xData);
                dialog.show();
            }
        }
    }
}